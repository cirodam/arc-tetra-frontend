import React from 'react';
import Link from 'next/Link';

const MobileNav = () => {
    return (
        <div id="mobile-nav">
            <Link href="/"><a><i className="fas fa-home"></i></a></Link>
            <Link href="/services"><a><i className="fas fa-list"></i></a></Link>
            <Link href="/payments"><a><i className="fas fa-dollar-sign"></i></a></Link>
        </div>
    )
}

export default MobileNav
