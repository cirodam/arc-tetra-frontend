import React from 'react';
import Link from 'next/Link';

const Footer = () => {
    return (
        <footer id="footer">
            <div className="container">
                <div id="footer-list">
                    <div id="footer-info">
                        <p className="fs-16p">We're Arc Tetra, a Dawsonville, Georgia based web developer. Our committment is to provide the services that small businesses need to succeed.</p>
                    </div>
                    
                    <div id="footer-links">
                        <div id="footer-links-list">
                            <p><Link href="/"><a>Home</a></Link></p>
                            <p><Link href="/services"><a>Services</a></Link></p>
                            <p><Link href="/payments"><a>Payments</a></Link></p>
                        </div>
                    </div>

                    <div id="footer-contact">
                        <p><i className="fas fa-phone"></i> - (555) 555 - 5555</p>
                        <p><i className="fas fa-envelope"></i> - contact@arctetra.com</p>
                        <p><i className="fas fa-map-marker"></i> - PO box 1453 Dawsonville, Ga</p>
                    </div>
                </div>
                <div id="footer-border"></div>
                <p className="fs-16p">Copyright &copy; 2021 Arc Tetra Software Ltd. Co. All Rights Reserved</p>
            </div>
        </footer>
    )
}

export default Footer
