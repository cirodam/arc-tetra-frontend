import axios from 'axios';
import React from 'react';
import styles from './posts.module.css';
import {useRouter} from 'next/router';

export async function getServerSideProps() {

    try {
        let p = await axios.get("http://54.160.183.136/wp-json/wp/v2/posts");

        return {
            props: {posts: p.data}, 
          }

    } catch (error) {

        return {
            props: {error: JSON.stringify(error)}, 
          }
    }
}

const posts = ({posts, error}) => {
console.log(posts);
console.log(error);

    const router = useRouter()

    if(!posts){
        return null;
    }

    const onPostClick = (id) => {
        router.push(`/posts/${id}`);
    }

    return (
        <div id={styles.postPage}>
            <h1 className="fs-60p mt-50p ff-rob ta-center">Our Posts</h1>
            <ul id={styles.postList}>
                {
                    posts.map(p => (
                        <li key={p.id} id={styles.postItem} onClick={() => onPostClick(p.id)}>
                            <img id={styles.postImg} src={p.jetpack_featured_media_url} alt="" />
                            <div>
                                <h2 id={styles.postTitle}>{p.title.rendered}</h2>
                                <p id={styles.postDate}>{p.date.substr(0,10)}</p>
                            </div>
                        </li>
                    ))
                }
            </ul>
        </div>
    )
}

export default posts
