webpackHotUpdate_N_E("pages/payments/[orderID]",{

/***/ "./components/CheckoutForm.js":
/*!************************************!*\
  !*** ./components/CheckoutForm.js ***!
  \************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return CheckoutForm; });
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "./node_modules/react/jsx-dev-runtime.js");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var C_Users_Tyler_Desktop_Projects_arc_tetra_software_v2_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/@babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var C_Users_Tyler_Desktop_Projects_arc_tetra_software_v2_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(C_Users_Tyler_Desktop_Projects_arc_tetra_software_v2_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var C_Users_Tyler_Desktop_Projects_arc_tetra_software_v2_node_modules_babel_runtime_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/esm/asyncToGenerator */ "./node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _stripe_react_stripe_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @stripe/react-stripe-js */ "./node_modules/@stripe/react-stripe-js/dist/react-stripe.umd.js");
/* harmony import */ var _stripe_react_stripe_js__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_stripe_react_stripe_js__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! next/router */ "./node_modules/next/router.js");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_6__);




var _jsxFileName = "C:\\Users\\Tyler\\Desktop\\Projects\\arc-tetra-software-v2\\components\\CheckoutForm.js",
    _s = $RefreshSig$();





function CheckoutForm(_ref) {
  _s();

  var orderID = _ref.orderID,
      amount = _ref.amount,
      onSuccess = _ref.onSuccess;

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_3__["useState"])(false),
      succeeded = _useState[0],
      setSucceeded = _useState[1];

  var _useState2 = Object(react__WEBPACK_IMPORTED_MODULE_3__["useState"])(null),
      error = _useState2[0],
      setError = _useState2[1];

  var _useState3 = Object(react__WEBPACK_IMPORTED_MODULE_3__["useState"])(''),
      processing = _useState3[0],
      setProcessing = _useState3[1];

  var _useState4 = Object(react__WEBPACK_IMPORTED_MODULE_3__["useState"])(false),
      disabled = _useState4[0],
      setDisabled = _useState4[1];

  var _useState5 = Object(react__WEBPACK_IMPORTED_MODULE_3__["useState"])(''),
      clientSecret = _useState5[0],
      setClientSecret = _useState5[1];

  var stripe = Object(_stripe_react_stripe_js__WEBPACK_IMPORTED_MODULE_4__["useStripe"])();
  var elements = Object(_stripe_react_stripe_js__WEBPACK_IMPORTED_MODULE_4__["useElements"])();
  var router = Object(next_router__WEBPACK_IMPORTED_MODULE_6__["useRouter"])();
  Object(react__WEBPACK_IMPORTED_MODULE_3__["useEffect"])(function () {
    // Create PaymentIntent as soon as the page loads
    window.fetch("https://29oddee2eh.execute-api.us-east-1.amazonaws.com/dev/stripe", {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        amount: amount
      })
    }).then(function (res) {
      return res.json();
    }).then(function (data) {
      setClientSecret(data.clientSecret);
    });
  }, []);
  var cardStyle = {
    style: {
      base: {
        color: "#32325d",
        fontFamily: 'Poppins, sans-serif',
        fontSmoothing: "antialiased",
        fontSize: "16px",
        "::placeholder": {
          color: "#32325d"
        }
      },
      invalid: {
        color: "#fa755a",
        iconColor: "#fa755a"
      }
    }
  };

  var handleChange = /*#__PURE__*/function () {
    var _ref2 = Object(C_Users_Tyler_Desktop_Projects_arc_tetra_software_v2_node_modules_babel_runtime_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_2__["default"])( /*#__PURE__*/C_Users_Tyler_Desktop_Projects_arc_tetra_software_v2_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.mark(function _callee(event) {
      return C_Users_Tyler_Desktop_Projects_arc_tetra_software_v2_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              setDisabled(event.empty);
              setError(event.error ? event.error.message : "");

            case 2:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }));

    return function handleChange(_x) {
      return _ref2.apply(this, arguments);
    };
  }();

  var handleSubmit = /*#__PURE__*/function () {
    var _ref3 = Object(C_Users_Tyler_Desktop_Projects_arc_tetra_software_v2_node_modules_babel_runtime_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_2__["default"])( /*#__PURE__*/C_Users_Tyler_Desktop_Projects_arc_tetra_software_v2_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.mark(function _callee2(ev) {
      var payload, body;
      return C_Users_Tyler_Desktop_Projects_arc_tetra_software_v2_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.wrap(function _callee2$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
              ev.preventDefault();
              setProcessing(true);
              _context2.next = 4;
              return stripe.confirmCardPayment(clientSecret, {
                payment_method: {
                  card: elements.getElement(_stripe_react_stripe_js__WEBPACK_IMPORTED_MODULE_4__["CardElement"])
                }
              });

            case 4:
              payload = _context2.sent;

              if (!payload.error) {
                _context2.next = 10;
                break;
              }

              setError("Payment failed ".concat(payload.error.message));
              setProcessing(false);
              _context2.next = 17;
              break;

            case 10:
              setError(null);
              setProcessing(false);
              setSucceeded(true);
              body = {
                paymentResult: payload
              };
              _context2.next = 16;
              return axios__WEBPACK_IMPORTED_MODULE_5___default.a.put("https://29oddee2eh.execute-api.us-east-1.amazonaws.com/dev/orders/".concat(orderID), body);

            case 16:
              onSuccess();

            case 17:
            case "end":
              return _context2.stop();
          }
        }
      }, _callee2);
    }));

    return function handleSubmit(_x2) {
      return _ref3.apply(this, arguments);
    };
  }();

  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("form", {
    onSubmit: handleSubmit,
    children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_stripe_react_stripe_js__WEBPACK_IMPORTED_MODULE_4__["CardElement"], {
      id: "card-element",
      options: cardStyle,
      onChange: handleChange
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 92,
      columnNumber: 7
    }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("button", {
      disabled: processing || disabled || succeeded,
      id: "submit",
      className: "btn",
      children: "Pay Now"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 93,
      columnNumber: 7
    }, this), error && /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
      className: "card-error",
      role: "alert",
      children: error
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 97,
      columnNumber: 9
    }, this)]
  }, void 0, true, {
    fileName: _jsxFileName,
    lineNumber: 90,
    columnNumber: 5
  }, this);
}

_s(CheckoutForm, "TD5+ko+lb+Su6RfyPBjxDsv+3/I=", false, function () {
  return [_stripe_react_stripe_js__WEBPACK_IMPORTED_MODULE_4__["useStripe"], _stripe_react_stripe_js__WEBPACK_IMPORTED_MODULE_4__["useElements"], next_router__WEBPACK_IMPORTED_MODULE_6__["useRouter"]];
});

_c = CheckoutForm;

var _c;

$RefreshReg$(_c, "CheckoutForm");

;
    var _a, _b;
    // Legacy CSS implementations will `eval` browser code in a Node.js context
    // to extract CSS. For backwards compatibility, we need to check we're in a
    // browser context before continuing.
    if (typeof self !== 'undefined' &&
        // AMP / No-JS mode does not inject these helpers:
        '$RefreshHelpers$' in self) {
        var currentExports = module.__proto__.exports;
        var prevExports = (_b = (_a = module.hot.data) === null || _a === void 0 ? void 0 : _a.prevExports) !== null && _b !== void 0 ? _b : null;
        // This cannot happen in MainTemplate because the exports mismatch between
        // templating and execution.
        self.$RefreshHelpers$.registerExportsForReactRefresh(currentExports, module.i);
        // A module can be accepted automatically based on its exports, e.g. when
        // it is a Refresh Boundary.
        if (self.$RefreshHelpers$.isReactRefreshBoundary(currentExports)) {
            // Save the previous exports on update so we can compare the boundary
            // signatures.
            module.hot.dispose(function (data) {
                data.prevExports = currentExports;
            });
            // Unconditionally accept an update to this module, we'll check if it's
            // still a Refresh Boundary later.
            module.hot.accept();
            // This field is set when the previous version of this module was a
            // Refresh Boundary, letting us know we need to check for invalidation or
            // enqueue an update.
            if (prevExports !== null) {
                // A boundary can become ineligible if its exports are incompatible
                // with the previous exports.
                //
                // For example, if you add/remove/change exports, we'll want to
                // re-execute the importing modules, and force those components to
                // re-render. Similarly, if you convert a class component to a
                // function, we want to invalidate the boundary.
                if (self.$RefreshHelpers$.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)) {
                    module.hot.invalidate();
                }
                else {
                    self.$RefreshHelpers$.scheduleUpdate();
                }
            }
        }
        else {
            // Since we just executed the code for the module, it's possible that the
            // new exports made it ineligible for being a boundary.
            // We only care about the case when we were _previously_ a boundary,
            // because we already accepted this update (accidental side effect).
            var isNoLongerABoundary = prevExports !== null;
            if (isNoLongerABoundary) {
                module.hot.invalidate();
            }
        }
    }

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../node_modules/next/dist/compiled/webpack/harmony-module.js */ "./node_modules/next/dist/compiled/webpack/harmony-module.js")(module)))

/***/ })

})
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9fTl9FLy4vY29tcG9uZW50cy9DaGVja291dEZvcm0uanMiXSwibmFtZXMiOlsiQ2hlY2tvdXRGb3JtIiwib3JkZXJJRCIsImFtb3VudCIsIm9uU3VjY2VzcyIsInVzZVN0YXRlIiwic3VjY2VlZGVkIiwic2V0U3VjY2VlZGVkIiwiZXJyb3IiLCJzZXRFcnJvciIsInByb2Nlc3NpbmciLCJzZXRQcm9jZXNzaW5nIiwiZGlzYWJsZWQiLCJzZXREaXNhYmxlZCIsImNsaWVudFNlY3JldCIsInNldENsaWVudFNlY3JldCIsInN0cmlwZSIsInVzZVN0cmlwZSIsImVsZW1lbnRzIiwidXNlRWxlbWVudHMiLCJyb3V0ZXIiLCJ1c2VSb3V0ZXIiLCJ1c2VFZmZlY3QiLCJ3aW5kb3ciLCJmZXRjaCIsIm1ldGhvZCIsImhlYWRlcnMiLCJib2R5IiwiSlNPTiIsInN0cmluZ2lmeSIsInRoZW4iLCJyZXMiLCJqc29uIiwiZGF0YSIsImNhcmRTdHlsZSIsInN0eWxlIiwiYmFzZSIsImNvbG9yIiwiZm9udEZhbWlseSIsImZvbnRTbW9vdGhpbmciLCJmb250U2l6ZSIsImludmFsaWQiLCJpY29uQ29sb3IiLCJoYW5kbGVDaGFuZ2UiLCJldmVudCIsImVtcHR5IiwibWVzc2FnZSIsImhhbmRsZVN1Ym1pdCIsImV2IiwicHJldmVudERlZmF1bHQiLCJjb25maXJtQ2FyZFBheW1lbnQiLCJwYXltZW50X21ldGhvZCIsImNhcmQiLCJnZXRFbGVtZW50IiwiQ2FyZEVsZW1lbnQiLCJwYXlsb2FkIiwicGF5bWVudFJlc3VsdCIsImF4aW9zIiwicHV0Il0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBRWUsU0FBU0EsWUFBVCxPQUFvRDtBQUFBOztBQUFBLE1BQTdCQyxPQUE2QixRQUE3QkEsT0FBNkI7QUFBQSxNQUFwQkMsTUFBb0IsUUFBcEJBLE1BQW9CO0FBQUEsTUFBWkMsU0FBWSxRQUFaQSxTQUFZOztBQUFBLGtCQUUvQkMsc0RBQVEsQ0FBQyxLQUFELENBRnVCO0FBQUEsTUFFMURDLFNBRjBEO0FBQUEsTUFFL0NDLFlBRitDOztBQUFBLG1CQUd2Q0Ysc0RBQVEsQ0FBQyxJQUFELENBSCtCO0FBQUEsTUFHMURHLEtBSDBEO0FBQUEsTUFHbkRDLFFBSG1EOztBQUFBLG1CQUk3Qkosc0RBQVEsQ0FBQyxFQUFELENBSnFCO0FBQUEsTUFJMURLLFVBSjBEO0FBQUEsTUFJOUNDLGFBSjhDOztBQUFBLG1CQUtqQ04sc0RBQVEsQ0FBQyxLQUFELENBTHlCO0FBQUEsTUFLMURPLFFBTDBEO0FBQUEsTUFLaERDLFdBTGdEOztBQUFBLG1CQU16QlIsc0RBQVEsQ0FBQyxFQUFELENBTmlCO0FBQUEsTUFNMURTLFlBTjBEO0FBQUEsTUFNNUNDLGVBTjRDOztBQU9qRSxNQUFNQyxNQUFNLEdBQUdDLHlFQUFTLEVBQXhCO0FBQ0EsTUFBTUMsUUFBUSxHQUFHQywyRUFBVyxFQUE1QjtBQUVBLE1BQU1DLE1BQU0sR0FBR0MsNkRBQVMsRUFBeEI7QUFFQUMseURBQVMsQ0FBQyxZQUFNO0FBQ2Q7QUFDQUMsVUFBTSxDQUNIQyxLQURILENBQ1MsbUVBRFQsRUFDOEU7QUFDMUVDLFlBQU0sRUFBRSxNQURrRTtBQUUxRUMsYUFBTyxFQUFFO0FBQ1Asd0JBQWdCO0FBRFQsT0FGaUU7QUFLMUVDLFVBQUksRUFBRUMsSUFBSSxDQUFDQyxTQUFMLENBQWU7QUFBQzFCLGNBQU0sRUFBTkE7QUFBRCxPQUFmO0FBTG9FLEtBRDlFLEVBUUcyQixJQVJILENBUVEsVUFBQUMsR0FBRyxFQUFJO0FBQ1gsYUFBT0EsR0FBRyxDQUFDQyxJQUFKLEVBQVA7QUFDRCxLQVZILEVBV0dGLElBWEgsQ0FXUSxVQUFBRyxJQUFJLEVBQUk7QUFDWmxCLHFCQUFlLENBQUNrQixJQUFJLENBQUNuQixZQUFOLENBQWY7QUFDRCxLQWJIO0FBY0QsR0FoQlEsRUFnQk4sRUFoQk0sQ0FBVDtBQWtCQSxNQUFNb0IsU0FBUyxHQUFHO0FBQ2hCQyxTQUFLLEVBQUU7QUFDTEMsVUFBSSxFQUFFO0FBQ0pDLGFBQUssRUFBRSxTQURIO0FBRUpDLGtCQUFVLEVBQUUscUJBRlI7QUFHSkMscUJBQWEsRUFBRSxhQUhYO0FBSUpDLGdCQUFRLEVBQUUsTUFKTjtBQUtKLHlCQUFpQjtBQUNmSCxlQUFLLEVBQUU7QUFEUTtBQUxiLE9BREQ7QUFVTEksYUFBTyxFQUFFO0FBQ1BKLGFBQUssRUFBRSxTQURBO0FBRVBLLGlCQUFTLEVBQUU7QUFGSjtBQVZKO0FBRFMsR0FBbEI7O0FBa0JBLE1BQU1DLFlBQVk7QUFBQSxtVUFBRyxpQkFBT0MsS0FBUDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRW5CL0IseUJBQVcsQ0FBQytCLEtBQUssQ0FBQ0MsS0FBUCxDQUFYO0FBQ0FwQyxzQkFBUSxDQUFDbUMsS0FBSyxDQUFDcEMsS0FBTixHQUFjb0MsS0FBSyxDQUFDcEMsS0FBTixDQUFZc0MsT0FBMUIsR0FBb0MsRUFBckMsQ0FBUjs7QUFIbUI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsS0FBSDs7QUFBQSxvQkFBWkgsWUFBWTtBQUFBO0FBQUE7QUFBQSxLQUFsQjs7QUFPQSxNQUFNSSxZQUFZO0FBQUEsbVVBQUcsa0JBQU1DLEVBQU47QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRW5CQSxnQkFBRSxDQUFDQyxjQUFIO0FBQ0F0QywyQkFBYSxDQUFDLElBQUQsQ0FBYjtBQUhtQjtBQUFBLHFCQUtHSyxNQUFNLENBQUNrQyxrQkFBUCxDQUEwQnBDLFlBQTFCLEVBQXdDO0FBQzVEcUMsOEJBQWMsRUFBRTtBQUNkQyxzQkFBSSxFQUFFbEMsUUFBUSxDQUFDbUMsVUFBVCxDQUFvQkMsbUVBQXBCO0FBRFE7QUFENEMsZUFBeEMsQ0FMSDs7QUFBQTtBQUtiQyxxQkFMYTs7QUFBQSxtQkFXZkEsT0FBTyxDQUFDL0MsS0FYTztBQUFBO0FBQUE7QUFBQTs7QUFZakJDLHNCQUFRLDBCQUFtQjhDLE9BQU8sQ0FBQy9DLEtBQVIsQ0FBY3NDLE9BQWpDLEVBQVI7QUFDQW5DLDJCQUFhLENBQUMsS0FBRCxDQUFiO0FBYmlCO0FBQUE7O0FBQUE7QUFlakJGLHNCQUFRLENBQUMsSUFBRCxDQUFSO0FBQ0FFLDJCQUFhLENBQUMsS0FBRCxDQUFiO0FBQ0FKLDBCQUFZLENBQUMsSUFBRCxDQUFaO0FBRU1vQixrQkFuQlcsR0FtQko7QUFBQzZCLDZCQUFhLEVBQUVEO0FBQWhCLGVBbkJJO0FBQUE7QUFBQSxxQkFvQlhFLDRDQUFLLENBQUNDLEdBQU4sNkVBQStFeEQsT0FBL0UsR0FBMEZ5QixJQUExRixDQXBCVzs7QUFBQTtBQXNCakJ2Qix1QkFBUzs7QUF0QlE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsS0FBSDs7QUFBQSxvQkFBWjJDLFlBQVk7QUFBQTtBQUFBO0FBQUEsS0FBbEI7O0FBMkJBLHNCQUVFO0FBQU0sWUFBUSxFQUFFQSxZQUFoQjtBQUFBLDRCQUVFLHFFQUFDLG1FQUFEO0FBQWEsUUFBRSxFQUFDLGNBQWhCO0FBQStCLGFBQU8sRUFBRWIsU0FBeEM7QUFBbUQsY0FBUSxFQUFFUztBQUE3RDtBQUFBO0FBQUE7QUFBQTtBQUFBLFlBRkYsZUFHRTtBQUFRLGNBQVEsRUFBRWpDLFVBQVUsSUFBSUUsUUFBZCxJQUEwQk4sU0FBNUM7QUFBdUQsUUFBRSxFQUFDLFFBQTFEO0FBQW1FLGVBQVMsRUFBQyxLQUE3RTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxZQUhGLEVBTUdFLEtBQUssaUJBQ0o7QUFBSyxlQUFTLEVBQUMsWUFBZjtBQUE0QixVQUFJLEVBQUMsT0FBakM7QUFBQSxnQkFDR0E7QUFESDtBQUFBO0FBQUE7QUFBQTtBQUFBLFlBUEo7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLFVBRkY7QUFnQkQ7O0dBbEd1QlAsWTtVQU9QZ0IsaUUsRUFDRUUsbUUsRUFFRkUscUQ7OztLQVZPcEIsWSIsImZpbGUiOiJzdGF0aWMvd2VicGFjay9wYWdlcy9wYXltZW50cy9bb3JkZXJJRF0uMWIzYTJkYmMwN2MwMGM0ODhiNGYuaG90LXVwZGF0ZS5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBSZWFjdCwgeyB1c2VTdGF0ZSwgdXNlRWZmZWN0IH0gZnJvbSBcInJlYWN0XCI7XHJcbmltcG9ydCB7Q2FyZEVsZW1lbnQsIHVzZVN0cmlwZSwgdXNlRWxlbWVudHN9IGZyb20gXCJAc3RyaXBlL3JlYWN0LXN0cmlwZS1qc1wiO1xyXG5pbXBvcnQgYXhpb3MgZnJvbSAnYXhpb3MnO1xyXG5pbXBvcnQge3VzZVJvdXRlcn0gZnJvbSAnbmV4dC9yb3V0ZXInO1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgZnVuY3Rpb24gQ2hlY2tvdXRGb3JtKHtvcmRlcklELCBhbW91bnQsIG9uU3VjY2Vzc30pIHtcclxuXHJcbiAgY29uc3QgW3N1Y2NlZWRlZCwgc2V0U3VjY2VlZGVkXSA9IHVzZVN0YXRlKGZhbHNlKTtcclxuICBjb25zdCBbZXJyb3IsIHNldEVycm9yXSA9IHVzZVN0YXRlKG51bGwpO1xyXG4gIGNvbnN0IFtwcm9jZXNzaW5nLCBzZXRQcm9jZXNzaW5nXSA9IHVzZVN0YXRlKCcnKTtcclxuICBjb25zdCBbZGlzYWJsZWQsIHNldERpc2FibGVkXSA9IHVzZVN0YXRlKGZhbHNlKTtcclxuICBjb25zdCBbY2xpZW50U2VjcmV0LCBzZXRDbGllbnRTZWNyZXRdID0gdXNlU3RhdGUoJycpO1xyXG4gIGNvbnN0IHN0cmlwZSA9IHVzZVN0cmlwZSgpO1xyXG4gIGNvbnN0IGVsZW1lbnRzID0gdXNlRWxlbWVudHMoKTtcclxuXHJcbiAgY29uc3Qgcm91dGVyID0gdXNlUm91dGVyKCk7XHJcblxyXG4gIHVzZUVmZmVjdCgoKSA9PiB7XHJcbiAgICAvLyBDcmVhdGUgUGF5bWVudEludGVudCBhcyBzb29uIGFzIHRoZSBwYWdlIGxvYWRzXHJcbiAgICB3aW5kb3dcclxuICAgICAgLmZldGNoKFwiaHR0cHM6Ly8yOW9kZGVlMmVoLmV4ZWN1dGUtYXBpLnVzLWVhc3QtMS5hbWF6b25hd3MuY29tL2Rldi9zdHJpcGVcIiwge1xyXG4gICAgICAgIG1ldGhvZDogXCJQT1NUXCIsXHJcbiAgICAgICAgaGVhZGVyczoge1xyXG4gICAgICAgICAgXCJDb250ZW50LVR5cGVcIjogXCJhcHBsaWNhdGlvbi9qc29uXCJcclxuICAgICAgICB9LFxyXG4gICAgICAgIGJvZHk6IEpTT04uc3RyaW5naWZ5KHthbW91bnR9KVxyXG4gICAgICB9KVxyXG4gICAgICAudGhlbihyZXMgPT4ge1xyXG4gICAgICAgIHJldHVybiByZXMuanNvbigpO1xyXG4gICAgICB9KVxyXG4gICAgICAudGhlbihkYXRhID0+IHtcclxuICAgICAgICBzZXRDbGllbnRTZWNyZXQoZGF0YS5jbGllbnRTZWNyZXQpO1xyXG4gICAgICB9KTtcclxuICB9LCBbXSk7XHJcblxyXG4gIGNvbnN0IGNhcmRTdHlsZSA9IHtcclxuICAgIHN0eWxlOiB7XHJcbiAgICAgIGJhc2U6IHtcclxuICAgICAgICBjb2xvcjogXCIjMzIzMjVkXCIsXHJcbiAgICAgICAgZm9udEZhbWlseTogJ1BvcHBpbnMsIHNhbnMtc2VyaWYnLFxyXG4gICAgICAgIGZvbnRTbW9vdGhpbmc6IFwiYW50aWFsaWFzZWRcIixcclxuICAgICAgICBmb250U2l6ZTogXCIxNnB4XCIsXHJcbiAgICAgICAgXCI6OnBsYWNlaG9sZGVyXCI6IHtcclxuICAgICAgICAgIGNvbG9yOiBcIiMzMjMyNWRcIlxyXG4gICAgICAgIH1cclxuICAgICAgfSxcclxuICAgICAgaW52YWxpZDoge1xyXG4gICAgICAgIGNvbG9yOiBcIiNmYTc1NWFcIixcclxuICAgICAgICBpY29uQ29sb3I6IFwiI2ZhNzU1YVwiXHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9O1xyXG5cclxuICBjb25zdCBoYW5kbGVDaGFuZ2UgPSBhc3luYyAoZXZlbnQpID0+IHtcclxuXHJcbiAgICBzZXREaXNhYmxlZChldmVudC5lbXB0eSk7XHJcbiAgICBzZXRFcnJvcihldmVudC5lcnJvciA/IGV2ZW50LmVycm9yLm1lc3NhZ2UgOiBcIlwiKTtcclxuXHJcbiAgfTtcclxuXHJcbiAgY29uc3QgaGFuZGxlU3VibWl0ID0gYXN5bmMgZXYgPT4ge1xyXG5cclxuICAgIGV2LnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICBzZXRQcm9jZXNzaW5nKHRydWUpO1xyXG5cclxuICAgIGNvbnN0IHBheWxvYWQgPSBhd2FpdCBzdHJpcGUuY29uZmlybUNhcmRQYXltZW50KGNsaWVudFNlY3JldCwge1xyXG4gICAgICBwYXltZW50X21ldGhvZDoge1xyXG4gICAgICAgIGNhcmQ6IGVsZW1lbnRzLmdldEVsZW1lbnQoQ2FyZEVsZW1lbnQpXHJcbiAgICAgIH1cclxuICAgIH0pO1xyXG5cclxuICAgIGlmIChwYXlsb2FkLmVycm9yKSB7XHJcbiAgICAgIHNldEVycm9yKGBQYXltZW50IGZhaWxlZCAke3BheWxvYWQuZXJyb3IubWVzc2FnZX1gKTtcclxuICAgICAgc2V0UHJvY2Vzc2luZyhmYWxzZSk7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICBzZXRFcnJvcihudWxsKTtcclxuICAgICAgc2V0UHJvY2Vzc2luZyhmYWxzZSk7XHJcbiAgICAgIHNldFN1Y2NlZWRlZCh0cnVlKTtcclxuXHJcbiAgICAgIGNvbnN0IGJvZHkgPSB7cGF5bWVudFJlc3VsdDogcGF5bG9hZH07XHJcbiAgICAgIGF3YWl0IGF4aW9zLnB1dChgaHR0cHM6Ly8yOW9kZGVlMmVoLmV4ZWN1dGUtYXBpLnVzLWVhc3QtMS5hbWF6b25hd3MuY29tL2Rldi9vcmRlcnMvJHtvcmRlcklEfWAsIGJvZHkpO1xyXG5cclxuICAgICAgb25TdWNjZXNzKCk7XHJcbiAgICB9XHJcblxyXG4gIH07XHJcblxyXG4gIHJldHVybiAoXHJcblxyXG4gICAgPGZvcm0gb25TdWJtaXQ9e2hhbmRsZVN1Ym1pdH0+XHJcblxyXG4gICAgICA8Q2FyZEVsZW1lbnQgaWQ9XCJjYXJkLWVsZW1lbnRcIiBvcHRpb25zPXtjYXJkU3R5bGV9IG9uQ2hhbmdlPXtoYW5kbGVDaGFuZ2V9IC8+XHJcbiAgICAgIDxidXR0b24gZGlzYWJsZWQ9e3Byb2Nlc3NpbmcgfHwgZGlzYWJsZWQgfHwgc3VjY2VlZGVkfSBpZD1cInN1Ym1pdFwiIGNsYXNzTmFtZT1cImJ0blwiPlBheSBOb3c8L2J1dHRvbj5cclxuXHJcbiAgICAgIHsvKiBTaG93IGFueSBlcnJvciB0aGF0IGhhcHBlbnMgd2hlbiBwcm9jZXNzaW5nIHRoZSBwYXltZW50ICovfVxyXG4gICAgICB7ZXJyb3IgJiYgKFxyXG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiY2FyZC1lcnJvclwiIHJvbGU9XCJhbGVydFwiPlxyXG4gICAgICAgICAge2Vycm9yfVxyXG4gICAgICAgIDwvZGl2PlxyXG4gICAgICApfVxyXG5cclxuICAgIDwvZm9ybT5cclxuICApO1xyXG59Il0sInNvdXJjZVJvb3QiOiIifQ==