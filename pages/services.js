import React from 'react';
import Head from 'next/head';

export const getStaticProps = (context) => {
    return {
      props: {}
    }
  }

const services = () => {
    return (
        <>
            <Head>
                <title>Arc Tetra Software | Services</title>
                <meta name="description" content="Our Services, including web development and cloud computing"/>
            </Head>

            <div id="landing-alt">
                <div className="container">
                    <h2 className="fs-30p ff-rob ta-center">Our Services</h2>
                    <h1 className="fs-90p ff-rob ta-center">Custom Solutions For Any Project</h1>
                    <p>We have experience with a wide range of technologies and we can provide a custom solution for your precise need. Explore our list of services below and contact us to get started on your project</p>
                </div>
            </div>

            <section className="service-group">
                <div className="service-head">
                    <h2 className="fs-60p ff-rob">Web Development</h2>
                    <p className="fs-18p">We specialize in developing websites that are appealing, responsive, and secure. We can program front-end and back-end solutions.</p>
                </div>
                <div className="service-detail">
                    <i className="fas fa-laptop-code fs-48p co-primary"></i>
                    <h2 className="fs-30p ff-rob">HTML and CSS</h2>
                    <p>We'll use custom-written HTML, CSS, and Javascript to develop your site. This option is ideal for sites requiring a high amount of specialized behaviour.</p>
                </div>
                <div className="service-detail">
                    <i className="fab fa-wordpress fs-48p co-primary"></i>
                    <h2 className="fs-30p ff-rob">Wordpress</h2>
                    <p>We'll use the Wordpress framework for your site. This option is excellent for blogs and is good if you expect to make additions or changes later.</p>
                </div>
            </section>

            <section className="service-group bg-light">
                <div className="service-head">
                    <h2 className="fs-60p ff-rob">Software Solutions</h2>
                    <p className="fs-18p">We are able to write custom software using a variety of technologies. If your business has a need for software, we can provide a solution.</p>
                </div>
                <div className="service-detail">
                    <i className="fas fa-building fs-48p co-primary"></i>
                    <h2 className="fs-30p ff-rob">Business Processes</h2>
                    <p>If you need custom software for a process in your business, we can identify the best tools for the job and provide an optimal solution</p>
                </div>
                <div className="service-detail">
                    <i className="fas fa-database fs-48p co-primary"></i>
                    <h2 className="fs-30p ff-rob">Custom Databases</h2>
                    <p>We can setup a database for your business using the technology of your choice. We specialize in NoSQL databases such as MongoDB.</p>
                </div>
            </section>

            <section className="service-group">
                <div className="service-head">
                    <h2 className="fs-60p ff-rob">Cloud Computing</h2>
                    <p className="fs-18p">We can help your business realize the benefits of moving to the cloud. These include increased realibility and decreased overhead.</p>
                </div>
                <div className="service-detail">
                    <i className="fas fa-server fs-48p co-primary"></i>
                    <h2 className="fs-30p ff-rob">Virtual Servers</h2>
                    <p>We can help you set up a virtual server needed for storing a database or hosting a website. We can help you identify the most cost-effective options.</p>
                </div>
                <div className="service-detail">
                    <i className="fas fa-cloud fs-48p co-primary"></i>
                    <h2 className="fs-30p ff-rob">Serverless Technologies</h2>
                    <p>We specialize in serverless technologies such as AWS Lambda and DynamoDB. Using this tech bypasses the need to worry about servers.</p>
                </div>
            </section>

            <section className="service-group bg-light">
                <div className="service-head">
                    <h2 className="fs-60p ff-rob">Web Hosting / Maintenance</h2>
                    <p className="fs-18p">In addition to developing a website, we can also assist in provisioning and configuring it. We also offer regular maintenance plans.</p>
                </div>
                <div className="service-detail">
                    <i className="fas fa-network-wired fs-48p co-primary"></i>
                    <h2 className="fs-30p ff-rob">Domain Management</h2>
                    <p>We can manage the DNS configuration for your site. We can also configure a CDN in cases where your customers are international.</p>
                </div>
                <div className="service-detail">
                    <i className="fas fa-envelope fs-48p co-primary"></i>
                    <h2 className="fs-30p ff-rob">Business Email Addresses</h2>
                    <p>We can configure professional email addresses for your business, cementing your business's professional image.</p>
                </div>
            </section>
        </>
    )
}

export default services
