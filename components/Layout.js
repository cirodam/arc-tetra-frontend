import React from 'react';
import Head from 'next/head';

import Nav from '../components/Nav';
import MobileNav from '../components/MobileNav';
import Footer from '../components/Footer';

const Layout = ({children}) => {
    return (
        <>
            <Head>
                <meta name="viewport" content="width=device-width,initial-scale=1"/>
                <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css" integrity="sha512-+4zCK9k+qNFUR5X+cKL9EIR+ZOhtIloNl9GIKS57V1MyNsYpYcUrUeQc9vNfzsWfV28IaLL3i96P9sdNyeRssA==" crossOrigin="anonymous" />
            </Head>

            <Nav />
            {children}
            <Footer />
            <MobileNav />
        </>
    )
}

export default Layout
