webpackHotUpdate_N_E("pages/index",{

/***/ "./components/Contact.js":
/*!*******************************!*\
  !*** ./components/Contact.js ***!
  \*******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "./node_modules/react/jsx-dev-runtime.js");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var C_Users_Tyler_Desktop_Projects_arc_tetra_software_v2_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/esm/defineProperty */ "./node_modules/@babel/runtime/helpers/esm/defineProperty.js");
/* harmony import */ var C_Users_Tyler_Desktop_Projects_arc_tetra_software_v2_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/@babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var C_Users_Tyler_Desktop_Projects_arc_tetra_software_v2_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(C_Users_Tyler_Desktop_Projects_arc_tetra_software_v2_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var C_Users_Tyler_Desktop_Projects_arc_tetra_software_v2_node_modules_babel_runtime_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/esm/asyncToGenerator */ "./node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_5__);





var _jsxFileName = "C:\\Users\\Tyler\\Desktop\\Projects\\arc-tetra-software-v2\\components\\Contact.js",
    _this = undefined,
    _s = $RefreshSig$();

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { Object(C_Users_Tyler_Desktop_Projects_arc_tetra_software_v2_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_1__["default"])(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }




var Contact = function Contact() {
  _s();

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_4__["useState"])(false),
      formSubmitted = _useState[0],
      setFormSubmitted = _useState[1];

  var _useState2 = Object(react__WEBPACK_IMPORTED_MODULE_4__["useState"])({
    firstName: "",
    lastName: "",
    email: "",
    message: ""
  }),
      formData = _useState2[0],
      setFormData = _useState2[1];

  var firstName = formData.firstName,
      lastName = formData.lastName,
      email = formData.email,
      message = formData.message;

  var onFormSubmit = /*#__PURE__*/function () {
    var _ref = Object(C_Users_Tyler_Desktop_Projects_arc_tetra_software_v2_node_modules_babel_runtime_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_3__["default"])( /*#__PURE__*/C_Users_Tyler_Desktop_Projects_arc_tetra_software_v2_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_2___default.a.mark(function _callee(e) {
      var body, res;
      return C_Users_Tyler_Desktop_Projects_arc_tetra_software_v2_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_2___default.a.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              e.preventDefault();
              body = {
                firstName: firstName,
                lastName: lastName,
                email: email,
                message: message
              };
              _context.next = 4;
              return axios__WEBPACK_IMPORTED_MODULE_5___default.a.post('https://yw83qq63c0.execute-api.us-east-1.amazonaws.com/dev/contact', body);

            case 4:
              res = _context.sent;
              setFormData({
                firstName: "",
                lastName: "",
                email: "",
                message: ""
              });
              setFormSubmitted(true);

            case 7:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }));

    return function onFormSubmit(_x) {
      return _ref.apply(this, arguments);
    };
  }();

  var onInputChange = function onInputChange(e) {
    setFormData(_objectSpread(_objectSpread({}, formData), {}, Object(C_Users_Tyler_Desktop_Projects_arc_tetra_software_v2_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_1__["default"])({}, e.target.name, e.target.value)));
  };

  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("section", {
    id: "contact",
    children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
      className: "container",
      children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
        id: "contact-info",
        children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("h3", {
          className: "fs-30p ff-rob co-primary",
          children: "Contact Us"
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 39,
          columnNumber: 21
        }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("h2", {
          className: "fs-60p ff-rob",
          children: "Let's Get Started On Your Project"
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 40,
          columnNumber: 21
        }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("p", {
          children: "Use this form to contact us with your project information. We'll get in touch as soon as possible"
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 41,
          columnNumber: 21
        }, _this)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 38,
        columnNumber: 17
      }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("form", {
        onSubmit: function onSubmit(e) {
          return onFormSubmit(e);
        },
        id: "contact-form",
        children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
          className: "form-row",
          children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("input", {
            type: "text",
            name: "firstName",
            id: "first-name",
            placeholder: "First Name",
            className: "input",
            value: firstName,
            onChange: function onChange(e) {
              return onInputChange(e);
            }
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 46,
            columnNumber: 25
          }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("input", {
            type: "text",
            name: "lastName",
            id: "last-name",
            placeholder: "Last Name",
            className: "input",
            value: lastName,
            onChange: function onChange(e) {
              return onInputChange(e);
            }
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 47,
            columnNumber: 25
          }, _this)]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 45,
          columnNumber: 21
        }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("input", {
          type: "email",
          name: "email",
          id: "email",
          placeholder: "Email Address",
          className: "input input-double",
          value: email,
          onChange: function onChange(e) {
            return onInputChange(e);
          }
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 49,
          columnNumber: 21
        }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("textarea", {
          name: "message",
          id: "message",
          placeholder: "Message",
          className: "input input-textarea input-double",
          value: message,
          onChange: function onChange(e) {
            return onInputChange(e);
          }
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 50,
          columnNumber: 21
        }, _this), formSubmitted && /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("p", {
          children: "Form Submitted!"
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 51,
          columnNumber: 39
        }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("button", {
          type: "submit",
          className: "btn",
          children: "Contact Us"
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 52,
          columnNumber: 21
        }, _this)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 44,
        columnNumber: 17
      }, _this)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 36,
      columnNumber: 13
    }, _this)
  }, void 0, false, {
    fileName: _jsxFileName,
    lineNumber: 35,
    columnNumber: 9
  }, _this);
};

_s(Contact, "sRHxVIIID6rhV8lLi2+mR4UntG0=");

_c = Contact;
/* harmony default export */ __webpack_exports__["default"] = (Contact);

var _c;

$RefreshReg$(_c, "Contact");

;
    var _a, _b;
    // Legacy CSS implementations will `eval` browser code in a Node.js context
    // to extract CSS. For backwards compatibility, we need to check we're in a
    // browser context before continuing.
    if (typeof self !== 'undefined' &&
        // AMP / No-JS mode does not inject these helpers:
        '$RefreshHelpers$' in self) {
        var currentExports = module.__proto__.exports;
        var prevExports = (_b = (_a = module.hot.data) === null || _a === void 0 ? void 0 : _a.prevExports) !== null && _b !== void 0 ? _b : null;
        // This cannot happen in MainTemplate because the exports mismatch between
        // templating and execution.
        self.$RefreshHelpers$.registerExportsForReactRefresh(currentExports, module.i);
        // A module can be accepted automatically based on its exports, e.g. when
        // it is a Refresh Boundary.
        if (self.$RefreshHelpers$.isReactRefreshBoundary(currentExports)) {
            // Save the previous exports on update so we can compare the boundary
            // signatures.
            module.hot.dispose(function (data) {
                data.prevExports = currentExports;
            });
            // Unconditionally accept an update to this module, we'll check if it's
            // still a Refresh Boundary later.
            module.hot.accept();
            // This field is set when the previous version of this module was a
            // Refresh Boundary, letting us know we need to check for invalidation or
            // enqueue an update.
            if (prevExports !== null) {
                // A boundary can become ineligible if its exports are incompatible
                // with the previous exports.
                //
                // For example, if you add/remove/change exports, we'll want to
                // re-execute the importing modules, and force those components to
                // re-render. Similarly, if you convert a class component to a
                // function, we want to invalidate the boundary.
                if (self.$RefreshHelpers$.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)) {
                    module.hot.invalidate();
                }
                else {
                    self.$RefreshHelpers$.scheduleUpdate();
                }
            }
        }
        else {
            // Since we just executed the code for the module, it's possible that the
            // new exports made it ineligible for being a boundary.
            // We only care about the case when we were _previously_ a boundary,
            // because we already accepted this update (accidental side effect).
            var isNoLongerABoundary = prevExports !== null;
            if (isNoLongerABoundary) {
                module.hot.invalidate();
            }
        }
    }

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../node_modules/next/dist/compiled/webpack/harmony-module.js */ "./node_modules/next/dist/compiled/webpack/harmony-module.js")(module)))

/***/ })

})
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9fTl9FLy4vY29tcG9uZW50cy9Db250YWN0LmpzIl0sIm5hbWVzIjpbIkNvbnRhY3QiLCJ1c2VTdGF0ZSIsImZvcm1TdWJtaXR0ZWQiLCJzZXRGb3JtU3VibWl0dGVkIiwiZmlyc3ROYW1lIiwibGFzdE5hbWUiLCJlbWFpbCIsIm1lc3NhZ2UiLCJmb3JtRGF0YSIsInNldEZvcm1EYXRhIiwib25Gb3JtU3VibWl0IiwiZSIsInByZXZlbnREZWZhdWx0IiwiYm9keSIsImF4aW9zIiwicG9zdCIsInJlcyIsIm9uSW5wdXRDaGFuZ2UiLCJ0YXJnZXQiLCJuYW1lIiwidmFsdWUiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUNBOztBQUVBLElBQU1BLE9BQU8sR0FBRyxTQUFWQSxPQUFVLEdBQU07QUFBQTs7QUFBQSxrQkFFd0JDLHNEQUFRLENBQUMsS0FBRCxDQUZoQztBQUFBLE1BRVhDLGFBRlc7QUFBQSxNQUVJQyxnQkFGSjs7QUFBQSxtQkFHY0Ysc0RBQVEsQ0FBQztBQUN2Q0csYUFBUyxFQUFFLEVBRDRCO0FBRXZDQyxZQUFRLEVBQUUsRUFGNkI7QUFHdkNDLFNBQUssRUFBRSxFQUhnQztBQUl2Q0MsV0FBTyxFQUFFO0FBSjhCLEdBQUQsQ0FIdEI7QUFBQSxNQUdYQyxRQUhXO0FBQUEsTUFHREMsV0FIQzs7QUFBQSxNQVNYTCxTQVRXLEdBUzRCSSxRQVQ1QixDQVNYSixTQVRXO0FBQUEsTUFTQUMsUUFUQSxHQVM0QkcsUUFUNUIsQ0FTQUgsUUFUQTtBQUFBLE1BU1VDLEtBVFYsR0FTNEJFLFFBVDVCLENBU1VGLEtBVFY7QUFBQSxNQVNpQkMsT0FUakIsR0FTNEJDLFFBVDVCLENBU2lCRCxPQVRqQjs7QUFXbEIsTUFBTUcsWUFBWTtBQUFBLGtVQUFHLGlCQUFNQyxDQUFOO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNqQkEsZUFBQyxDQUFDQyxjQUFGO0FBRU1DLGtCQUhXLEdBR0o7QUFBQ1QseUJBQVMsRUFBVEEsU0FBRDtBQUFZQyx3QkFBUSxFQUFSQSxRQUFaO0FBQXNCQyxxQkFBSyxFQUFMQSxLQUF0QjtBQUE2QkMsdUJBQU8sRUFBUEE7QUFBN0IsZUFISTtBQUFBO0FBQUEscUJBSUNPLDRDQUFLLENBQUNDLElBQU4sQ0FBVyxvRUFBWCxFQUFpRkYsSUFBakYsQ0FKRDs7QUFBQTtBQUlYRyxpQkFKVztBQUtqQlAseUJBQVcsQ0FBQztBQUNWTCx5QkFBUyxFQUFFLEVBREQ7QUFFVkMsd0JBQVEsRUFBRSxFQUZBO0FBR1ZDLHFCQUFLLEVBQUUsRUFIRztBQUlWQyx1QkFBTyxFQUFFO0FBSkMsZUFBRCxDQUFYO0FBTUFKLDhCQUFnQixDQUFDLElBQUQsQ0FBaEI7O0FBWGlCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLEtBQUg7O0FBQUEsb0JBQVpPLFlBQVk7QUFBQTtBQUFBO0FBQUEsS0FBbEI7O0FBZUEsTUFBTU8sYUFBYSxHQUFHLFNBQWhCQSxhQUFnQixDQUFBTixDQUFDLEVBQUk7QUFDdkJGLGVBQVcsaUNBQUtELFFBQUwsc0tBQWdCRyxDQUFDLENBQUNPLE1BQUYsQ0FBU0MsSUFBekIsRUFBZ0NSLENBQUMsQ0FBQ08sTUFBRixDQUFTRSxLQUF6QyxHQUFYO0FBQ0QsR0FGSDs7QUFJQSxzQkFDSTtBQUFTLE1BQUUsRUFBQyxTQUFaO0FBQUEsMkJBQ0k7QUFBSyxlQUFTLEVBQUMsV0FBZjtBQUFBLDhCQUVJO0FBQUssVUFBRSxFQUFDLGNBQVI7QUFBQSxnQ0FDSTtBQUFJLG1CQUFTLEVBQUMsMEJBQWQ7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsaUJBREosZUFFSTtBQUFJLG1CQUFTLEVBQUMsZUFBZDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxpQkFGSixlQUdJO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQUhKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxlQUZKLGVBUUk7QUFBTSxnQkFBUSxFQUFFLGtCQUFDVCxDQUFEO0FBQUEsaUJBQU9ELFlBQVksQ0FBQ0MsQ0FBRCxDQUFuQjtBQUFBLFNBQWhCO0FBQXdDLFVBQUUsRUFBQyxjQUEzQztBQUFBLGdDQUNJO0FBQUssbUJBQVMsRUFBQyxVQUFmO0FBQUEsa0NBQ0k7QUFBTyxnQkFBSSxFQUFDLE1BQVo7QUFBbUIsZ0JBQUksRUFBQyxXQUF4QjtBQUFvQyxjQUFFLEVBQUMsWUFBdkM7QUFBb0QsdUJBQVcsRUFBQyxZQUFoRTtBQUE2RSxxQkFBUyxFQUFDLE9BQXZGO0FBQStGLGlCQUFLLEVBQUVQLFNBQXRHO0FBQWlILG9CQUFRLEVBQUUsa0JBQUFPLENBQUM7QUFBQSxxQkFBSU0sYUFBYSxDQUFDTixDQUFELENBQWpCO0FBQUE7QUFBNUg7QUFBQTtBQUFBO0FBQUE7QUFBQSxtQkFESixlQUVJO0FBQU8sZ0JBQUksRUFBQyxNQUFaO0FBQW1CLGdCQUFJLEVBQUMsVUFBeEI7QUFBbUMsY0FBRSxFQUFDLFdBQXRDO0FBQWtELHVCQUFXLEVBQUMsV0FBOUQ7QUFBMEUscUJBQVMsRUFBQyxPQUFwRjtBQUE0RixpQkFBSyxFQUFFTixRQUFuRztBQUE2RyxvQkFBUSxFQUFFLGtCQUFBTSxDQUFDO0FBQUEscUJBQUlNLGFBQWEsQ0FBQ04sQ0FBRCxDQUFqQjtBQUFBO0FBQXhIO0FBQUE7QUFBQTtBQUFBO0FBQUEsbUJBRko7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQURKLGVBS0k7QUFBTyxjQUFJLEVBQUMsT0FBWjtBQUFvQixjQUFJLEVBQUMsT0FBekI7QUFBaUMsWUFBRSxFQUFDLE9BQXBDO0FBQTRDLHFCQUFXLEVBQUMsZUFBeEQ7QUFBd0UsbUJBQVMsRUFBQyxvQkFBbEY7QUFBdUcsZUFBSyxFQUFFTCxLQUE5RztBQUFxSCxrQkFBUSxFQUFFLGtCQUFBSyxDQUFDO0FBQUEsbUJBQUlNLGFBQWEsQ0FBQ04sQ0FBRCxDQUFqQjtBQUFBO0FBQWhJO0FBQUE7QUFBQTtBQUFBO0FBQUEsaUJBTEosZUFNSTtBQUFVLGNBQUksRUFBQyxTQUFmO0FBQXlCLFlBQUUsRUFBQyxTQUE1QjtBQUFzQyxxQkFBVyxFQUFDLFNBQWxEO0FBQTRELG1CQUFTLEVBQUMsbUNBQXRFO0FBQTBHLGVBQUssRUFBRUosT0FBakg7QUFBMEgsa0JBQVEsRUFBRSxrQkFBQUksQ0FBQztBQUFBLG1CQUFJTSxhQUFhLENBQUNOLENBQUQsQ0FBakI7QUFBQTtBQUFySTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQU5KLEVBT0tULGFBQWEsaUJBQUk7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsaUJBUHRCLGVBUUk7QUFBUSxjQUFJLEVBQUMsUUFBYjtBQUFzQixtQkFBUyxFQUFDLEtBQWhDO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQVJKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxlQVJKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURKO0FBQUE7QUFBQTtBQUFBO0FBQUEsV0FESjtBQXdCSCxDQXRERDs7R0FBTUYsTzs7S0FBQUEsTztBQXdEU0Esc0VBQWYiLCJmaWxlIjoic3RhdGljL3dlYnBhY2svcGFnZXMvaW5kZXguMjQ4NDkxMTAxYTQ4NTE2YjE2Y2MuaG90LXVwZGF0ZS5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBSZWFjdCwge3VzZVN0YXRlfSBmcm9tICdyZWFjdCc7XHJcbmltcG9ydCBheGlvcyBmcm9tICdheGlvcyc7XHJcblxyXG5jb25zdCBDb250YWN0ID0gKCkgPT4ge1xyXG5cclxuICAgIGNvbnN0IFtmb3JtU3VibWl0dGVkLCBzZXRGb3JtU3VibWl0dGVkXSA9IHVzZVN0YXRlKGZhbHNlKTtcclxuICAgIGNvbnN0IFtmb3JtRGF0YSwgc2V0Rm9ybURhdGFdID0gdXNlU3RhdGUoe1xyXG4gICAgICBmaXJzdE5hbWU6IFwiXCIsXHJcbiAgICAgIGxhc3ROYW1lOiBcIlwiLFxyXG4gICAgICBlbWFpbDogXCJcIixcclxuICAgICAgbWVzc2FnZTogXCJcIlxyXG4gICAgfSlcclxuICAgIGNvbnN0IHtmaXJzdE5hbWUsIGxhc3ROYW1lLCBlbWFpbCwgbWVzc2FnZX0gPSBmb3JtRGF0YTtcclxuXHJcbiAgICBjb25zdCBvbkZvcm1TdWJtaXQgPSBhc3luYyBlID0+IHtcclxuICAgICAgICBlLnByZXZlbnREZWZhdWx0KCk7XHJcblxyXG4gICAgICAgIGNvbnN0IGJvZHkgPSB7Zmlyc3ROYW1lLCBsYXN0TmFtZSwgZW1haWwsIG1lc3NhZ2V9O1xyXG4gICAgICAgIGNvbnN0IHJlcyA9IGF3YWl0IGF4aW9zLnBvc3QoJ2h0dHBzOi8veXc4M3FxNjNjMC5leGVjdXRlLWFwaS51cy1lYXN0LTEuYW1hem9uYXdzLmNvbS9kZXYvY29udGFjdCcsIGJvZHkpO1xyXG4gICAgICAgIHNldEZvcm1EYXRhKHtcclxuICAgICAgICAgIGZpcnN0TmFtZTogXCJcIixcclxuICAgICAgICAgIGxhc3ROYW1lOiBcIlwiLFxyXG4gICAgICAgICAgZW1haWw6IFwiXCIsXHJcbiAgICAgICAgICBtZXNzYWdlOiBcIlwiXHJcbiAgICAgICAgfSlcclxuICAgICAgICBzZXRGb3JtU3VibWl0dGVkKHRydWUpO1xyXG5cclxuICAgIH1cclxuXHJcbiAgICBjb25zdCBvbklucHV0Q2hhbmdlID0gZSA9PiB7XHJcbiAgICAgICAgc2V0Rm9ybURhdGEoey4uLmZvcm1EYXRhLCBbZS50YXJnZXQubmFtZV06IGUudGFyZ2V0LnZhbHVlfSk7XHJcbiAgICAgIH1cclxuXHJcbiAgICByZXR1cm4gKFxyXG4gICAgICAgIDxzZWN0aW9uIGlkPVwiY29udGFjdFwiPlxyXG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImNvbnRhaW5lclwiPlxyXG5cclxuICAgICAgICAgICAgICAgIDxkaXYgaWQ9XCJjb250YWN0LWluZm9cIj5cclxuICAgICAgICAgICAgICAgICAgICA8aDMgY2xhc3NOYW1lPVwiZnMtMzBwIGZmLXJvYiBjby1wcmltYXJ5XCI+Q29udGFjdCBVczwvaDM+XHJcbiAgICAgICAgICAgICAgICAgICAgPGgyIGNsYXNzTmFtZT1cImZzLTYwcCBmZi1yb2JcIj5MZXQncyBHZXQgU3RhcnRlZCBPbiBZb3VyIFByb2plY3Q8L2gyPlxyXG4gICAgICAgICAgICAgICAgICAgIDxwPlVzZSB0aGlzIGZvcm0gdG8gY29udGFjdCB1cyB3aXRoIHlvdXIgcHJvamVjdCBpbmZvcm1hdGlvbi4gV2UnbGwgZ2V0IGluIHRvdWNoIGFzIHNvb24gYXMgcG9zc2libGU8L3A+XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuXHJcbiAgICAgICAgICAgICAgICA8Zm9ybSBvblN1Ym1pdD17KGUpID0+IG9uRm9ybVN1Ym1pdChlKX0gaWQ9XCJjb250YWN0LWZvcm1cIj5cclxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImZvcm0tcm93XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxpbnB1dCB0eXBlPVwidGV4dFwiIG5hbWU9XCJmaXJzdE5hbWVcIiBpZD1cImZpcnN0LW5hbWVcIiBwbGFjZWhvbGRlcj1cIkZpcnN0IE5hbWVcIiBjbGFzc05hbWU9XCJpbnB1dFwiIHZhbHVlPXtmaXJzdE5hbWV9IG9uQ2hhbmdlPXtlID0+IG9uSW5wdXRDaGFuZ2UoZSl9IC8+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxpbnB1dCB0eXBlPVwidGV4dFwiIG5hbWU9XCJsYXN0TmFtZVwiIGlkPVwibGFzdC1uYW1lXCIgcGxhY2Vob2xkZXI9XCJMYXN0IE5hbWVcIiBjbGFzc05hbWU9XCJpbnB1dFwiIHZhbHVlPXtsYXN0TmFtZX0gb25DaGFuZ2U9e2UgPT4gb25JbnB1dENoYW5nZShlKX0gLz5cclxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICA8aW5wdXQgdHlwZT1cImVtYWlsXCIgbmFtZT1cImVtYWlsXCIgaWQ9XCJlbWFpbFwiIHBsYWNlaG9sZGVyPVwiRW1haWwgQWRkcmVzc1wiIGNsYXNzTmFtZT1cImlucHV0IGlucHV0LWRvdWJsZVwiIHZhbHVlPXtlbWFpbH0gb25DaGFuZ2U9e2UgPT4gb25JbnB1dENoYW5nZShlKX0gLz5cclxuICAgICAgICAgICAgICAgICAgICA8dGV4dGFyZWEgbmFtZT1cIm1lc3NhZ2VcIiBpZD1cIm1lc3NhZ2VcIiBwbGFjZWhvbGRlcj1cIk1lc3NhZ2VcIiBjbGFzc05hbWU9XCJpbnB1dCBpbnB1dC10ZXh0YXJlYSBpbnB1dC1kb3VibGVcIiB2YWx1ZT17bWVzc2FnZX0gb25DaGFuZ2U9e2UgPT4gb25JbnB1dENoYW5nZShlKX0gPjwvdGV4dGFyZWE+XHJcbiAgICAgICAgICAgICAgICAgICAge2Zvcm1TdWJtaXR0ZWQgJiYgPHA+Rm9ybSBTdWJtaXR0ZWQhPC9wPn1cclxuICAgICAgICAgICAgICAgICAgICA8YnV0dG9uIHR5cGU9XCJzdWJtaXRcIiBjbGFzc05hbWU9XCJidG5cIj5Db250YWN0IFVzPC9idXR0b24+XHJcbiAgICAgICAgICAgICAgICA8L2Zvcm0+XHJcblxyXG4gICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICA8L3NlY3Rpb24+XHJcbiAgICApXHJcbn1cclxuXHJcbmV4cG9ydCBkZWZhdWx0IENvbnRhY3RcclxuIl0sInNvdXJjZVJvb3QiOiIifQ==