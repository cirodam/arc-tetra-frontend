import React from 'react';
import Link from 'next/Link';

const Nav = () => {
    return (
        <div id="nav">
            <img src='/logo.png' alt="Logo"/>
            <ul>
                <li><Link href="/"><a className="fs-16p">Home</a></Link></li>
                <li><Link href="/services"><a className="fs-16p">Services</a></Link></li>
                <li><Link href="/payments"><a className="fs-16p">Payments</a></Link></li>
            </ul>
        </div>
    )
}

export default Nav
