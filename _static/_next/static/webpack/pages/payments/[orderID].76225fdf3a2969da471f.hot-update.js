webpackHotUpdate_N_E("pages/payments/[orderID]",{

/***/ "./components/CheckoutForm.js":
/*!************************************!*\
  !*** ./components/CheckoutForm.js ***!
  \************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return CheckoutForm; });
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "./node_modules/react/jsx-dev-runtime.js");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var C_Users_Tyler_Desktop_Projects_arc_tetra_software_v2_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/@babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var C_Users_Tyler_Desktop_Projects_arc_tetra_software_v2_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(C_Users_Tyler_Desktop_Projects_arc_tetra_software_v2_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var C_Users_Tyler_Desktop_Projects_arc_tetra_software_v2_node_modules_babel_runtime_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/esm/asyncToGenerator */ "./node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _stripe_react_stripe_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @stripe/react-stripe-js */ "./node_modules/@stripe/react-stripe-js/dist/react-stripe.umd.js");
/* harmony import */ var _stripe_react_stripe_js__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_stripe_react_stripe_js__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_5__);




var _jsxFileName = "C:\\Users\\Tyler\\Desktop\\Projects\\arc-tetra-software-v2\\components\\CheckoutForm.js",
    _s = $RefreshSig$();




function CheckoutForm(_ref) {
  _s();

  var orderID = _ref.orderID,
      amount = _ref.amount;

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_3__["useState"])(false),
      succeeded = _useState[0],
      setSucceeded = _useState[1];

  var _useState2 = Object(react__WEBPACK_IMPORTED_MODULE_3__["useState"])(null),
      error = _useState2[0],
      setError = _useState2[1];

  var _useState3 = Object(react__WEBPACK_IMPORTED_MODULE_3__["useState"])(''),
      processing = _useState3[0],
      setProcessing = _useState3[1];

  var _useState4 = Object(react__WEBPACK_IMPORTED_MODULE_3__["useState"])(false),
      disabled = _useState4[0],
      setDisabled = _useState4[1];

  var _useState5 = Object(react__WEBPACK_IMPORTED_MODULE_3__["useState"])(''),
      clientSecret = _useState5[0],
      setClientSecret = _useState5[1];

  var stripe = Object(_stripe_react_stripe_js__WEBPACK_IMPORTED_MODULE_4__["useStripe"])();
  var elements = Object(_stripe_react_stripe_js__WEBPACK_IMPORTED_MODULE_4__["useElements"])();
  Object(react__WEBPACK_IMPORTED_MODULE_3__["useEffect"])(function () {
    // Create PaymentIntent as soon as the page loads
    window.fetch("https://29oddee2eh.execute-api.us-east-1.amazonaws.com/dev/stripe", {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        amount: amount
      })
    }).then(function (res) {
      return res.json();
    }).then(function (data) {
      setClientSecret(data.clientSecret);
    });
  }, []);
  var cardStyle = {
    style: {
      base: {
        color: "#32325d",
        fontFamily: 'Poppins, sans-serif',
        fontSmoothing: "antialiased",
        fontSize: "16px",
        "::placeholder": {
          color: "#32325d"
        }
      },
      invalid: {
        color: "#fa755a",
        iconColor: "#fa755a"
      }
    }
  };

  var handleChange = /*#__PURE__*/function () {
    var _ref2 = Object(C_Users_Tyler_Desktop_Projects_arc_tetra_software_v2_node_modules_babel_runtime_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_2__["default"])( /*#__PURE__*/C_Users_Tyler_Desktop_Projects_arc_tetra_software_v2_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.mark(function _callee(event) {
      return C_Users_Tyler_Desktop_Projects_arc_tetra_software_v2_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              setDisabled(event.empty);
              setError(event.error ? event.error.message : "");

            case 2:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }));

    return function handleChange(_x) {
      return _ref2.apply(this, arguments);
    };
  }();

  var handleSubmit = /*#__PURE__*/function () {
    var _ref3 = Object(C_Users_Tyler_Desktop_Projects_arc_tetra_software_v2_node_modules_babel_runtime_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_2__["default"])( /*#__PURE__*/C_Users_Tyler_Desktop_Projects_arc_tetra_software_v2_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.mark(function _callee2(ev) {
      var payload, body;
      return C_Users_Tyler_Desktop_Projects_arc_tetra_software_v2_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.wrap(function _callee2$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
              ev.preventDefault();
              setProcessing(true);
              _context2.next = 4;
              return stripe.confirmCardPayment(clientSecret, {
                payment_method: {
                  card: elements.getElement(_stripe_react_stripe_js__WEBPACK_IMPORTED_MODULE_4__["CardElement"])
                }
              });

            case 4:
              payload = _context2.sent;

              if (!payload.error) {
                _context2.next = 10;
                break;
              }

              setError("Payment failed ".concat(payload.error.message));
              setProcessing(false);
              _context2.next = 16;
              break;

            case 10:
              setError(null);
              setProcessing(false);
              setSucceeded(true);
              body = {
                paymentResult: payload
              };
              _context2.next = 16;
              return axios__WEBPACK_IMPORTED_MODULE_5___default.a.put("https://29oddee2eh.execute-api.us-east-1.amazonaws.com/dev/orders/".concat(orderID), body);

            case 16:
            case "end":
              return _context2.stop();
          }
        }
      }, _callee2);
    }));

    return function handleSubmit(_x2) {
      return _ref3.apply(this, arguments);
    };
  }();

  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("section", {
    id: "stripe",
    children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("h2", {
      className: "fs-60p ff-rob ta-center",
      children: "Pay Now"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 86,
      columnNumber: 5
    }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(Elements, {
      stripe: promise,
      children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("form", {
        id: "payment-form",
        onSubmit: handleSubmit,
        children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_stripe_react_stripe_js__WEBPACK_IMPORTED_MODULE_4__["CardElement"], {
          id: "card-element",
          options: cardStyle,
          onChange: handleChange
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 90,
          columnNumber: 13
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("button", {
          disabled: processing || disabled || succeeded,
          id: "submit",
          children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("span", {
            id: "button-text",
            children: processing ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
              className: "spinner",
              id: "spinner"
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 94,
              columnNumber: 13
            }, this) : "Pay now"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 92,
            columnNumber: 13
          }, this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 91,
          columnNumber: 13
        }, this), error && /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
          className: "card-error",
          role: "alert",
          children: error
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 103,
          columnNumber: 13
        }, this)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 88,
        columnNumber: 9
      }, this)
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 87,
      columnNumber: 5
    }, this)]
  }, void 0, true, {
    fileName: _jsxFileName,
    lineNumber: 85,
    columnNumber: 5
  }, this);
}

_s(CheckoutForm, "GhxqeXSXbsPzU6SqFIQHLcFON5I=", false, function () {
  return [_stripe_react_stripe_js__WEBPACK_IMPORTED_MODULE_4__["useStripe"], _stripe_react_stripe_js__WEBPACK_IMPORTED_MODULE_4__["useElements"]];
});

_c = CheckoutForm;

var _c;

$RefreshReg$(_c, "CheckoutForm");

;
    var _a, _b;
    // Legacy CSS implementations will `eval` browser code in a Node.js context
    // to extract CSS. For backwards compatibility, we need to check we're in a
    // browser context before continuing.
    if (typeof self !== 'undefined' &&
        // AMP / No-JS mode does not inject these helpers:
        '$RefreshHelpers$' in self) {
        var currentExports = module.__proto__.exports;
        var prevExports = (_b = (_a = module.hot.data) === null || _a === void 0 ? void 0 : _a.prevExports) !== null && _b !== void 0 ? _b : null;
        // This cannot happen in MainTemplate because the exports mismatch between
        // templating and execution.
        self.$RefreshHelpers$.registerExportsForReactRefresh(currentExports, module.i);
        // A module can be accepted automatically based on its exports, e.g. when
        // it is a Refresh Boundary.
        if (self.$RefreshHelpers$.isReactRefreshBoundary(currentExports)) {
            // Save the previous exports on update so we can compare the boundary
            // signatures.
            module.hot.dispose(function (data) {
                data.prevExports = currentExports;
            });
            // Unconditionally accept an update to this module, we'll check if it's
            // still a Refresh Boundary later.
            module.hot.accept();
            // This field is set when the previous version of this module was a
            // Refresh Boundary, letting us know we need to check for invalidation or
            // enqueue an update.
            if (prevExports !== null) {
                // A boundary can become ineligible if its exports are incompatible
                // with the previous exports.
                //
                // For example, if you add/remove/change exports, we'll want to
                // re-execute the importing modules, and force those components to
                // re-render. Similarly, if you convert a class component to a
                // function, we want to invalidate the boundary.
                if (self.$RefreshHelpers$.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)) {
                    module.hot.invalidate();
                }
                else {
                    self.$RefreshHelpers$.scheduleUpdate();
                }
            }
        }
        else {
            // Since we just executed the code for the module, it's possible that the
            // new exports made it ineligible for being a boundary.
            // We only care about the case when we were _previously_ a boundary,
            // because we already accepted this update (accidental side effect).
            var isNoLongerABoundary = prevExports !== null;
            if (isNoLongerABoundary) {
                module.hot.invalidate();
            }
        }
    }

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../node_modules/next/dist/compiled/webpack/harmony-module.js */ "./node_modules/next/dist/compiled/webpack/harmony-module.js")(module)))

/***/ })

})
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9fTl9FLy4vY29tcG9uZW50cy9DaGVja291dEZvcm0uanMiXSwibmFtZXMiOlsiQ2hlY2tvdXRGb3JtIiwib3JkZXJJRCIsImFtb3VudCIsInVzZVN0YXRlIiwic3VjY2VlZGVkIiwic2V0U3VjY2VlZGVkIiwiZXJyb3IiLCJzZXRFcnJvciIsInByb2Nlc3NpbmciLCJzZXRQcm9jZXNzaW5nIiwiZGlzYWJsZWQiLCJzZXREaXNhYmxlZCIsImNsaWVudFNlY3JldCIsInNldENsaWVudFNlY3JldCIsInN0cmlwZSIsInVzZVN0cmlwZSIsImVsZW1lbnRzIiwidXNlRWxlbWVudHMiLCJ1c2VFZmZlY3QiLCJ3aW5kb3ciLCJmZXRjaCIsIm1ldGhvZCIsImhlYWRlcnMiLCJib2R5IiwiSlNPTiIsInN0cmluZ2lmeSIsInRoZW4iLCJyZXMiLCJqc29uIiwiZGF0YSIsImNhcmRTdHlsZSIsInN0eWxlIiwiYmFzZSIsImNvbG9yIiwiZm9udEZhbWlseSIsImZvbnRTbW9vdGhpbmciLCJmb250U2l6ZSIsImludmFsaWQiLCJpY29uQ29sb3IiLCJoYW5kbGVDaGFuZ2UiLCJldmVudCIsImVtcHR5IiwibWVzc2FnZSIsImhhbmRsZVN1Ym1pdCIsImV2IiwicHJldmVudERlZmF1bHQiLCJjb25maXJtQ2FyZFBheW1lbnQiLCJwYXltZW50X21ldGhvZCIsImNhcmQiLCJnZXRFbGVtZW50IiwiQ2FyZEVsZW1lbnQiLCJwYXlsb2FkIiwicGF5bWVudFJlc3VsdCIsImF4aW9zIiwicHV0IiwicHJvbWlzZSJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7QUFDQTtBQUNBO0FBRWUsU0FBU0EsWUFBVCxPQUF5QztBQUFBOztBQUFBLE1BQWxCQyxPQUFrQixRQUFsQkEsT0FBa0I7QUFBQSxNQUFUQyxNQUFTLFFBQVRBLE1BQVM7O0FBQUEsa0JBRXBCQyxzREFBUSxDQUFDLEtBQUQsQ0FGWTtBQUFBLE1BRS9DQyxTQUYrQztBQUFBLE1BRXBDQyxZQUZvQzs7QUFBQSxtQkFHNUJGLHNEQUFRLENBQUMsSUFBRCxDQUhvQjtBQUFBLE1BRy9DRyxLQUgrQztBQUFBLE1BR3hDQyxRQUh3Qzs7QUFBQSxtQkFJbEJKLHNEQUFRLENBQUMsRUFBRCxDQUpVO0FBQUEsTUFJL0NLLFVBSitDO0FBQUEsTUFJbkNDLGFBSm1DOztBQUFBLG1CQUt0Qk4sc0RBQVEsQ0FBQyxLQUFELENBTGM7QUFBQSxNQUsvQ08sUUFMK0M7QUFBQSxNQUtyQ0MsV0FMcUM7O0FBQUEsbUJBTWRSLHNEQUFRLENBQUMsRUFBRCxDQU5NO0FBQUEsTUFNL0NTLFlBTitDO0FBQUEsTUFNakNDLGVBTmlDOztBQU90RCxNQUFNQyxNQUFNLEdBQUdDLHlFQUFTLEVBQXhCO0FBQ0EsTUFBTUMsUUFBUSxHQUFHQywyRUFBVyxFQUE1QjtBQUVBQyx5REFBUyxDQUFDLFlBQU07QUFDZDtBQUNBQyxVQUFNLENBQ0hDLEtBREgsQ0FDUyxtRUFEVCxFQUM4RTtBQUMxRUMsWUFBTSxFQUFFLE1BRGtFO0FBRTFFQyxhQUFPLEVBQUU7QUFDUCx3QkFBZ0I7QUFEVCxPQUZpRTtBQUsxRUMsVUFBSSxFQUFFQyxJQUFJLENBQUNDLFNBQUwsQ0FBZTtBQUFDdkIsY0FBTSxFQUFOQTtBQUFELE9BQWY7QUFMb0UsS0FEOUUsRUFRR3dCLElBUkgsQ0FRUSxVQUFBQyxHQUFHLEVBQUk7QUFDWCxhQUFPQSxHQUFHLENBQUNDLElBQUosRUFBUDtBQUNELEtBVkgsRUFXR0YsSUFYSCxDQVdRLFVBQUFHLElBQUksRUFBSTtBQUNaaEIscUJBQWUsQ0FBQ2dCLElBQUksQ0FBQ2pCLFlBQU4sQ0FBZjtBQUNELEtBYkg7QUFjRCxHQWhCUSxFQWdCTixFQWhCTSxDQUFUO0FBa0JBLE1BQU1rQixTQUFTLEdBQUc7QUFDaEJDLFNBQUssRUFBRTtBQUNMQyxVQUFJLEVBQUU7QUFDSkMsYUFBSyxFQUFFLFNBREg7QUFFSkMsa0JBQVUsRUFBRSxxQkFGUjtBQUdKQyxxQkFBYSxFQUFFLGFBSFg7QUFJSkMsZ0JBQVEsRUFBRSxNQUpOO0FBS0oseUJBQWlCO0FBQ2ZILGVBQUssRUFBRTtBQURRO0FBTGIsT0FERDtBQVVMSSxhQUFPLEVBQUU7QUFDUEosYUFBSyxFQUFFLFNBREE7QUFFUEssaUJBQVMsRUFBRTtBQUZKO0FBVko7QUFEUyxHQUFsQjs7QUFrQkEsTUFBTUMsWUFBWTtBQUFBLG1VQUFHLGlCQUFPQyxLQUFQO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFbkI3Qix5QkFBVyxDQUFDNkIsS0FBSyxDQUFDQyxLQUFQLENBQVg7QUFDQWxDLHNCQUFRLENBQUNpQyxLQUFLLENBQUNsQyxLQUFOLEdBQWNrQyxLQUFLLENBQUNsQyxLQUFOLENBQVlvQyxPQUExQixHQUFvQyxFQUFyQyxDQUFSOztBQUhtQjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxLQUFIOztBQUFBLG9CQUFaSCxZQUFZO0FBQUE7QUFBQTtBQUFBLEtBQWxCOztBQU9BLE1BQU1JLFlBQVk7QUFBQSxtVUFBRyxrQkFBTUMsRUFBTjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFbkJBLGdCQUFFLENBQUNDLGNBQUg7QUFDQXBDLDJCQUFhLENBQUMsSUFBRCxDQUFiO0FBSG1CO0FBQUEscUJBS0dLLE1BQU0sQ0FBQ2dDLGtCQUFQLENBQTBCbEMsWUFBMUIsRUFBd0M7QUFDNURtQyw4QkFBYyxFQUFFO0FBQ2RDLHNCQUFJLEVBQUVoQyxRQUFRLENBQUNpQyxVQUFULENBQW9CQyxtRUFBcEI7QUFEUTtBQUQ0QyxlQUF4QyxDQUxIOztBQUFBO0FBS2JDLHFCQUxhOztBQUFBLG1CQVdmQSxPQUFPLENBQUM3QyxLQVhPO0FBQUE7QUFBQTtBQUFBOztBQVlqQkMsc0JBQVEsMEJBQW1CNEMsT0FBTyxDQUFDN0MsS0FBUixDQUFjb0MsT0FBakMsRUFBUjtBQUNBakMsMkJBQWEsQ0FBQyxLQUFELENBQWI7QUFiaUI7QUFBQTs7QUFBQTtBQWVqQkYsc0JBQVEsQ0FBQyxJQUFELENBQVI7QUFDQUUsMkJBQWEsQ0FBQyxLQUFELENBQWI7QUFDQUosMEJBQVksQ0FBQyxJQUFELENBQVo7QUFFTWtCLGtCQW5CVyxHQW1CSjtBQUFDNkIsNkJBQWEsRUFBRUQ7QUFBaEIsZUFuQkk7QUFBQTtBQUFBLHFCQW9CWEUsNENBQUssQ0FBQ0MsR0FBTiw2RUFBK0VyRCxPQUEvRSxHQUEwRnNCLElBQTFGLENBcEJXOztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLEtBQUg7O0FBQUEsb0JBQVpvQixZQUFZO0FBQUE7QUFBQTtBQUFBLEtBQWxCOztBQXlCQSxzQkFFRTtBQUFTLE1BQUUsRUFBQyxRQUFaO0FBQUEsNEJBQ0E7QUFBSSxlQUFTLEVBQUMseUJBQWQ7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsWUFEQSxlQUVBLHFFQUFDLFFBQUQ7QUFBVSxZQUFNLEVBQUVZLE9BQWxCO0FBQUEsNkJBQ0k7QUFBTSxVQUFFLEVBQUMsY0FBVDtBQUF3QixnQkFBUSxFQUFFWixZQUFsQztBQUFBLGdDQUVJLHFFQUFDLG1FQUFEO0FBQWEsWUFBRSxFQUFDLGNBQWhCO0FBQStCLGlCQUFPLEVBQUViLFNBQXhDO0FBQW1ELGtCQUFRLEVBQUVTO0FBQTdEO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0JBRkosZUFHSTtBQUFRLGtCQUFRLEVBQUUvQixVQUFVLElBQUlFLFFBQWQsSUFBMEJOLFNBQTVDO0FBQXVELFlBQUUsRUFBQyxRQUExRDtBQUFBLGlDQUNBO0FBQU0sY0FBRSxFQUFDLGFBQVQ7QUFBQSxzQkFDQ0ksVUFBVSxnQkFDWDtBQUFLLHVCQUFTLEVBQUMsU0FBZjtBQUF5QixnQkFBRSxFQUFDO0FBQTVCO0FBQUE7QUFBQTtBQUFBO0FBQUEsb0JBRFcsR0FHWDtBQUpBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGdCQUhKLEVBY0tGLEtBQUssaUJBQ047QUFBSyxtQkFBUyxFQUFDLFlBQWY7QUFBNEIsY0FBSSxFQUFDLE9BQWpDO0FBQUEsb0JBQ0tBO0FBREw7QUFBQTtBQUFBO0FBQUE7QUFBQSxnQkFmSjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFESjtBQUFBO0FBQUE7QUFBQTtBQUFBLFlBRkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLFVBRkY7QUE2QkQ7O0dBM0d1Qk4sWTtVQU9QZSxpRSxFQUNFRSxtRTs7O0tBUktqQixZIiwiZmlsZSI6InN0YXRpYy93ZWJwYWNrL3BhZ2VzL3BheW1lbnRzL1tvcmRlcklEXS43NjIyNWZkZjNhMjk2OWRhNDcxZi5ob3QtdXBkYXRlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IFJlYWN0LCB7IHVzZVN0YXRlLCB1c2VFZmZlY3QgfSBmcm9tIFwicmVhY3RcIjtcclxuaW1wb3J0IHtDYXJkRWxlbWVudCwgdXNlU3RyaXBlLCB1c2VFbGVtZW50c30gZnJvbSBcIkBzdHJpcGUvcmVhY3Qtc3RyaXBlLWpzXCI7XHJcbmltcG9ydCBheGlvcyBmcm9tICdheGlvcyc7XHJcblxyXG5leHBvcnQgZGVmYXVsdCBmdW5jdGlvbiBDaGVja291dEZvcm0oe29yZGVySUQsIGFtb3VudH0pIHtcclxuXHJcbiAgY29uc3QgW3N1Y2NlZWRlZCwgc2V0U3VjY2VlZGVkXSA9IHVzZVN0YXRlKGZhbHNlKTtcclxuICBjb25zdCBbZXJyb3IsIHNldEVycm9yXSA9IHVzZVN0YXRlKG51bGwpO1xyXG4gIGNvbnN0IFtwcm9jZXNzaW5nLCBzZXRQcm9jZXNzaW5nXSA9IHVzZVN0YXRlKCcnKTtcclxuICBjb25zdCBbZGlzYWJsZWQsIHNldERpc2FibGVkXSA9IHVzZVN0YXRlKGZhbHNlKTtcclxuICBjb25zdCBbY2xpZW50U2VjcmV0LCBzZXRDbGllbnRTZWNyZXRdID0gdXNlU3RhdGUoJycpO1xyXG4gIGNvbnN0IHN0cmlwZSA9IHVzZVN0cmlwZSgpO1xyXG4gIGNvbnN0IGVsZW1lbnRzID0gdXNlRWxlbWVudHMoKTtcclxuXHJcbiAgdXNlRWZmZWN0KCgpID0+IHtcclxuICAgIC8vIENyZWF0ZSBQYXltZW50SW50ZW50IGFzIHNvb24gYXMgdGhlIHBhZ2UgbG9hZHNcclxuICAgIHdpbmRvd1xyXG4gICAgICAuZmV0Y2goXCJodHRwczovLzI5b2RkZWUyZWguZXhlY3V0ZS1hcGkudXMtZWFzdC0xLmFtYXpvbmF3cy5jb20vZGV2L3N0cmlwZVwiLCB7XHJcbiAgICAgICAgbWV0aG9kOiBcIlBPU1RcIixcclxuICAgICAgICBoZWFkZXJzOiB7XHJcbiAgICAgICAgICBcIkNvbnRlbnQtVHlwZVwiOiBcImFwcGxpY2F0aW9uL2pzb25cIlxyXG4gICAgICAgIH0sXHJcbiAgICAgICAgYm9keTogSlNPTi5zdHJpbmdpZnkoe2Ftb3VudH0pXHJcbiAgICAgIH0pXHJcbiAgICAgIC50aGVuKHJlcyA9PiB7XHJcbiAgICAgICAgcmV0dXJuIHJlcy5qc29uKCk7XHJcbiAgICAgIH0pXHJcbiAgICAgIC50aGVuKGRhdGEgPT4ge1xyXG4gICAgICAgIHNldENsaWVudFNlY3JldChkYXRhLmNsaWVudFNlY3JldCk7XHJcbiAgICAgIH0pO1xyXG4gIH0sIFtdKTtcclxuXHJcbiAgY29uc3QgY2FyZFN0eWxlID0ge1xyXG4gICAgc3R5bGU6IHtcclxuICAgICAgYmFzZToge1xyXG4gICAgICAgIGNvbG9yOiBcIiMzMjMyNWRcIixcclxuICAgICAgICBmb250RmFtaWx5OiAnUG9wcGlucywgc2Fucy1zZXJpZicsXHJcbiAgICAgICAgZm9udFNtb290aGluZzogXCJhbnRpYWxpYXNlZFwiLFxyXG4gICAgICAgIGZvbnRTaXplOiBcIjE2cHhcIixcclxuICAgICAgICBcIjo6cGxhY2Vob2xkZXJcIjoge1xyXG4gICAgICAgICAgY29sb3I6IFwiIzMyMzI1ZFwiXHJcbiAgICAgICAgfVxyXG4gICAgICB9LFxyXG4gICAgICBpbnZhbGlkOiB7XHJcbiAgICAgICAgY29sb3I6IFwiI2ZhNzU1YVwiLFxyXG4gICAgICAgIGljb25Db2xvcjogXCIjZmE3NTVhXCJcclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH07XHJcblxyXG4gIGNvbnN0IGhhbmRsZUNoYW5nZSA9IGFzeW5jIChldmVudCkgPT4ge1xyXG5cclxuICAgIHNldERpc2FibGVkKGV2ZW50LmVtcHR5KTtcclxuICAgIHNldEVycm9yKGV2ZW50LmVycm9yID8gZXZlbnQuZXJyb3IubWVzc2FnZSA6IFwiXCIpO1xyXG5cclxuICB9O1xyXG5cclxuICBjb25zdCBoYW5kbGVTdWJtaXQgPSBhc3luYyBldiA9PiB7XHJcblxyXG4gICAgZXYucHJldmVudERlZmF1bHQoKTtcclxuICAgIHNldFByb2Nlc3NpbmcodHJ1ZSk7XHJcblxyXG4gICAgY29uc3QgcGF5bG9hZCA9IGF3YWl0IHN0cmlwZS5jb25maXJtQ2FyZFBheW1lbnQoY2xpZW50U2VjcmV0LCB7XHJcbiAgICAgIHBheW1lbnRfbWV0aG9kOiB7XHJcbiAgICAgICAgY2FyZDogZWxlbWVudHMuZ2V0RWxlbWVudChDYXJkRWxlbWVudClcclxuICAgICAgfVxyXG4gICAgfSk7XHJcblxyXG4gICAgaWYgKHBheWxvYWQuZXJyb3IpIHtcclxuICAgICAgc2V0RXJyb3IoYFBheW1lbnQgZmFpbGVkICR7cGF5bG9hZC5lcnJvci5tZXNzYWdlfWApO1xyXG4gICAgICBzZXRQcm9jZXNzaW5nKGZhbHNlKTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHNldEVycm9yKG51bGwpO1xyXG4gICAgICBzZXRQcm9jZXNzaW5nKGZhbHNlKTtcclxuICAgICAgc2V0U3VjY2VlZGVkKHRydWUpO1xyXG5cclxuICAgICAgY29uc3QgYm9keSA9IHtwYXltZW50UmVzdWx0OiBwYXlsb2FkfTtcclxuICAgICAgYXdhaXQgYXhpb3MucHV0KGBodHRwczovLzI5b2RkZWUyZWguZXhlY3V0ZS1hcGkudXMtZWFzdC0xLmFtYXpvbmF3cy5jb20vZGV2L29yZGVycy8ke29yZGVySUR9YCwgYm9keSk7XHJcbiAgICB9XHJcblxyXG4gIH07XHJcblxyXG4gIHJldHVybiAoXHJcblxyXG4gICAgPHNlY3Rpb24gaWQ9XCJzdHJpcGVcIj5cclxuICAgIDxoMiBjbGFzc05hbWU9XCJmcy02MHAgZmYtcm9iIHRhLWNlbnRlclwiPlBheSBOb3c8L2gyPlxyXG4gICAgPEVsZW1lbnRzIHN0cmlwZT17cHJvbWlzZX0+XHJcbiAgICAgICAgPGZvcm0gaWQ9XCJwYXltZW50LWZvcm1cIiBvblN1Ym1pdD17aGFuZGxlU3VibWl0fT5cclxuXHJcbiAgICAgICAgICAgIDxDYXJkRWxlbWVudCBpZD1cImNhcmQtZWxlbWVudFwiIG9wdGlvbnM9e2NhcmRTdHlsZX0gb25DaGFuZ2U9e2hhbmRsZUNoYW5nZX0gLz5cclxuICAgICAgICAgICAgPGJ1dHRvbiBkaXNhYmxlZD17cHJvY2Vzc2luZyB8fCBkaXNhYmxlZCB8fCBzdWNjZWVkZWR9IGlkPVwic3VibWl0XCI+XHJcbiAgICAgICAgICAgIDxzcGFuIGlkPVwiYnV0dG9uLXRleHRcIj5cclxuICAgICAgICAgICAge3Byb2Nlc3NpbmcgPyAoXHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwic3Bpbm5lclwiIGlkPVwic3Bpbm5lclwiPjwvZGl2PlxyXG4gICAgICAgICAgICApIDogKFxyXG4gICAgICAgICAgICBcIlBheSBub3dcIlxyXG4gICAgICAgICAgICApfVxyXG4gICAgICAgICAgICA8L3NwYW4+XHJcbiAgICAgICAgICAgIDwvYnV0dG9uPlxyXG5cclxuICAgICAgICAgICAgey8qIFNob3cgYW55IGVycm9yIHRoYXQgaGFwcGVucyB3aGVuIHByb2Nlc3NpbmcgdGhlIHBheW1lbnQgKi99XHJcbiAgICAgICAgICAgIHtlcnJvciAmJiAoXHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiY2FyZC1lcnJvclwiIHJvbGU9XCJhbGVydFwiPlxyXG4gICAgICAgICAgICAgICAge2Vycm9yfVxyXG4gICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgKX1cclxuXHJcbiAgICAgICAgPC9mb3JtPlxyXG4gICAgPC9FbGVtZW50cz5cclxuICAgIDwvc2VjdGlvbj5cclxuICApO1xyXG59Il0sInNvdXJjZVJvb3QiOiIifQ==