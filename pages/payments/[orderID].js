import React, {useEffect, useState} from 'react';
import axios from 'axios';

import {loadStripe} from '@stripe/stripe-js';
import {Elements} from '@stripe/react-stripe-js';
import CheckoutForm from '../../components/CheckoutForm';
import Link from 'next/Link';
import { STRIPE_KEY_TEST } from '../../const';


const promise = loadStripe(STRIPE_KEY_TEST);

export const getServerSideProps = async ({params, query}) => {

    const orderID = params.orderID;
    const accessCode = query.accessCode;

    try {
        
        const res = await axios.get(`https://api.arctetra.com/orders/${orderID}?accessCode=${accessCode}`);
        const order = res.data;

        return {
            props: {order}
        }

    } catch (err) {

        return {
            props: {error: "Access code invalid or invoice does not exist"}
        }
        
    }
}

const PaymentSummary = ({order, error}) => {

    if(error) {
        return (
            <section id="payment-error">
                <div className="container">
                    <p>{error}</p>
                    <Link href="/payments"><a className="btn">Try Again</a></Link>
                </div>
            </section>
        ) 
    }

    const [formData, setFormData] = useState({
        firstName: "",
        lastName: "",
        email: "",
        address: "",
        city: "",
        state: "",
        zip: "",
        phone: ""
    });

    const { firstName, lastName, email, address, city, state, zip, phone} = formData;

    const [paid, setPaid] = useState(order.paid || false);

    const onInputChange = e => {
        setFormData({...formData, [e.target.name]: e.target.value});
    }

    const onPaymentSuccess = async paymentResult => {

        setPaid(true);
        paymentResult.paidAt = Date.now();

        const body = {
            paymentInfo: {
                firstName,
                lastName,
                email,
                address,
                city,
                state,
                zip,
                phone
            },
            paymentResult
        };
        await axios.put(`https://api.arctetra.com/orders/${order.orderID}/pay`, body);
    }

    return (
        <div>
            <section id="order-summary">
                <h2 className="fs-60p ff-rob ta-center">Invoice Details</h2>
                <table id="order-table">
                    <tbody>
                    <tr>
                        <td>OrderID: </td>
                        <td>{order.orderID}</td>
                    </tr>
                    <tr>
                        <td>Payer: </td>
                        <td>{order.firstName} {order.lastName}</td>
                    </tr>
                    <tr>
                        <td>Amount Due: </td>
                        <td>${order.amountDue}</td>
                    </tr>
                    <tr>
                        <td>Due Date: </td>
                        <td>{order.dateDue}</td>
                    </tr>
                    <tr>
                        <td>Description: </td>
                        <td>{order.desc}</td>
                    </tr>
                    <tr>
                        <td>Status: </td>
                        <td>{paid ? "paid" : "unpaid"}</td>
                    </tr>
                    </tbody>
                </table>
            </section>

            {
                paid ? (
                    <section id="payment-success">
                        <h2 className="fs-60p ff-rob ta-center">Invoice Paid</h2>
                        <p>A receipt has been sent to {email}</p>
                        <Link href="/"><a className="btn">Return Home</a></Link>
                    </section>
                ) : (
                    <>
                        <section id="billing-section">
                            <h2 className="fs-60p ff-rob ta-center">Pay Now</h2>
                            <form id="billing-form">

                                <div id="info-form">
                                    <input type="text" name="firstName" id="first-name" placeholder="First Name" className="input" value={firstName} onChange={e => onInputChange(e)} />
                                    <input type="text" name="lastName" id="last-name" placeholder="Last Name" className="input" value={lastName} onChange={e => onInputChange(e)} />
                                    <input type="email" name="email" id="email" placeholder="Email Address" className="input span-2" value={email} onChange={e => onInputChange(e)} />
                                </div>

                                <div id="address-form">
                                    <input type="text" name="address" id="address" placeholder="Address" className="input span-3" value={address} onChange={e => onInputChange(e)}/>
                                    <input type="text" name="city" id="city" placeholder="City" className="input span-2" value={city} onChange={e => onInputChange(e)}/>
                                    <input type="text" name="state" id="state" placeholder="State" className="input" value={state} onChange={e => onInputChange(e)}/>
                                    <input type="text" name="zip" id="zip" placeholder="Zip" className="input" value={zip} onChange={e => onInputChange(e)}/>
                                    <input type="text" name="phone" id="phone" placeholder="Phone" className="input span-22" value={phone} onChange={e => onInputChange(e)}/>
                                </div>
                            </form>
                        </section>

                        <section id="card-section">
                            <h3 className="fs-30p ff-rob ta-center">Card Information</h3>
                            <Elements stripe={promise}>
                                <CheckoutForm orderID={order.orderID} onSuccess={(paymentResult) => onPaymentSuccess(paymentResult)}/>
                            </Elements>
                            <img id="stripe-badge" src='/stripe.png' alt="Powered By Stripe"/>
                        </section>
                    </>
                )
            }

        </div>
    )
}

export default PaymentSummary;
