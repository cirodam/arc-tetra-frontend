webpackHotUpdate_N_E("pages/payments/[orderID]",{

/***/ "./components/CheckoutForm.js":
/*!************************************!*\
  !*** ./components/CheckoutForm.js ***!
  \************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return CheckoutForm; });
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "./node_modules/react/jsx-dev-runtime.js");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var C_Users_Tyler_Desktop_Projects_arc_tetra_software_v2_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/@babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var C_Users_Tyler_Desktop_Projects_arc_tetra_software_v2_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(C_Users_Tyler_Desktop_Projects_arc_tetra_software_v2_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var C_Users_Tyler_Desktop_Projects_arc_tetra_software_v2_node_modules_babel_runtime_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/esm/asyncToGenerator */ "./node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _stripe_react_stripe_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @stripe/react-stripe-js */ "./node_modules/@stripe/react-stripe-js/dist/react-stripe.umd.js");
/* harmony import */ var _stripe_react_stripe_js__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_stripe_react_stripe_js__WEBPACK_IMPORTED_MODULE_4__);




var _jsxFileName = "C:\\Users\\Tyler\\Desktop\\Projects\\arc-tetra-software-v2\\components\\CheckoutForm.js",
    _s = $RefreshSig$();



function CheckoutForm() {
  _s();

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_3__["useState"])(false),
      succeeded = _useState[0],
      setSucceeded = _useState[1];

  var _useState2 = Object(react__WEBPACK_IMPORTED_MODULE_3__["useState"])(null),
      error = _useState2[0],
      setError = _useState2[1];

  var _useState3 = Object(react__WEBPACK_IMPORTED_MODULE_3__["useState"])(''),
      processing = _useState3[0],
      setProcessing = _useState3[1];

  var _useState4 = Object(react__WEBPACK_IMPORTED_MODULE_3__["useState"])(false),
      disabled = _useState4[0],
      setDisabled = _useState4[1];

  var _useState5 = Object(react__WEBPACK_IMPORTED_MODULE_3__["useState"])(''),
      clientSecret = _useState5[0],
      setClientSecret = _useState5[1];

  var stripe = Object(_stripe_react_stripe_js__WEBPACK_IMPORTED_MODULE_4__["useStripe"])();
  var elements = Object(_stripe_react_stripe_js__WEBPACK_IMPORTED_MODULE_4__["useElements"])();
  Object(react__WEBPACK_IMPORTED_MODULE_3__["useEffect"])(function () {
    // Create PaymentIntent as soon as the page loads
    window.fetch("https://29oddee2eh.execute-api.us-east-1.amazonaws.com/dev/stripe", {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        amount: 100
      })
    }).then(function (res) {
      return res.json();
    }).then(function (data) {
      setClientSecret(data.clientSecret);
    });
  }, []);
  var cardStyle = {
    style: {
      base: {
        color: "#32325d",
        fontFamily: 'Poppins, sans-serif',
        fontSmoothing: "antialiased",
        fontSize: "16px",
        "::placeholder": {
          color: "#32325d"
        }
      },
      invalid: {
        color: "#fa755a",
        iconColor: "#fa755a"
      }
    }
  };

  var handleChange = /*#__PURE__*/function () {
    var _ref = Object(C_Users_Tyler_Desktop_Projects_arc_tetra_software_v2_node_modules_babel_runtime_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_2__["default"])( /*#__PURE__*/C_Users_Tyler_Desktop_Projects_arc_tetra_software_v2_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.mark(function _callee(event) {
      return C_Users_Tyler_Desktop_Projects_arc_tetra_software_v2_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              setDisabled(event.empty);
              setError(event.error ? event.error.message : "");

            case 2:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }));

    return function handleChange(_x) {
      return _ref.apply(this, arguments);
    };
  }();

  var handleSubmit = /*#__PURE__*/function () {
    var _ref2 = Object(C_Users_Tyler_Desktop_Projects_arc_tetra_software_v2_node_modules_babel_runtime_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_2__["default"])( /*#__PURE__*/C_Users_Tyler_Desktop_Projects_arc_tetra_software_v2_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.mark(function _callee2(ev) {
      var payload;
      return C_Users_Tyler_Desktop_Projects_arc_tetra_software_v2_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.wrap(function _callee2$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
              ev.preventDefault();
              setProcessing(true);
              _context2.next = 4;
              return stripe.confirmCardPayment(clientSecret, {
                payment_method: {
                  card: elements.getElement(_stripe_react_stripe_js__WEBPACK_IMPORTED_MODULE_4__["CardElement"])
                }
              });

            case 4:
              payload = _context2.sent;

              if (payload.error) {
                setError("Payment failed ".concat(payload.error.message));
                setProcessing(false);
              } else {
                setError(null);
                setProcessing(false);
                setSucceeded(true);
                console.log(payload);
              }

            case 6:
            case "end":
              return _context2.stop();
          }
        }
      }, _callee2);
    }));

    return function handleSubmit(_x2) {
      return _ref2.apply(this, arguments);
    };
  }();

  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("form", {
    id: "payment-form",
    onSubmit: handleSubmit,
    children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_stripe_react_stripe_js__WEBPACK_IMPORTED_MODULE_4__["CardElement"], {
      id: "card-element",
      options: cardStyle,
      onChange: handleChange
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 84,
      columnNumber: 7
    }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("button", {
      disabled: processing || disabled || succeeded,
      id: "submit",
      children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("span", {
        id: "button-text",
        children: processing ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
          className: "spinner",
          id: "spinner"
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 88,
          columnNumber: 13
        }, this) : "Pay now"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 86,
        columnNumber: 9
      }, this)
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 85,
      columnNumber: 7
    }, this), error && /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
      className: "card-error",
      role: "alert",
      children: error
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 97,
      columnNumber: 9
    }, this)]
  }, void 0, true, {
    fileName: _jsxFileName,
    lineNumber: 82,
    columnNumber: 5
  }, this);
}

_s(CheckoutForm, "GhxqeXSXbsPzU6SqFIQHLcFON5I=", false, function () {
  return [_stripe_react_stripe_js__WEBPACK_IMPORTED_MODULE_4__["useStripe"], _stripe_react_stripe_js__WEBPACK_IMPORTED_MODULE_4__["useElements"]];
});

_c = CheckoutForm;

var _c;

$RefreshReg$(_c, "CheckoutForm");

;
    var _a, _b;
    // Legacy CSS implementations will `eval` browser code in a Node.js context
    // to extract CSS. For backwards compatibility, we need to check we're in a
    // browser context before continuing.
    if (typeof self !== 'undefined' &&
        // AMP / No-JS mode does not inject these helpers:
        '$RefreshHelpers$' in self) {
        var currentExports = module.__proto__.exports;
        var prevExports = (_b = (_a = module.hot.data) === null || _a === void 0 ? void 0 : _a.prevExports) !== null && _b !== void 0 ? _b : null;
        // This cannot happen in MainTemplate because the exports mismatch between
        // templating and execution.
        self.$RefreshHelpers$.registerExportsForReactRefresh(currentExports, module.i);
        // A module can be accepted automatically based on its exports, e.g. when
        // it is a Refresh Boundary.
        if (self.$RefreshHelpers$.isReactRefreshBoundary(currentExports)) {
            // Save the previous exports on update so we can compare the boundary
            // signatures.
            module.hot.dispose(function (data) {
                data.prevExports = currentExports;
            });
            // Unconditionally accept an update to this module, we'll check if it's
            // still a Refresh Boundary later.
            module.hot.accept();
            // This field is set when the previous version of this module was a
            // Refresh Boundary, letting us know we need to check for invalidation or
            // enqueue an update.
            if (prevExports !== null) {
                // A boundary can become ineligible if its exports are incompatible
                // with the previous exports.
                //
                // For example, if you add/remove/change exports, we'll want to
                // re-execute the importing modules, and force those components to
                // re-render. Similarly, if you convert a class component to a
                // function, we want to invalidate the boundary.
                if (self.$RefreshHelpers$.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)) {
                    module.hot.invalidate();
                }
                else {
                    self.$RefreshHelpers$.scheduleUpdate();
                }
            }
        }
        else {
            // Since we just executed the code for the module, it's possible that the
            // new exports made it ineligible for being a boundary.
            // We only care about the case when we were _previously_ a boundary,
            // because we already accepted this update (accidental side effect).
            var isNoLongerABoundary = prevExports !== null;
            if (isNoLongerABoundary) {
                module.hot.invalidate();
            }
        }
    }

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../node_modules/next/dist/compiled/webpack/harmony-module.js */ "./node_modules/next/dist/compiled/webpack/harmony-module.js")(module)))

/***/ })

})
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9fTl9FLy4vY29tcG9uZW50cy9DaGVja291dEZvcm0uanMiXSwibmFtZXMiOlsiQ2hlY2tvdXRGb3JtIiwidXNlU3RhdGUiLCJzdWNjZWVkZWQiLCJzZXRTdWNjZWVkZWQiLCJlcnJvciIsInNldEVycm9yIiwicHJvY2Vzc2luZyIsInNldFByb2Nlc3NpbmciLCJkaXNhYmxlZCIsInNldERpc2FibGVkIiwiY2xpZW50U2VjcmV0Iiwic2V0Q2xpZW50U2VjcmV0Iiwic3RyaXBlIiwidXNlU3RyaXBlIiwiZWxlbWVudHMiLCJ1c2VFbGVtZW50cyIsInVzZUVmZmVjdCIsIndpbmRvdyIsImZldGNoIiwibWV0aG9kIiwiaGVhZGVycyIsImJvZHkiLCJKU09OIiwic3RyaW5naWZ5IiwiYW1vdW50IiwidGhlbiIsInJlcyIsImpzb24iLCJkYXRhIiwiY2FyZFN0eWxlIiwic3R5bGUiLCJiYXNlIiwiY29sb3IiLCJmb250RmFtaWx5IiwiZm9udFNtb290aGluZyIsImZvbnRTaXplIiwiaW52YWxpZCIsImljb25Db2xvciIsImhhbmRsZUNoYW5nZSIsImV2ZW50IiwiZW1wdHkiLCJtZXNzYWdlIiwiaGFuZGxlU3VibWl0IiwiZXYiLCJwcmV2ZW50RGVmYXVsdCIsImNvbmZpcm1DYXJkUGF5bWVudCIsInBheW1lbnRfbWV0aG9kIiwiY2FyZCIsImdldEVsZW1lbnQiLCJDYXJkRWxlbWVudCIsInBheWxvYWQiLCJjb25zb2xlIiwibG9nIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7QUFDQTtBQUVlLFNBQVNBLFlBQVQsR0FBd0I7QUFBQTs7QUFBQSxrQkFFSEMsc0RBQVEsQ0FBQyxLQUFELENBRkw7QUFBQSxNQUU5QkMsU0FGOEI7QUFBQSxNQUVuQkMsWUFGbUI7O0FBQUEsbUJBR1hGLHNEQUFRLENBQUMsSUFBRCxDQUhHO0FBQUEsTUFHOUJHLEtBSDhCO0FBQUEsTUFHdkJDLFFBSHVCOztBQUFBLG1CQUlESixzREFBUSxDQUFDLEVBQUQsQ0FKUDtBQUFBLE1BSTlCSyxVQUo4QjtBQUFBLE1BSWxCQyxhQUprQjs7QUFBQSxtQkFLTE4sc0RBQVEsQ0FBQyxLQUFELENBTEg7QUFBQSxNQUs5Qk8sUUFMOEI7QUFBQSxNQUtwQkMsV0FMb0I7O0FBQUEsbUJBTUdSLHNEQUFRLENBQUMsRUFBRCxDQU5YO0FBQUEsTUFNOUJTLFlBTjhCO0FBQUEsTUFNaEJDLGVBTmdCOztBQU9yQyxNQUFNQyxNQUFNLEdBQUdDLHlFQUFTLEVBQXhCO0FBQ0EsTUFBTUMsUUFBUSxHQUFHQywyRUFBVyxFQUE1QjtBQUVBQyx5REFBUyxDQUFDLFlBQU07QUFDZDtBQUNBQyxVQUFNLENBQ0hDLEtBREgsQ0FDUyxtRUFEVCxFQUM4RTtBQUMxRUMsWUFBTSxFQUFFLE1BRGtFO0FBRTFFQyxhQUFPLEVBQUU7QUFDUCx3QkFBZ0I7QUFEVCxPQUZpRTtBQUsxRUMsVUFBSSxFQUFFQyxJQUFJLENBQUNDLFNBQUwsQ0FBZTtBQUFDQyxjQUFNLEVBQUU7QUFBVCxPQUFmO0FBTG9FLEtBRDlFLEVBUUdDLElBUkgsQ0FRUSxVQUFBQyxHQUFHLEVBQUk7QUFDWCxhQUFPQSxHQUFHLENBQUNDLElBQUosRUFBUDtBQUNELEtBVkgsRUFXR0YsSUFYSCxDQVdRLFVBQUFHLElBQUksRUFBSTtBQUNaakIscUJBQWUsQ0FBQ2lCLElBQUksQ0FBQ2xCLFlBQU4sQ0FBZjtBQUNELEtBYkg7QUFjRCxHQWhCUSxFQWdCTixFQWhCTSxDQUFUO0FBa0JBLE1BQU1tQixTQUFTLEdBQUc7QUFDaEJDLFNBQUssRUFBRTtBQUNMQyxVQUFJLEVBQUU7QUFDSkMsYUFBSyxFQUFFLFNBREg7QUFFSkMsa0JBQVUsRUFBRSxxQkFGUjtBQUdKQyxxQkFBYSxFQUFFLGFBSFg7QUFJSkMsZ0JBQVEsRUFBRSxNQUpOO0FBS0oseUJBQWlCO0FBQ2ZILGVBQUssRUFBRTtBQURRO0FBTGIsT0FERDtBQVVMSSxhQUFPLEVBQUU7QUFDUEosYUFBSyxFQUFFLFNBREE7QUFFUEssaUJBQVMsRUFBRTtBQUZKO0FBVko7QUFEUyxHQUFsQjs7QUFrQkEsTUFBTUMsWUFBWTtBQUFBLGtVQUFHLGlCQUFPQyxLQUFQO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFbkI5Qix5QkFBVyxDQUFDOEIsS0FBSyxDQUFDQyxLQUFQLENBQVg7QUFDQW5DLHNCQUFRLENBQUNrQyxLQUFLLENBQUNuQyxLQUFOLEdBQWNtQyxLQUFLLENBQUNuQyxLQUFOLENBQVlxQyxPQUExQixHQUFvQyxFQUFyQyxDQUFSOztBQUhtQjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxLQUFIOztBQUFBLG9CQUFaSCxZQUFZO0FBQUE7QUFBQTtBQUFBLEtBQWxCOztBQU9BLE1BQU1JLFlBQVk7QUFBQSxtVUFBRyxrQkFBTUMsRUFBTjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFbkJBLGdCQUFFLENBQUNDLGNBQUg7QUFDQXJDLDJCQUFhLENBQUMsSUFBRCxDQUFiO0FBSG1CO0FBQUEscUJBS0dLLE1BQU0sQ0FBQ2lDLGtCQUFQLENBQTBCbkMsWUFBMUIsRUFBd0M7QUFDNURvQyw4QkFBYyxFQUFFO0FBQ2RDLHNCQUFJLEVBQUVqQyxRQUFRLENBQUNrQyxVQUFULENBQW9CQyxtRUFBcEI7QUFEUTtBQUQ0QyxlQUF4QyxDQUxIOztBQUFBO0FBS2JDLHFCQUxhOztBQVduQixrQkFBSUEsT0FBTyxDQUFDOUMsS0FBWixFQUFtQjtBQUNqQkMsd0JBQVEsMEJBQW1CNkMsT0FBTyxDQUFDOUMsS0FBUixDQUFjcUMsT0FBakMsRUFBUjtBQUNBbEMsNkJBQWEsQ0FBQyxLQUFELENBQWI7QUFDRCxlQUhELE1BR087QUFDTEYsd0JBQVEsQ0FBQyxJQUFELENBQVI7QUFDQUUsNkJBQWEsQ0FBQyxLQUFELENBQWI7QUFDQUosNEJBQVksQ0FBQyxJQUFELENBQVo7QUFDQWdELHVCQUFPLENBQUNDLEdBQVIsQ0FBWUYsT0FBWjtBQUNEOztBQW5Ca0I7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsS0FBSDs7QUFBQSxvQkFBWlIsWUFBWTtBQUFBO0FBQUE7QUFBQSxLQUFsQjs7QUF1QkEsc0JBRUU7QUFBTSxNQUFFLEVBQUMsY0FBVDtBQUF3QixZQUFRLEVBQUVBLFlBQWxDO0FBQUEsNEJBRUUscUVBQUMsbUVBQUQ7QUFBYSxRQUFFLEVBQUMsY0FBaEI7QUFBK0IsYUFBTyxFQUFFYixTQUF4QztBQUFtRCxjQUFRLEVBQUVTO0FBQTdEO0FBQUE7QUFBQTtBQUFBO0FBQUEsWUFGRixlQUdFO0FBQVEsY0FBUSxFQUFFaEMsVUFBVSxJQUFJRSxRQUFkLElBQTBCTixTQUE1QztBQUF1RCxRQUFFLEVBQUMsUUFBMUQ7QUFBQSw2QkFDRTtBQUFNLFVBQUUsRUFBQyxhQUFUO0FBQUEsa0JBQ0dJLFVBQVUsZ0JBQ1Q7QUFBSyxtQkFBUyxFQUFDLFNBQWY7QUFBeUIsWUFBRSxFQUFDO0FBQTVCO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0JBRFMsR0FHVDtBQUpKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBLFlBSEYsRUFjR0YsS0FBSyxpQkFDSjtBQUFLLGVBQVMsRUFBQyxZQUFmO0FBQTRCLFVBQUksRUFBQyxPQUFqQztBQUFBLGdCQUNHQTtBQURIO0FBQUE7QUFBQTtBQUFBO0FBQUEsWUFmSjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsVUFGRjtBQXdCRDs7R0FwR3VCSixZO1VBT1BhLGlFLEVBQ0VFLG1FOzs7S0FSS2YsWSIsImZpbGUiOiJzdGF0aWMvd2VicGFjay9wYWdlcy9wYXltZW50cy9bb3JkZXJJRF0uYzI1YzE0ZmJkOTJkOWQzOTA1ZGUuaG90LXVwZGF0ZS5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBSZWFjdCwgeyB1c2VTdGF0ZSwgdXNlRWZmZWN0IH0gZnJvbSBcInJlYWN0XCI7XHJcbmltcG9ydCB7Q2FyZEVsZW1lbnQsIHVzZVN0cmlwZSwgdXNlRWxlbWVudHN9IGZyb20gXCJAc3RyaXBlL3JlYWN0LXN0cmlwZS1qc1wiO1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgZnVuY3Rpb24gQ2hlY2tvdXRGb3JtKCkge1xyXG5cclxuICBjb25zdCBbc3VjY2VlZGVkLCBzZXRTdWNjZWVkZWRdID0gdXNlU3RhdGUoZmFsc2UpO1xyXG4gIGNvbnN0IFtlcnJvciwgc2V0RXJyb3JdID0gdXNlU3RhdGUobnVsbCk7XHJcbiAgY29uc3QgW3Byb2Nlc3NpbmcsIHNldFByb2Nlc3NpbmddID0gdXNlU3RhdGUoJycpO1xyXG4gIGNvbnN0IFtkaXNhYmxlZCwgc2V0RGlzYWJsZWRdID0gdXNlU3RhdGUoZmFsc2UpO1xyXG4gIGNvbnN0IFtjbGllbnRTZWNyZXQsIHNldENsaWVudFNlY3JldF0gPSB1c2VTdGF0ZSgnJyk7XHJcbiAgY29uc3Qgc3RyaXBlID0gdXNlU3RyaXBlKCk7XHJcbiAgY29uc3QgZWxlbWVudHMgPSB1c2VFbGVtZW50cygpO1xyXG5cclxuICB1c2VFZmZlY3QoKCkgPT4ge1xyXG4gICAgLy8gQ3JlYXRlIFBheW1lbnRJbnRlbnQgYXMgc29vbiBhcyB0aGUgcGFnZSBsb2Fkc1xyXG4gICAgd2luZG93XHJcbiAgICAgIC5mZXRjaChcImh0dHBzOi8vMjlvZGRlZTJlaC5leGVjdXRlLWFwaS51cy1lYXN0LTEuYW1hem9uYXdzLmNvbS9kZXYvc3RyaXBlXCIsIHtcclxuICAgICAgICBtZXRob2Q6IFwiUE9TVFwiLFxyXG4gICAgICAgIGhlYWRlcnM6IHtcclxuICAgICAgICAgIFwiQ29udGVudC1UeXBlXCI6IFwiYXBwbGljYXRpb24vanNvblwiXHJcbiAgICAgICAgfSxcclxuICAgICAgICBib2R5OiBKU09OLnN0cmluZ2lmeSh7YW1vdW50OiAxMDB9KVxyXG4gICAgICB9KVxyXG4gICAgICAudGhlbihyZXMgPT4ge1xyXG4gICAgICAgIHJldHVybiByZXMuanNvbigpO1xyXG4gICAgICB9KVxyXG4gICAgICAudGhlbihkYXRhID0+IHtcclxuICAgICAgICBzZXRDbGllbnRTZWNyZXQoZGF0YS5jbGllbnRTZWNyZXQpO1xyXG4gICAgICB9KTtcclxuICB9LCBbXSk7XHJcblxyXG4gIGNvbnN0IGNhcmRTdHlsZSA9IHtcclxuICAgIHN0eWxlOiB7XHJcbiAgICAgIGJhc2U6IHtcclxuICAgICAgICBjb2xvcjogXCIjMzIzMjVkXCIsXHJcbiAgICAgICAgZm9udEZhbWlseTogJ1BvcHBpbnMsIHNhbnMtc2VyaWYnLFxyXG4gICAgICAgIGZvbnRTbW9vdGhpbmc6IFwiYW50aWFsaWFzZWRcIixcclxuICAgICAgICBmb250U2l6ZTogXCIxNnB4XCIsXHJcbiAgICAgICAgXCI6OnBsYWNlaG9sZGVyXCI6IHtcclxuICAgICAgICAgIGNvbG9yOiBcIiMzMjMyNWRcIlxyXG4gICAgICAgIH1cclxuICAgICAgfSxcclxuICAgICAgaW52YWxpZDoge1xyXG4gICAgICAgIGNvbG9yOiBcIiNmYTc1NWFcIixcclxuICAgICAgICBpY29uQ29sb3I6IFwiI2ZhNzU1YVwiXHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9O1xyXG5cclxuICBjb25zdCBoYW5kbGVDaGFuZ2UgPSBhc3luYyAoZXZlbnQpID0+IHtcclxuXHJcbiAgICBzZXREaXNhYmxlZChldmVudC5lbXB0eSk7XHJcbiAgICBzZXRFcnJvcihldmVudC5lcnJvciA/IGV2ZW50LmVycm9yLm1lc3NhZ2UgOiBcIlwiKTtcclxuXHJcbiAgfTtcclxuXHJcbiAgY29uc3QgaGFuZGxlU3VibWl0ID0gYXN5bmMgZXYgPT4ge1xyXG5cclxuICAgIGV2LnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICBzZXRQcm9jZXNzaW5nKHRydWUpO1xyXG5cclxuICAgIGNvbnN0IHBheWxvYWQgPSBhd2FpdCBzdHJpcGUuY29uZmlybUNhcmRQYXltZW50KGNsaWVudFNlY3JldCwge1xyXG4gICAgICBwYXltZW50X21ldGhvZDoge1xyXG4gICAgICAgIGNhcmQ6IGVsZW1lbnRzLmdldEVsZW1lbnQoQ2FyZEVsZW1lbnQpXHJcbiAgICAgIH1cclxuICAgIH0pO1xyXG5cclxuICAgIGlmIChwYXlsb2FkLmVycm9yKSB7XHJcbiAgICAgIHNldEVycm9yKGBQYXltZW50IGZhaWxlZCAke3BheWxvYWQuZXJyb3IubWVzc2FnZX1gKTtcclxuICAgICAgc2V0UHJvY2Vzc2luZyhmYWxzZSk7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICBzZXRFcnJvcihudWxsKTtcclxuICAgICAgc2V0UHJvY2Vzc2luZyhmYWxzZSk7XHJcbiAgICAgIHNldFN1Y2NlZWRlZCh0cnVlKTtcclxuICAgICAgY29uc29sZS5sb2cocGF5bG9hZCk7XHJcbiAgICB9XHJcblxyXG4gIH07XHJcblxyXG4gIHJldHVybiAoXHJcblxyXG4gICAgPGZvcm0gaWQ9XCJwYXltZW50LWZvcm1cIiBvblN1Ym1pdD17aGFuZGxlU3VibWl0fT5cclxuXHJcbiAgICAgIDxDYXJkRWxlbWVudCBpZD1cImNhcmQtZWxlbWVudFwiIG9wdGlvbnM9e2NhcmRTdHlsZX0gb25DaGFuZ2U9e2hhbmRsZUNoYW5nZX0gLz5cclxuICAgICAgPGJ1dHRvbiBkaXNhYmxlZD17cHJvY2Vzc2luZyB8fCBkaXNhYmxlZCB8fCBzdWNjZWVkZWR9IGlkPVwic3VibWl0XCI+XHJcbiAgICAgICAgPHNwYW4gaWQ9XCJidXR0b24tdGV4dFwiPlxyXG4gICAgICAgICAge3Byb2Nlc3NpbmcgPyAoXHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwic3Bpbm5lclwiIGlkPVwic3Bpbm5lclwiPjwvZGl2PlxyXG4gICAgICAgICAgKSA6IChcclxuICAgICAgICAgICAgXCJQYXkgbm93XCJcclxuICAgICAgICAgICl9XHJcbiAgICAgICAgPC9zcGFuPlxyXG4gICAgICA8L2J1dHRvbj5cclxuXHJcbiAgICAgIHsvKiBTaG93IGFueSBlcnJvciB0aGF0IGhhcHBlbnMgd2hlbiBwcm9jZXNzaW5nIHRoZSBwYXltZW50ICovfVxyXG4gICAgICB7ZXJyb3IgJiYgKFxyXG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiY2FyZC1lcnJvclwiIHJvbGU9XCJhbGVydFwiPlxyXG4gICAgICAgICAge2Vycm9yfVxyXG4gICAgICAgIDwvZGl2PlxyXG4gICAgICApfVxyXG5cclxuICAgIDwvZm9ybT5cclxuICApO1xyXG59Il0sInNvdXJjZVJvb3QiOiIifQ==