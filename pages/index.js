import Link from 'next/Link';
import Head from 'next/head';

import Contact from '../components/Contact';

export const getStaticProps = (context) => {
  return {
    props: {}
  }
} 

export default function Home() {
  return (
    <>
      <Head>
        <title>Arc Tetra Software | Web Development</title>
        <meta name="description" content="Web Developer Based in Dawsonville, Georgia"/>
      </Head>

      <section id="landing">
        <div className="container">
          <h1 className="fs-90p ff-rob co-white">North Georgia Web Development</h1>
          <p className="fs-18p co-white">We specialize in writing high-quality, affordable, and secure software for a variety of purposes. Contact us today to get started.</p>
          <Link href="#contact"><a className="btn">Contact Us</a></Link>
        </div>
      </section>

      <section id="services">
        <div className="container">
          <h3 className="fs-30p ff-rob co-primary ta-center">Our Services</h3>
          <h2 className="fs-60p ff-rob ta-center">Custom Solutions for your project</h2>
          <div id="services-list">
            
            <div className="service-item">
              <i className="fas fa-laptop-code fs-48p"></i>
              <h3>Web Development</h3>
              <p>Use a personlized website to advertise your business or blog about your life. We specialize in HTML as well as Wordpress.</p>
              <Link href="/services"><a>Learn More <i className="fas fa-chevron-right"></i></a></Link>
            </div>

            <div className="service-item">
              <i className="fas fa-microchip"></i>
              <h3>Custom Software</h3>
              <p>Improve your business's workflow with customized software. Record company information in a secure, cloud database.</p>
              <Link href="/services"><a>Learn More <i className="fas fa-chevron-right"></i></a></Link>
            </div>

            <div className="service-item">
              <i className="fas fa-cloud"></i>
              <h3>Cloud Computing</h3>
              <p>Move your business operations into the cloud for improved security, reliability, and affordability. We specialize in AWS.</p>
              <Link href="/services"><a>Learn More <i className="fas fa-chevron-right"></i></a></Link>
            </div>

            <div className="service-item">
              <i className="fas fa-wrench"></i>
              <h3>Web Hosting</h3>
              <p>Let us handle the challenges of deploying and configuring your site. We also offer regular maintenance plans.</p>
              <Link href="/services"><a>Learn More <i className="fas fa-chevron-right"></i></a></Link>
            </div>

          </div>
        </div>
      </section>

      <section id="choose-us" className="bg-light">
        <div className="container">
          <h3 className="fs-30p ff-rob co-primary ta-center">Why Us?</h3>
          <h2 className="fs-60p ff-rob ta-center">What makes us the best choice?</h2>
          <div id="choose-list">

            <div className="choose-item">
              <h3 className="ff-rob fs-30p">Integrity</h3>
              <p className="fs-16p">We are committed to providing high-quality work performed in a timely manner. We stand by the value that our services can add to your business. Our goal is to see our partners succeed.</p>
            </div>

            <div className="choose-item">
              <h3 className="ff-rob fs-30p">Efficiency</h3>
              <p className="fs-16p">We have experience with a wide range of technologies. Regardless of what you need, our committment is to find a cost-effective solution that is optimal for your needs. </p>
           </div>

            <div className="choose-item">
              <h3 className="ff-rob fs-30p">Affordability</h3>
              <p className="fs-16p">Our goal is to support small businesses. We are therefore committed to keeping our services affordable. We're flexible and we will work with you to identify options regardless of your budget</p>
            </div>

          </div>
        </div>
      </section>
      <Contact />
    </>
  )
}
