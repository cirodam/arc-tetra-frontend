webpackHotUpdate_N_E("pages/payments/[orderID]",{

/***/ "./components/CheckoutForm.js":
/*!************************************!*\
  !*** ./components/CheckoutForm.js ***!
  \************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return CheckoutForm; });
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "./node_modules/react/jsx-dev-runtime.js");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var C_Users_Tyler_Desktop_Projects_arc_tetra_software_v2_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/@babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var C_Users_Tyler_Desktop_Projects_arc_tetra_software_v2_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(C_Users_Tyler_Desktop_Projects_arc_tetra_software_v2_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var C_Users_Tyler_Desktop_Projects_arc_tetra_software_v2_node_modules_babel_runtime_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/esm/asyncToGenerator */ "./node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _stripe_react_stripe_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @stripe/react-stripe-js */ "./node_modules/@stripe/react-stripe-js/dist/react-stripe.umd.js");
/* harmony import */ var _stripe_react_stripe_js__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_stripe_react_stripe_js__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! next/router */ "./node_modules/next/router.js");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_6__);




var _jsxFileName = "C:\\Users\\Tyler\\Desktop\\Projects\\arc-tetra-software-v2\\components\\CheckoutForm.js",
    _s = $RefreshSig$();





function CheckoutForm(_ref) {
  _s();

  var orderID = _ref.orderID,
      amount = _ref.amount,
      onSuccess = _ref.onSuccess;

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_3__["useState"])(false),
      succeeded = _useState[0],
      setSucceeded = _useState[1];

  var _useState2 = Object(react__WEBPACK_IMPORTED_MODULE_3__["useState"])(null),
      error = _useState2[0],
      setError = _useState2[1];

  var _useState3 = Object(react__WEBPACK_IMPORTED_MODULE_3__["useState"])(''),
      processing = _useState3[0],
      setProcessing = _useState3[1];

  var _useState4 = Object(react__WEBPACK_IMPORTED_MODULE_3__["useState"])(false),
      disabled = _useState4[0],
      setDisabled = _useState4[1];

  var _useState5 = Object(react__WEBPACK_IMPORTED_MODULE_3__["useState"])(''),
      clientSecret = _useState5[0],
      setClientSecret = _useState5[1];

  var stripe = Object(_stripe_react_stripe_js__WEBPACK_IMPORTED_MODULE_4__["useStripe"])();
  var elements = Object(_stripe_react_stripe_js__WEBPACK_IMPORTED_MODULE_4__["useElements"])();
  var router = Object(next_router__WEBPACK_IMPORTED_MODULE_6__["useRouter"])();
  Object(react__WEBPACK_IMPORTED_MODULE_3__["useEffect"])(function () {
    var getIntent = /*#__PURE__*/function () {
      var _ref2 = Object(C_Users_Tyler_Desktop_Projects_arc_tetra_software_v2_node_modules_babel_runtime_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_2__["default"])( /*#__PURE__*/C_Users_Tyler_Desktop_Projects_arc_tetra_software_v2_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.mark(function _callee() {
        var res;
        return C_Users_Tyler_Desktop_Projects_arc_tetra_software_v2_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                res = axios__WEBPACK_IMPORTED_MODULE_5___default.a.post("https://29oddee2eh.execute-api.us-east-1.amazonaws.com/dev/orders/".concat(orderID, "/intent"));
                setClientSecret(data.clientSecret);

              case 2:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }));

      return function getIntent() {
        return _ref2.apply(this, arguments);
      };
    }();

    window.fetch("https://29oddee2eh.execute-api.us-east-1.amazonaws.com/dev/orders/".concat(orderID, "/intent"), {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        amount: amount
      })
    }).then(function (res) {
      return res.json();
    }).then(function (data) {
      setClientSecret(data.clientSecret);
    });
  }, []);
  var cardStyle = {
    style: {
      base: {
        color: "#32325d",
        fontFamily: 'Poppins, sans-serif',
        fontSmoothing: "antialiased",
        fontSize: "16px",
        "::placeholder": {
          color: "#32325d"
        }
      },
      invalid: {
        color: "#fa755a",
        iconColor: "#fa755a"
      }
    }
  };

  var handleChange = /*#__PURE__*/function () {
    var _ref3 = Object(C_Users_Tyler_Desktop_Projects_arc_tetra_software_v2_node_modules_babel_runtime_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_2__["default"])( /*#__PURE__*/C_Users_Tyler_Desktop_Projects_arc_tetra_software_v2_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.mark(function _callee2(event) {
      return C_Users_Tyler_Desktop_Projects_arc_tetra_software_v2_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.wrap(function _callee2$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
              setDisabled(event.empty);
              setError(event.error ? event.error.message : "");

            case 2:
            case "end":
              return _context2.stop();
          }
        }
      }, _callee2);
    }));

    return function handleChange(_x) {
      return _ref3.apply(this, arguments);
    };
  }();

  var handleSubmit = /*#__PURE__*/function () {
    var _ref4 = Object(C_Users_Tyler_Desktop_Projects_arc_tetra_software_v2_node_modules_babel_runtime_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_2__["default"])( /*#__PURE__*/C_Users_Tyler_Desktop_Projects_arc_tetra_software_v2_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.mark(function _callee3(ev) {
      var payload, body;
      return C_Users_Tyler_Desktop_Projects_arc_tetra_software_v2_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.wrap(function _callee3$(_context3) {
        while (1) {
          switch (_context3.prev = _context3.next) {
            case 0:
              ev.preventDefault();
              setProcessing(true);
              _context3.next = 4;
              return stripe.confirmCardPayment(clientSecret, {
                payment_method: {
                  card: elements.getElement(_stripe_react_stripe_js__WEBPACK_IMPORTED_MODULE_4__["CardElement"])
                }
              });

            case 4:
              payload = _context3.sent;

              if (!payload.error) {
                _context3.next = 10;
                break;
              }

              setError("Payment failed ".concat(payload.error.message));
              setProcessing(false);
              _context3.next = 17;
              break;

            case 10:
              setError(null);
              setProcessing(false);
              setSucceeded(true);
              body = {
                paymentResult: payload
              };
              _context3.next = 16;
              return axios__WEBPACK_IMPORTED_MODULE_5___default.a.put("https://29oddee2eh.execute-api.us-east-1.amazonaws.com/dev/orders/".concat(orderID, "/pay"), body);

            case 16:
              onSuccess();

            case 17:
            case "end":
              return _context3.stop();
          }
        }
      }, _callee3);
    }));

    return function handleSubmit(_x2) {
      return _ref4.apply(this, arguments);
    };
  }();

  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("form", {
    onSubmit: handleSubmit,
    children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_stripe_react_stripe_js__WEBPACK_IMPORTED_MODULE_4__["CardElement"], {
      id: "card-element",
      options: cardStyle,
      onChange: handleChange
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 97,
      columnNumber: 7
    }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("button", {
      disabled: processing || disabled || succeeded,
      id: "submit",
      className: "btn",
      children: "Pay Now"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 98,
      columnNumber: 7
    }, this), error && /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
      className: "card-error",
      role: "alert",
      children: error
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 102,
      columnNumber: 9
    }, this)]
  }, void 0, true, {
    fileName: _jsxFileName,
    lineNumber: 95,
    columnNumber: 5
  }, this);
}

_s(CheckoutForm, "TD5+ko+lb+Su6RfyPBjxDsv+3/I=", false, function () {
  return [_stripe_react_stripe_js__WEBPACK_IMPORTED_MODULE_4__["useStripe"], _stripe_react_stripe_js__WEBPACK_IMPORTED_MODULE_4__["useElements"], next_router__WEBPACK_IMPORTED_MODULE_6__["useRouter"]];
});

_c = CheckoutForm;

var _c;

$RefreshReg$(_c, "CheckoutForm");

;
    var _a, _b;
    // Legacy CSS implementations will `eval` browser code in a Node.js context
    // to extract CSS. For backwards compatibility, we need to check we're in a
    // browser context before continuing.
    if (typeof self !== 'undefined' &&
        // AMP / No-JS mode does not inject these helpers:
        '$RefreshHelpers$' in self) {
        var currentExports = module.__proto__.exports;
        var prevExports = (_b = (_a = module.hot.data) === null || _a === void 0 ? void 0 : _a.prevExports) !== null && _b !== void 0 ? _b : null;
        // This cannot happen in MainTemplate because the exports mismatch between
        // templating and execution.
        self.$RefreshHelpers$.registerExportsForReactRefresh(currentExports, module.i);
        // A module can be accepted automatically based on its exports, e.g. when
        // it is a Refresh Boundary.
        if (self.$RefreshHelpers$.isReactRefreshBoundary(currentExports)) {
            // Save the previous exports on update so we can compare the boundary
            // signatures.
            module.hot.dispose(function (data) {
                data.prevExports = currentExports;
            });
            // Unconditionally accept an update to this module, we'll check if it's
            // still a Refresh Boundary later.
            module.hot.accept();
            // This field is set when the previous version of this module was a
            // Refresh Boundary, letting us know we need to check for invalidation or
            // enqueue an update.
            if (prevExports !== null) {
                // A boundary can become ineligible if its exports are incompatible
                // with the previous exports.
                //
                // For example, if you add/remove/change exports, we'll want to
                // re-execute the importing modules, and force those components to
                // re-render. Similarly, if you convert a class component to a
                // function, we want to invalidate the boundary.
                if (self.$RefreshHelpers$.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)) {
                    module.hot.invalidate();
                }
                else {
                    self.$RefreshHelpers$.scheduleUpdate();
                }
            }
        }
        else {
            // Since we just executed the code for the module, it's possible that the
            // new exports made it ineligible for being a boundary.
            // We only care about the case when we were _previously_ a boundary,
            // because we already accepted this update (accidental side effect).
            var isNoLongerABoundary = prevExports !== null;
            if (isNoLongerABoundary) {
                module.hot.invalidate();
            }
        }
    }

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../node_modules/next/dist/compiled/webpack/harmony-module.js */ "./node_modules/next/dist/compiled/webpack/harmony-module.js")(module)))

/***/ })

})
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9fTl9FLy4vY29tcG9uZW50cy9DaGVja291dEZvcm0uanMiXSwibmFtZXMiOlsiQ2hlY2tvdXRGb3JtIiwib3JkZXJJRCIsImFtb3VudCIsIm9uU3VjY2VzcyIsInVzZVN0YXRlIiwic3VjY2VlZGVkIiwic2V0U3VjY2VlZGVkIiwiZXJyb3IiLCJzZXRFcnJvciIsInByb2Nlc3NpbmciLCJzZXRQcm9jZXNzaW5nIiwiZGlzYWJsZWQiLCJzZXREaXNhYmxlZCIsImNsaWVudFNlY3JldCIsInNldENsaWVudFNlY3JldCIsInN0cmlwZSIsInVzZVN0cmlwZSIsImVsZW1lbnRzIiwidXNlRWxlbWVudHMiLCJyb3V0ZXIiLCJ1c2VSb3V0ZXIiLCJ1c2VFZmZlY3QiLCJnZXRJbnRlbnQiLCJyZXMiLCJheGlvcyIsInBvc3QiLCJkYXRhIiwid2luZG93IiwiZmV0Y2giLCJtZXRob2QiLCJoZWFkZXJzIiwiYm9keSIsIkpTT04iLCJzdHJpbmdpZnkiLCJ0aGVuIiwianNvbiIsImNhcmRTdHlsZSIsInN0eWxlIiwiYmFzZSIsImNvbG9yIiwiZm9udEZhbWlseSIsImZvbnRTbW9vdGhpbmciLCJmb250U2l6ZSIsImludmFsaWQiLCJpY29uQ29sb3IiLCJoYW5kbGVDaGFuZ2UiLCJldmVudCIsImVtcHR5IiwibWVzc2FnZSIsImhhbmRsZVN1Ym1pdCIsImV2IiwicHJldmVudERlZmF1bHQiLCJjb25maXJtQ2FyZFBheW1lbnQiLCJwYXltZW50X21ldGhvZCIsImNhcmQiLCJnZXRFbGVtZW50IiwiQ2FyZEVsZW1lbnQiLCJwYXlsb2FkIiwicGF5bWVudFJlc3VsdCIsInB1dCJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUVlLFNBQVNBLFlBQVQsT0FBb0Q7QUFBQTs7QUFBQSxNQUE3QkMsT0FBNkIsUUFBN0JBLE9BQTZCO0FBQUEsTUFBcEJDLE1BQW9CLFFBQXBCQSxNQUFvQjtBQUFBLE1BQVpDLFNBQVksUUFBWkEsU0FBWTs7QUFBQSxrQkFFL0JDLHNEQUFRLENBQUMsS0FBRCxDQUZ1QjtBQUFBLE1BRTFEQyxTQUYwRDtBQUFBLE1BRS9DQyxZQUYrQzs7QUFBQSxtQkFHdkNGLHNEQUFRLENBQUMsSUFBRCxDQUgrQjtBQUFBLE1BRzFERyxLQUgwRDtBQUFBLE1BR25EQyxRQUhtRDs7QUFBQSxtQkFJN0JKLHNEQUFRLENBQUMsRUFBRCxDQUpxQjtBQUFBLE1BSTFESyxVQUowRDtBQUFBLE1BSTlDQyxhQUo4Qzs7QUFBQSxtQkFLakNOLHNEQUFRLENBQUMsS0FBRCxDQUx5QjtBQUFBLE1BSzFETyxRQUwwRDtBQUFBLE1BS2hEQyxXQUxnRDs7QUFBQSxtQkFNekJSLHNEQUFRLENBQUMsRUFBRCxDQU5pQjtBQUFBLE1BTTFEUyxZQU4wRDtBQUFBLE1BTTVDQyxlQU40Qzs7QUFPakUsTUFBTUMsTUFBTSxHQUFHQyx5RUFBUyxFQUF4QjtBQUNBLE1BQU1DLFFBQVEsR0FBR0MsMkVBQVcsRUFBNUI7QUFFQSxNQUFNQyxNQUFNLEdBQUdDLDZEQUFTLEVBQXhCO0FBRUFDLHlEQUFTLENBQUMsWUFBTTtBQUVkLFFBQU1DLFNBQVM7QUFBQSxxVUFBRztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDUkMsbUJBRFEsR0FDRkMsNENBQUssQ0FBQ0MsSUFBTiw2RUFBZ0Z4QixPQUFoRixhQURFO0FBRWRhLCtCQUFlLENBQUNZLElBQUksQ0FBQ2IsWUFBTixDQUFmOztBQUZjO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLE9BQUg7O0FBQUEsc0JBQVRTLFNBQVM7QUFBQTtBQUFBO0FBQUEsT0FBZjs7QUFLQUssVUFBTSxDQUNIQyxLQURILDZFQUM4RTNCLE9BRDlFLGNBQ2dHO0FBQzVGNEIsWUFBTSxFQUFFLE1BRG9GO0FBRTVGQyxhQUFPLEVBQUU7QUFDUCx3QkFBZ0I7QUFEVCxPQUZtRjtBQUs1RkMsVUFBSSxFQUFFQyxJQUFJLENBQUNDLFNBQUwsQ0FBZTtBQUFDL0IsY0FBTSxFQUFOQTtBQUFELE9BQWY7QUFMc0YsS0FEaEcsRUFRR2dDLElBUkgsQ0FRUSxVQUFBWCxHQUFHLEVBQUk7QUFDWCxhQUFPQSxHQUFHLENBQUNZLElBQUosRUFBUDtBQUNELEtBVkgsRUFXR0QsSUFYSCxDQVdRLFVBQUFSLElBQUksRUFBSTtBQUNaWixxQkFBZSxDQUFDWSxJQUFJLENBQUNiLFlBQU4sQ0FBZjtBQUNELEtBYkg7QUFjRCxHQXJCUSxFQXFCTixFQXJCTSxDQUFUO0FBdUJBLE1BQU11QixTQUFTLEdBQUc7QUFDaEJDLFNBQUssRUFBRTtBQUNMQyxVQUFJLEVBQUU7QUFDSkMsYUFBSyxFQUFFLFNBREg7QUFFSkMsa0JBQVUsRUFBRSxxQkFGUjtBQUdKQyxxQkFBYSxFQUFFLGFBSFg7QUFJSkMsZ0JBQVEsRUFBRSxNQUpOO0FBS0oseUJBQWlCO0FBQ2ZILGVBQUssRUFBRTtBQURRO0FBTGIsT0FERDtBQVVMSSxhQUFPLEVBQUU7QUFDUEosYUFBSyxFQUFFLFNBREE7QUFFUEssaUJBQVMsRUFBRTtBQUZKO0FBVko7QUFEUyxHQUFsQjs7QUFrQkEsTUFBTUMsWUFBWTtBQUFBLG1VQUFHLGtCQUFPQyxLQUFQO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFbkJsQyx5QkFBVyxDQUFDa0MsS0FBSyxDQUFDQyxLQUFQLENBQVg7QUFDQXZDLHNCQUFRLENBQUNzQyxLQUFLLENBQUN2QyxLQUFOLEdBQWN1QyxLQUFLLENBQUN2QyxLQUFOLENBQVl5QyxPQUExQixHQUFvQyxFQUFyQyxDQUFSOztBQUhtQjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxLQUFIOztBQUFBLG9CQUFaSCxZQUFZO0FBQUE7QUFBQTtBQUFBLEtBQWxCOztBQU9BLE1BQU1JLFlBQVk7QUFBQSxtVUFBRyxrQkFBTUMsRUFBTjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFbkJBLGdCQUFFLENBQUNDLGNBQUg7QUFDQXpDLDJCQUFhLENBQUMsSUFBRCxDQUFiO0FBSG1CO0FBQUEscUJBS0dLLE1BQU0sQ0FBQ3FDLGtCQUFQLENBQTBCdkMsWUFBMUIsRUFBd0M7QUFDNUR3Qyw4QkFBYyxFQUFFO0FBQ2RDLHNCQUFJLEVBQUVyQyxRQUFRLENBQUNzQyxVQUFULENBQW9CQyxtRUFBcEI7QUFEUTtBQUQ0QyxlQUF4QyxDQUxIOztBQUFBO0FBS2JDLHFCQUxhOztBQUFBLG1CQVdmQSxPQUFPLENBQUNsRCxLQVhPO0FBQUE7QUFBQTtBQUFBOztBQVlqQkMsc0JBQVEsMEJBQW1CaUQsT0FBTyxDQUFDbEQsS0FBUixDQUFjeUMsT0FBakMsRUFBUjtBQUNBdEMsMkJBQWEsQ0FBQyxLQUFELENBQWI7QUFiaUI7QUFBQTs7QUFBQTtBQWVqQkYsc0JBQVEsQ0FBQyxJQUFELENBQVI7QUFDQUUsMkJBQWEsQ0FBQyxLQUFELENBQWI7QUFDQUosMEJBQVksQ0FBQyxJQUFELENBQVo7QUFFTXlCLGtCQW5CVyxHQW1CSjtBQUFDMkIsNkJBQWEsRUFBRUQ7QUFBaEIsZUFuQkk7QUFBQTtBQUFBLHFCQW9CWGpDLDRDQUFLLENBQUNtQyxHQUFOLDZFQUErRTFELE9BQS9FLFdBQThGOEIsSUFBOUYsQ0FwQlc7O0FBQUE7QUFzQmpCNUIsdUJBQVM7O0FBdEJRO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLEtBQUg7O0FBQUEsb0JBQVo4QyxZQUFZO0FBQUE7QUFBQTtBQUFBLEtBQWxCOztBQTJCQSxzQkFFRTtBQUFNLFlBQVEsRUFBRUEsWUFBaEI7QUFBQSw0QkFFRSxxRUFBQyxtRUFBRDtBQUFhLFFBQUUsRUFBQyxjQUFoQjtBQUErQixhQUFPLEVBQUViLFNBQXhDO0FBQW1ELGNBQVEsRUFBRVM7QUFBN0Q7QUFBQTtBQUFBO0FBQUE7QUFBQSxZQUZGLGVBR0U7QUFBUSxjQUFRLEVBQUVwQyxVQUFVLElBQUlFLFFBQWQsSUFBMEJOLFNBQTVDO0FBQXVELFFBQUUsRUFBQyxRQUExRDtBQUFtRSxlQUFTLEVBQUMsS0FBN0U7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsWUFIRixFQU1HRSxLQUFLLGlCQUNKO0FBQUssZUFBUyxFQUFDLFlBQWY7QUFBNEIsVUFBSSxFQUFDLE9BQWpDO0FBQUEsZ0JBQ0dBO0FBREg7QUFBQTtBQUFBO0FBQUE7QUFBQSxZQVBKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxVQUZGO0FBZ0JEOztHQXZHdUJQLFk7VUFPUGdCLGlFLEVBQ0VFLG1FLEVBRUZFLHFEOzs7S0FWT3BCLFkiLCJmaWxlIjoic3RhdGljL3dlYnBhY2svcGFnZXMvcGF5bWVudHMvW29yZGVySURdLmRhN2IzZGQyZDk3OGVmYTExMjhmLmhvdC11cGRhdGUuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgUmVhY3QsIHsgdXNlU3RhdGUsIHVzZUVmZmVjdCB9IGZyb20gXCJyZWFjdFwiO1xyXG5pbXBvcnQge0NhcmRFbGVtZW50LCB1c2VTdHJpcGUsIHVzZUVsZW1lbnRzfSBmcm9tIFwiQHN0cmlwZS9yZWFjdC1zdHJpcGUtanNcIjtcclxuaW1wb3J0IGF4aW9zIGZyb20gJ2F4aW9zJztcclxuaW1wb3J0IHt1c2VSb3V0ZXJ9IGZyb20gJ25leHQvcm91dGVyJztcclxuXHJcbmV4cG9ydCBkZWZhdWx0IGZ1bmN0aW9uIENoZWNrb3V0Rm9ybSh7b3JkZXJJRCwgYW1vdW50LCBvblN1Y2Nlc3N9KSB7XHJcblxyXG4gIGNvbnN0IFtzdWNjZWVkZWQsIHNldFN1Y2NlZWRlZF0gPSB1c2VTdGF0ZShmYWxzZSk7XHJcbiAgY29uc3QgW2Vycm9yLCBzZXRFcnJvcl0gPSB1c2VTdGF0ZShudWxsKTtcclxuICBjb25zdCBbcHJvY2Vzc2luZywgc2V0UHJvY2Vzc2luZ10gPSB1c2VTdGF0ZSgnJyk7XHJcbiAgY29uc3QgW2Rpc2FibGVkLCBzZXREaXNhYmxlZF0gPSB1c2VTdGF0ZShmYWxzZSk7XHJcbiAgY29uc3QgW2NsaWVudFNlY3JldCwgc2V0Q2xpZW50U2VjcmV0XSA9IHVzZVN0YXRlKCcnKTtcclxuICBjb25zdCBzdHJpcGUgPSB1c2VTdHJpcGUoKTtcclxuICBjb25zdCBlbGVtZW50cyA9IHVzZUVsZW1lbnRzKCk7XHJcblxyXG4gIGNvbnN0IHJvdXRlciA9IHVzZVJvdXRlcigpO1xyXG5cclxuICB1c2VFZmZlY3QoKCkgPT4ge1xyXG5cclxuICAgIGNvbnN0IGdldEludGVudCA9IGFzeW5jICgpID0+IHtcclxuICAgICAgICBjb25zdCByZXMgPSBheGlvcy5wb3N0KGBodHRwczovLzI5b2RkZWUyZWguZXhlY3V0ZS1hcGkudXMtZWFzdC0xLmFtYXpvbmF3cy5jb20vZGV2L29yZGVycy8ke29yZGVySUR9L2ludGVudGApO1xyXG4gICAgICAgIHNldENsaWVudFNlY3JldChkYXRhLmNsaWVudFNlY3JldCk7XHJcbiAgICB9XHJcblxyXG4gICAgd2luZG93XHJcbiAgICAgIC5mZXRjaChgaHR0cHM6Ly8yOW9kZGVlMmVoLmV4ZWN1dGUtYXBpLnVzLWVhc3QtMS5hbWF6b25hd3MuY29tL2Rldi9vcmRlcnMvJHtvcmRlcklEfS9pbnRlbnRgLCB7XHJcbiAgICAgICAgbWV0aG9kOiBcIlBPU1RcIixcclxuICAgICAgICBoZWFkZXJzOiB7XHJcbiAgICAgICAgICBcIkNvbnRlbnQtVHlwZVwiOiBcImFwcGxpY2F0aW9uL2pzb25cIlxyXG4gICAgICAgIH0sXHJcbiAgICAgICAgYm9keTogSlNPTi5zdHJpbmdpZnkoe2Ftb3VudH0pXHJcbiAgICAgIH0pXHJcbiAgICAgIC50aGVuKHJlcyA9PiB7XHJcbiAgICAgICAgcmV0dXJuIHJlcy5qc29uKCk7XHJcbiAgICAgIH0pXHJcbiAgICAgIC50aGVuKGRhdGEgPT4ge1xyXG4gICAgICAgIHNldENsaWVudFNlY3JldChkYXRhLmNsaWVudFNlY3JldCk7XHJcbiAgICAgIH0pO1xyXG4gIH0sIFtdKTtcclxuXHJcbiAgY29uc3QgY2FyZFN0eWxlID0ge1xyXG4gICAgc3R5bGU6IHtcclxuICAgICAgYmFzZToge1xyXG4gICAgICAgIGNvbG9yOiBcIiMzMjMyNWRcIixcclxuICAgICAgICBmb250RmFtaWx5OiAnUG9wcGlucywgc2Fucy1zZXJpZicsXHJcbiAgICAgICAgZm9udFNtb290aGluZzogXCJhbnRpYWxpYXNlZFwiLFxyXG4gICAgICAgIGZvbnRTaXplOiBcIjE2cHhcIixcclxuICAgICAgICBcIjo6cGxhY2Vob2xkZXJcIjoge1xyXG4gICAgICAgICAgY29sb3I6IFwiIzMyMzI1ZFwiXHJcbiAgICAgICAgfVxyXG4gICAgICB9LFxyXG4gICAgICBpbnZhbGlkOiB7XHJcbiAgICAgICAgY29sb3I6IFwiI2ZhNzU1YVwiLFxyXG4gICAgICAgIGljb25Db2xvcjogXCIjZmE3NTVhXCJcclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH07XHJcblxyXG4gIGNvbnN0IGhhbmRsZUNoYW5nZSA9IGFzeW5jIChldmVudCkgPT4ge1xyXG5cclxuICAgIHNldERpc2FibGVkKGV2ZW50LmVtcHR5KTtcclxuICAgIHNldEVycm9yKGV2ZW50LmVycm9yID8gZXZlbnQuZXJyb3IubWVzc2FnZSA6IFwiXCIpO1xyXG5cclxuICB9O1xyXG5cclxuICBjb25zdCBoYW5kbGVTdWJtaXQgPSBhc3luYyBldiA9PiB7XHJcblxyXG4gICAgZXYucHJldmVudERlZmF1bHQoKTtcclxuICAgIHNldFByb2Nlc3NpbmcodHJ1ZSk7XHJcblxyXG4gICAgY29uc3QgcGF5bG9hZCA9IGF3YWl0IHN0cmlwZS5jb25maXJtQ2FyZFBheW1lbnQoY2xpZW50U2VjcmV0LCB7XHJcbiAgICAgIHBheW1lbnRfbWV0aG9kOiB7XHJcbiAgICAgICAgY2FyZDogZWxlbWVudHMuZ2V0RWxlbWVudChDYXJkRWxlbWVudClcclxuICAgICAgfVxyXG4gICAgfSk7XHJcblxyXG4gICAgaWYgKHBheWxvYWQuZXJyb3IpIHtcclxuICAgICAgc2V0RXJyb3IoYFBheW1lbnQgZmFpbGVkICR7cGF5bG9hZC5lcnJvci5tZXNzYWdlfWApO1xyXG4gICAgICBzZXRQcm9jZXNzaW5nKGZhbHNlKTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHNldEVycm9yKG51bGwpO1xyXG4gICAgICBzZXRQcm9jZXNzaW5nKGZhbHNlKTtcclxuICAgICAgc2V0U3VjY2VlZGVkKHRydWUpO1xyXG5cclxuICAgICAgY29uc3QgYm9keSA9IHtwYXltZW50UmVzdWx0OiBwYXlsb2FkfTtcclxuICAgICAgYXdhaXQgYXhpb3MucHV0KGBodHRwczovLzI5b2RkZWUyZWguZXhlY3V0ZS1hcGkudXMtZWFzdC0xLmFtYXpvbmF3cy5jb20vZGV2L29yZGVycy8ke29yZGVySUR9L3BheWAsIGJvZHkpO1xyXG5cclxuICAgICAgb25TdWNjZXNzKCk7XHJcbiAgICB9XHJcblxyXG4gIH07XHJcblxyXG4gIHJldHVybiAoXHJcblxyXG4gICAgPGZvcm0gb25TdWJtaXQ9e2hhbmRsZVN1Ym1pdH0+XHJcblxyXG4gICAgICA8Q2FyZEVsZW1lbnQgaWQ9XCJjYXJkLWVsZW1lbnRcIiBvcHRpb25zPXtjYXJkU3R5bGV9IG9uQ2hhbmdlPXtoYW5kbGVDaGFuZ2V9IC8+XHJcbiAgICAgIDxidXR0b24gZGlzYWJsZWQ9e3Byb2Nlc3NpbmcgfHwgZGlzYWJsZWQgfHwgc3VjY2VlZGVkfSBpZD1cInN1Ym1pdFwiIGNsYXNzTmFtZT1cImJ0blwiPlBheSBOb3c8L2J1dHRvbj5cclxuXHJcbiAgICAgIHsvKiBTaG93IGFueSBlcnJvciB0aGF0IGhhcHBlbnMgd2hlbiBwcm9jZXNzaW5nIHRoZSBwYXltZW50ICovfVxyXG4gICAgICB7ZXJyb3IgJiYgKFxyXG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiY2FyZC1lcnJvclwiIHJvbGU9XCJhbGVydFwiPlxyXG4gICAgICAgICAge2Vycm9yfVxyXG4gICAgICAgIDwvZGl2PlxyXG4gICAgICApfVxyXG5cclxuICAgIDwvZm9ybT5cclxuICApO1xyXG59Il0sInNvdXJjZVJvb3QiOiIifQ==