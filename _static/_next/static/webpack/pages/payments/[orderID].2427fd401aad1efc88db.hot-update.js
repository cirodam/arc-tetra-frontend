webpackHotUpdate_N_E("pages/payments/[orderID]",{

/***/ "./components/CheckoutForm.js":
/*!************************************!*\
  !*** ./components/CheckoutForm.js ***!
  \************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return CheckoutForm; });
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "./node_modules/react/jsx-dev-runtime.js");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var C_Users_Tyler_Desktop_Projects_arc_tetra_software_v2_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/@babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var C_Users_Tyler_Desktop_Projects_arc_tetra_software_v2_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(C_Users_Tyler_Desktop_Projects_arc_tetra_software_v2_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var C_Users_Tyler_Desktop_Projects_arc_tetra_software_v2_node_modules_babel_runtime_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/esm/asyncToGenerator */ "./node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _stripe_react_stripe_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @stripe/react-stripe-js */ "./node_modules/@stripe/react-stripe-js/dist/react-stripe.umd.js");
/* harmony import */ var _stripe_react_stripe_js__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_stripe_react_stripe_js__WEBPACK_IMPORTED_MODULE_4__);




var _jsxFileName = "C:\\Users\\Tyler\\Desktop\\Projects\\arc-tetra-software-v2\\components\\CheckoutForm.js",
    _s = $RefreshSig$();



function CheckoutForm() {
  _s();

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_3__["useState"])(false),
      succeeded = _useState[0],
      setSucceeded = _useState[1];

  var _useState2 = Object(react__WEBPACK_IMPORTED_MODULE_3__["useState"])(null),
      error = _useState2[0],
      setError = _useState2[1];

  var _useState3 = Object(react__WEBPACK_IMPORTED_MODULE_3__["useState"])(''),
      processing = _useState3[0],
      setProcessing = _useState3[1];

  var _useState4 = Object(react__WEBPACK_IMPORTED_MODULE_3__["useState"])(false),
      disabled = _useState4[0],
      setDisabled = _useState4[1];

  var _useState5 = Object(react__WEBPACK_IMPORTED_MODULE_3__["useState"])(''),
      clientSecret = _useState5[0],
      setClientSecret = _useState5[1];

  var stripe = Object(_stripe_react_stripe_js__WEBPACK_IMPORTED_MODULE_4__["useStripe"])();
  var elements = Object(_stripe_react_stripe_js__WEBPACK_IMPORTED_MODULE_4__["useElements"])();
  Object(react__WEBPACK_IMPORTED_MODULE_3__["useEffect"])(function () {
    // Create PaymentIntent as soon as the page loads
    window.fetch("https://29oddee2eh.execute-api.us-east-1.amazonaws.com/dev/stripe", {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        amount: 100
      })
    }).then(function (res) {
      return res.json();
    }).then(function (data) {
      setClientSecret(data.clientSecret);
    });
  }, []);
  var cardStyle = {
    style: {
      base: {
        color: "#32325d",
        fontFamily: 'Arial, sans-serif',
        fontSmoothing: "antialiased",
        fontSize: "16px",
        "::placeholder": {
          color: "#32325d"
        }
      },
      invalid: {
        color: "#fa755a",
        iconColor: "#fa755a"
      }
    }
  };

  var handleChange = /*#__PURE__*/function () {
    var _ref = Object(C_Users_Tyler_Desktop_Projects_arc_tetra_software_v2_node_modules_babel_runtime_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_2__["default"])( /*#__PURE__*/C_Users_Tyler_Desktop_Projects_arc_tetra_software_v2_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.mark(function _callee(event) {
      return C_Users_Tyler_Desktop_Projects_arc_tetra_software_v2_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              setDisabled(event.empty);
              setError(event.error ? event.error.message : "");

            case 2:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }));

    return function handleChange(_x) {
      return _ref.apply(this, arguments);
    };
  }();

  var handleSubmit = /*#__PURE__*/function () {
    var _ref2 = Object(C_Users_Tyler_Desktop_Projects_arc_tetra_software_v2_node_modules_babel_runtime_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_2__["default"])( /*#__PURE__*/C_Users_Tyler_Desktop_Projects_arc_tetra_software_v2_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.mark(function _callee2(ev) {
      var payload;
      return C_Users_Tyler_Desktop_Projects_arc_tetra_software_v2_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.wrap(function _callee2$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
              ev.preventDefault();
              setProcessing(true);
              _context2.next = 4;
              return stripe.confirmCardPayment(clientSecret, {
                payment_method: {
                  card: elements.getElement(_stripe_react_stripe_js__WEBPACK_IMPORTED_MODULE_4__["CardElement"])
                }
              });

            case 4:
              payload = _context2.sent;

              if (payload.error) {
                setError("Payment failed ".concat(payload.error.message));
                setProcessing(false);
              } else {
                setError(null);
                setProcessing(false);
                setSucceeded(true);
              }

            case 6:
            case "end":
              return _context2.stop();
          }
        }
      }, _callee2);
    }));

    return function handleSubmit(_x2) {
      return _ref2.apply(this, arguments);
    };
  }();

  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("form", {
    id: "payment-form",
    onSubmit: handleSubmit,
    children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_stripe_react_stripe_js__WEBPACK_IMPORTED_MODULE_4__["CardElement"], {
      id: "card-element",
      options: cardStyle,
      onChange: handleChange
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 83,
      columnNumber: 7
    }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("button", {
      disabled: processing || disabled || succeeded,
      id: "submit",
      children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("span", {
        id: "button-text",
        children: processing ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
          className: "spinner",
          id: "spinner"
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 87,
          columnNumber: 13
        }, this) : "Pay now"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 85,
        columnNumber: 9
      }, this)
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 84,
      columnNumber: 7
    }, this), error && /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
      className: "card-error",
      role: "alert",
      children: error
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 96,
      columnNumber: 9
    }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("p", {
      className: succeeded ? "result-message" : "result-message hidden",
      children: ["Payment succeeded, see the result in your", /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
        href: "https://dashboard.stripe.com/test/payments",
        children: [" ", "Stripe dashboard."]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 104,
        columnNumber: 9
      }, this), " Refresh the page to pay again."]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 102,
      columnNumber: 7
    }, this)]
  }, void 0, true, {
    fileName: _jsxFileName,
    lineNumber: 81,
    columnNumber: 5
  }, this);
}

_s(CheckoutForm, "GhxqeXSXbsPzU6SqFIQHLcFON5I=", false, function () {
  return [_stripe_react_stripe_js__WEBPACK_IMPORTED_MODULE_4__["useStripe"], _stripe_react_stripe_js__WEBPACK_IMPORTED_MODULE_4__["useElements"]];
});

_c = CheckoutForm;

var _c;

$RefreshReg$(_c, "CheckoutForm");

;
    var _a, _b;
    // Legacy CSS implementations will `eval` browser code in a Node.js context
    // to extract CSS. For backwards compatibility, we need to check we're in a
    // browser context before continuing.
    if (typeof self !== 'undefined' &&
        // AMP / No-JS mode does not inject these helpers:
        '$RefreshHelpers$' in self) {
        var currentExports = module.__proto__.exports;
        var prevExports = (_b = (_a = module.hot.data) === null || _a === void 0 ? void 0 : _a.prevExports) !== null && _b !== void 0 ? _b : null;
        // This cannot happen in MainTemplate because the exports mismatch between
        // templating and execution.
        self.$RefreshHelpers$.registerExportsForReactRefresh(currentExports, module.i);
        // A module can be accepted automatically based on its exports, e.g. when
        // it is a Refresh Boundary.
        if (self.$RefreshHelpers$.isReactRefreshBoundary(currentExports)) {
            // Save the previous exports on update so we can compare the boundary
            // signatures.
            module.hot.dispose(function (data) {
                data.prevExports = currentExports;
            });
            // Unconditionally accept an update to this module, we'll check if it's
            // still a Refresh Boundary later.
            module.hot.accept();
            // This field is set when the previous version of this module was a
            // Refresh Boundary, letting us know we need to check for invalidation or
            // enqueue an update.
            if (prevExports !== null) {
                // A boundary can become ineligible if its exports are incompatible
                // with the previous exports.
                //
                // For example, if you add/remove/change exports, we'll want to
                // re-execute the importing modules, and force those components to
                // re-render. Similarly, if you convert a class component to a
                // function, we want to invalidate the boundary.
                if (self.$RefreshHelpers$.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)) {
                    module.hot.invalidate();
                }
                else {
                    self.$RefreshHelpers$.scheduleUpdate();
                }
            }
        }
        else {
            // Since we just executed the code for the module, it's possible that the
            // new exports made it ineligible for being a boundary.
            // We only care about the case when we were _previously_ a boundary,
            // because we already accepted this update (accidental side effect).
            var isNoLongerABoundary = prevExports !== null;
            if (isNoLongerABoundary) {
                module.hot.invalidate();
            }
        }
    }

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../node_modules/next/dist/compiled/webpack/harmony-module.js */ "./node_modules/next/dist/compiled/webpack/harmony-module.js")(module)))

/***/ })

})
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9fTl9FLy4vY29tcG9uZW50cy9DaGVja291dEZvcm0uanMiXSwibmFtZXMiOlsiQ2hlY2tvdXRGb3JtIiwidXNlU3RhdGUiLCJzdWNjZWVkZWQiLCJzZXRTdWNjZWVkZWQiLCJlcnJvciIsInNldEVycm9yIiwicHJvY2Vzc2luZyIsInNldFByb2Nlc3NpbmciLCJkaXNhYmxlZCIsInNldERpc2FibGVkIiwiY2xpZW50U2VjcmV0Iiwic2V0Q2xpZW50U2VjcmV0Iiwic3RyaXBlIiwidXNlU3RyaXBlIiwiZWxlbWVudHMiLCJ1c2VFbGVtZW50cyIsInVzZUVmZmVjdCIsIndpbmRvdyIsImZldGNoIiwibWV0aG9kIiwiaGVhZGVycyIsImJvZHkiLCJKU09OIiwic3RyaW5naWZ5IiwiYW1vdW50IiwidGhlbiIsInJlcyIsImpzb24iLCJkYXRhIiwiY2FyZFN0eWxlIiwic3R5bGUiLCJiYXNlIiwiY29sb3IiLCJmb250RmFtaWx5IiwiZm9udFNtb290aGluZyIsImZvbnRTaXplIiwiaW52YWxpZCIsImljb25Db2xvciIsImhhbmRsZUNoYW5nZSIsImV2ZW50IiwiZW1wdHkiLCJtZXNzYWdlIiwiaGFuZGxlU3VibWl0IiwiZXYiLCJwcmV2ZW50RGVmYXVsdCIsImNvbmZpcm1DYXJkUGF5bWVudCIsInBheW1lbnRfbWV0aG9kIiwiY2FyZCIsImdldEVsZW1lbnQiLCJDYXJkRWxlbWVudCIsInBheWxvYWQiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUNBO0FBRWUsU0FBU0EsWUFBVCxHQUF3QjtBQUFBOztBQUFBLGtCQUVIQyxzREFBUSxDQUFDLEtBQUQsQ0FGTDtBQUFBLE1BRTlCQyxTQUY4QjtBQUFBLE1BRW5CQyxZQUZtQjs7QUFBQSxtQkFHWEYsc0RBQVEsQ0FBQyxJQUFELENBSEc7QUFBQSxNQUc5QkcsS0FIOEI7QUFBQSxNQUd2QkMsUUFIdUI7O0FBQUEsbUJBSURKLHNEQUFRLENBQUMsRUFBRCxDQUpQO0FBQUEsTUFJOUJLLFVBSjhCO0FBQUEsTUFJbEJDLGFBSmtCOztBQUFBLG1CQUtMTixzREFBUSxDQUFDLEtBQUQsQ0FMSDtBQUFBLE1BSzlCTyxRQUw4QjtBQUFBLE1BS3BCQyxXQUxvQjs7QUFBQSxtQkFNR1Isc0RBQVEsQ0FBQyxFQUFELENBTlg7QUFBQSxNQU05QlMsWUFOOEI7QUFBQSxNQU1oQkMsZUFOZ0I7O0FBT3JDLE1BQU1DLE1BQU0sR0FBR0MseUVBQVMsRUFBeEI7QUFDQSxNQUFNQyxRQUFRLEdBQUdDLDJFQUFXLEVBQTVCO0FBRUFDLHlEQUFTLENBQUMsWUFBTTtBQUNkO0FBQ0FDLFVBQU0sQ0FDSEMsS0FESCxDQUNTLG1FQURULEVBQzhFO0FBQzFFQyxZQUFNLEVBQUUsTUFEa0U7QUFFMUVDLGFBQU8sRUFBRTtBQUNQLHdCQUFnQjtBQURULE9BRmlFO0FBSzFFQyxVQUFJLEVBQUVDLElBQUksQ0FBQ0MsU0FBTCxDQUFlO0FBQUNDLGNBQU0sRUFBRTtBQUFULE9BQWY7QUFMb0UsS0FEOUUsRUFRR0MsSUFSSCxDQVFRLFVBQUFDLEdBQUcsRUFBSTtBQUNYLGFBQU9BLEdBQUcsQ0FBQ0MsSUFBSixFQUFQO0FBQ0QsS0FWSCxFQVdHRixJQVhILENBV1EsVUFBQUcsSUFBSSxFQUFJO0FBQ1pqQixxQkFBZSxDQUFDaUIsSUFBSSxDQUFDbEIsWUFBTixDQUFmO0FBQ0QsS0FiSDtBQWNELEdBaEJRLEVBZ0JOLEVBaEJNLENBQVQ7QUFrQkEsTUFBTW1CLFNBQVMsR0FBRztBQUNoQkMsU0FBSyxFQUFFO0FBQ0xDLFVBQUksRUFBRTtBQUNKQyxhQUFLLEVBQUUsU0FESDtBQUVKQyxrQkFBVSxFQUFFLG1CQUZSO0FBR0pDLHFCQUFhLEVBQUUsYUFIWDtBQUlKQyxnQkFBUSxFQUFFLE1BSk47QUFLSix5QkFBaUI7QUFDZkgsZUFBSyxFQUFFO0FBRFE7QUFMYixPQUREO0FBVUxJLGFBQU8sRUFBRTtBQUNQSixhQUFLLEVBQUUsU0FEQTtBQUVQSyxpQkFBUyxFQUFFO0FBRko7QUFWSjtBQURTLEdBQWxCOztBQWtCQSxNQUFNQyxZQUFZO0FBQUEsa1VBQUcsaUJBQU9DLEtBQVA7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVuQjlCLHlCQUFXLENBQUM4QixLQUFLLENBQUNDLEtBQVAsQ0FBWDtBQUNBbkMsc0JBQVEsQ0FBQ2tDLEtBQUssQ0FBQ25DLEtBQU4sR0FBY21DLEtBQUssQ0FBQ25DLEtBQU4sQ0FBWXFDLE9BQTFCLEdBQW9DLEVBQXJDLENBQVI7O0FBSG1CO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLEtBQUg7O0FBQUEsb0JBQVpILFlBQVk7QUFBQTtBQUFBO0FBQUEsS0FBbEI7O0FBT0EsTUFBTUksWUFBWTtBQUFBLG1VQUFHLGtCQUFNQyxFQUFOO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVuQkEsZ0JBQUUsQ0FBQ0MsY0FBSDtBQUNBckMsMkJBQWEsQ0FBQyxJQUFELENBQWI7QUFIbUI7QUFBQSxxQkFLR0ssTUFBTSxDQUFDaUMsa0JBQVAsQ0FBMEJuQyxZQUExQixFQUF3QztBQUM1RG9DLDhCQUFjLEVBQUU7QUFDZEMsc0JBQUksRUFBRWpDLFFBQVEsQ0FBQ2tDLFVBQVQsQ0FBb0JDLG1FQUFwQjtBQURRO0FBRDRDLGVBQXhDLENBTEg7O0FBQUE7QUFLYkMscUJBTGE7O0FBV25CLGtCQUFJQSxPQUFPLENBQUM5QyxLQUFaLEVBQW1CO0FBQ2pCQyx3QkFBUSwwQkFBbUI2QyxPQUFPLENBQUM5QyxLQUFSLENBQWNxQyxPQUFqQyxFQUFSO0FBQ0FsQyw2QkFBYSxDQUFDLEtBQUQsQ0FBYjtBQUNELGVBSEQsTUFHTztBQUNMRix3QkFBUSxDQUFDLElBQUQsQ0FBUjtBQUNBRSw2QkFBYSxDQUFDLEtBQUQsQ0FBYjtBQUNBSiw0QkFBWSxDQUFDLElBQUQsQ0FBWjtBQUNEOztBQWxCa0I7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsS0FBSDs7QUFBQSxvQkFBWnVDLFlBQVk7QUFBQTtBQUFBO0FBQUEsS0FBbEI7O0FBc0JBLHNCQUVFO0FBQU0sTUFBRSxFQUFDLGNBQVQ7QUFBd0IsWUFBUSxFQUFFQSxZQUFsQztBQUFBLDRCQUVFLHFFQUFDLG1FQUFEO0FBQWEsUUFBRSxFQUFDLGNBQWhCO0FBQStCLGFBQU8sRUFBRWIsU0FBeEM7QUFBbUQsY0FBUSxFQUFFUztBQUE3RDtBQUFBO0FBQUE7QUFBQTtBQUFBLFlBRkYsZUFHRTtBQUFRLGNBQVEsRUFBRWhDLFVBQVUsSUFBSUUsUUFBZCxJQUEwQk4sU0FBNUM7QUFBdUQsUUFBRSxFQUFDLFFBQTFEO0FBQUEsNkJBQ0U7QUFBTSxVQUFFLEVBQUMsYUFBVDtBQUFBLGtCQUNHSSxVQUFVLGdCQUNUO0FBQUssbUJBQVMsRUFBQyxTQUFmO0FBQXlCLFlBQUUsRUFBQztBQUE1QjtBQUFBO0FBQUE7QUFBQTtBQUFBLGdCQURTLEdBR1Q7QUFKSjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREY7QUFBQTtBQUFBO0FBQUE7QUFBQSxZQUhGLEVBY0dGLEtBQUssaUJBQ0o7QUFBSyxlQUFTLEVBQUMsWUFBZjtBQUE0QixVQUFJLEVBQUMsT0FBakM7QUFBQSxnQkFDR0E7QUFESDtBQUFBO0FBQUE7QUFBQTtBQUFBLFlBZkosZUFxQkU7QUFBRyxlQUFTLEVBQUVGLFNBQVMsR0FBRyxnQkFBSCxHQUFzQix1QkFBN0M7QUFBQSwyRUFFRTtBQUNFLFlBQUksOENBRE47QUFBQSxtQkFHRyxHQUhIO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxjQUZGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxZQXJCRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsVUFGRjtBQW1DRDs7R0E5R3VCRixZO1VBT1BhLGlFLEVBQ0VFLG1FOzs7S0FSS2YsWSIsImZpbGUiOiJzdGF0aWMvd2VicGFjay9wYWdlcy9wYXltZW50cy9bb3JkZXJJRF0uMjQyN2ZkNDAxYWFkMWVmYzg4ZGIuaG90LXVwZGF0ZS5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBSZWFjdCwgeyB1c2VTdGF0ZSwgdXNlRWZmZWN0IH0gZnJvbSBcInJlYWN0XCI7XHJcbmltcG9ydCB7Q2FyZEVsZW1lbnQsIHVzZVN0cmlwZSwgdXNlRWxlbWVudHN9IGZyb20gXCJAc3RyaXBlL3JlYWN0LXN0cmlwZS1qc1wiO1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgZnVuY3Rpb24gQ2hlY2tvdXRGb3JtKCkge1xyXG5cclxuICBjb25zdCBbc3VjY2VlZGVkLCBzZXRTdWNjZWVkZWRdID0gdXNlU3RhdGUoZmFsc2UpO1xyXG4gIGNvbnN0IFtlcnJvciwgc2V0RXJyb3JdID0gdXNlU3RhdGUobnVsbCk7XHJcbiAgY29uc3QgW3Byb2Nlc3NpbmcsIHNldFByb2Nlc3NpbmddID0gdXNlU3RhdGUoJycpO1xyXG4gIGNvbnN0IFtkaXNhYmxlZCwgc2V0RGlzYWJsZWRdID0gdXNlU3RhdGUoZmFsc2UpO1xyXG4gIGNvbnN0IFtjbGllbnRTZWNyZXQsIHNldENsaWVudFNlY3JldF0gPSB1c2VTdGF0ZSgnJyk7XHJcbiAgY29uc3Qgc3RyaXBlID0gdXNlU3RyaXBlKCk7XHJcbiAgY29uc3QgZWxlbWVudHMgPSB1c2VFbGVtZW50cygpO1xyXG5cclxuICB1c2VFZmZlY3QoKCkgPT4ge1xyXG4gICAgLy8gQ3JlYXRlIFBheW1lbnRJbnRlbnQgYXMgc29vbiBhcyB0aGUgcGFnZSBsb2Fkc1xyXG4gICAgd2luZG93XHJcbiAgICAgIC5mZXRjaChcImh0dHBzOi8vMjlvZGRlZTJlaC5leGVjdXRlLWFwaS51cy1lYXN0LTEuYW1hem9uYXdzLmNvbS9kZXYvc3RyaXBlXCIsIHtcclxuICAgICAgICBtZXRob2Q6IFwiUE9TVFwiLFxyXG4gICAgICAgIGhlYWRlcnM6IHtcclxuICAgICAgICAgIFwiQ29udGVudC1UeXBlXCI6IFwiYXBwbGljYXRpb24vanNvblwiXHJcbiAgICAgICAgfSxcclxuICAgICAgICBib2R5OiBKU09OLnN0cmluZ2lmeSh7YW1vdW50OiAxMDB9KVxyXG4gICAgICB9KVxyXG4gICAgICAudGhlbihyZXMgPT4ge1xyXG4gICAgICAgIHJldHVybiByZXMuanNvbigpO1xyXG4gICAgICB9KVxyXG4gICAgICAudGhlbihkYXRhID0+IHtcclxuICAgICAgICBzZXRDbGllbnRTZWNyZXQoZGF0YS5jbGllbnRTZWNyZXQpO1xyXG4gICAgICB9KTtcclxuICB9LCBbXSk7XHJcblxyXG4gIGNvbnN0IGNhcmRTdHlsZSA9IHtcclxuICAgIHN0eWxlOiB7XHJcbiAgICAgIGJhc2U6IHtcclxuICAgICAgICBjb2xvcjogXCIjMzIzMjVkXCIsXHJcbiAgICAgICAgZm9udEZhbWlseTogJ0FyaWFsLCBzYW5zLXNlcmlmJyxcclxuICAgICAgICBmb250U21vb3RoaW5nOiBcImFudGlhbGlhc2VkXCIsXHJcbiAgICAgICAgZm9udFNpemU6IFwiMTZweFwiLFxyXG4gICAgICAgIFwiOjpwbGFjZWhvbGRlclwiOiB7XHJcbiAgICAgICAgICBjb2xvcjogXCIjMzIzMjVkXCJcclxuICAgICAgICB9XHJcbiAgICAgIH0sXHJcbiAgICAgIGludmFsaWQ6IHtcclxuICAgICAgICBjb2xvcjogXCIjZmE3NTVhXCIsXHJcbiAgICAgICAgaWNvbkNvbG9yOiBcIiNmYTc1NWFcIlxyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfTtcclxuXHJcbiAgY29uc3QgaGFuZGxlQ2hhbmdlID0gYXN5bmMgKGV2ZW50KSA9PiB7XHJcblxyXG4gICAgc2V0RGlzYWJsZWQoZXZlbnQuZW1wdHkpO1xyXG4gICAgc2V0RXJyb3IoZXZlbnQuZXJyb3IgPyBldmVudC5lcnJvci5tZXNzYWdlIDogXCJcIik7XHJcblxyXG4gIH07XHJcblxyXG4gIGNvbnN0IGhhbmRsZVN1Ym1pdCA9IGFzeW5jIGV2ID0+IHtcclxuXHJcbiAgICBldi5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgc2V0UHJvY2Vzc2luZyh0cnVlKTtcclxuXHJcbiAgICBjb25zdCBwYXlsb2FkID0gYXdhaXQgc3RyaXBlLmNvbmZpcm1DYXJkUGF5bWVudChjbGllbnRTZWNyZXQsIHtcclxuICAgICAgcGF5bWVudF9tZXRob2Q6IHtcclxuICAgICAgICBjYXJkOiBlbGVtZW50cy5nZXRFbGVtZW50KENhcmRFbGVtZW50KVxyXG4gICAgICB9XHJcbiAgICB9KTtcclxuXHJcbiAgICBpZiAocGF5bG9hZC5lcnJvcikge1xyXG4gICAgICBzZXRFcnJvcihgUGF5bWVudCBmYWlsZWQgJHtwYXlsb2FkLmVycm9yLm1lc3NhZ2V9YCk7XHJcbiAgICAgIHNldFByb2Nlc3NpbmcoZmFsc2UpO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgc2V0RXJyb3IobnVsbCk7XHJcbiAgICAgIHNldFByb2Nlc3NpbmcoZmFsc2UpO1xyXG4gICAgICBzZXRTdWNjZWVkZWQodHJ1ZSk7XHJcbiAgICB9XHJcblxyXG4gIH07XHJcblxyXG4gIHJldHVybiAoXHJcblxyXG4gICAgPGZvcm0gaWQ9XCJwYXltZW50LWZvcm1cIiBvblN1Ym1pdD17aGFuZGxlU3VibWl0fT5cclxuXHJcbiAgICAgIDxDYXJkRWxlbWVudCBpZD1cImNhcmQtZWxlbWVudFwiIG9wdGlvbnM9e2NhcmRTdHlsZX0gb25DaGFuZ2U9e2hhbmRsZUNoYW5nZX0gLz5cclxuICAgICAgPGJ1dHRvbiBkaXNhYmxlZD17cHJvY2Vzc2luZyB8fCBkaXNhYmxlZCB8fCBzdWNjZWVkZWR9IGlkPVwic3VibWl0XCI+XHJcbiAgICAgICAgPHNwYW4gaWQ9XCJidXR0b24tdGV4dFwiPlxyXG4gICAgICAgICAge3Byb2Nlc3NpbmcgPyAoXHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwic3Bpbm5lclwiIGlkPVwic3Bpbm5lclwiPjwvZGl2PlxyXG4gICAgICAgICAgKSA6IChcclxuICAgICAgICAgICAgXCJQYXkgbm93XCJcclxuICAgICAgICAgICl9XHJcbiAgICAgICAgPC9zcGFuPlxyXG4gICAgICA8L2J1dHRvbj5cclxuXHJcbiAgICAgIHsvKiBTaG93IGFueSBlcnJvciB0aGF0IGhhcHBlbnMgd2hlbiBwcm9jZXNzaW5nIHRoZSBwYXltZW50ICovfVxyXG4gICAgICB7ZXJyb3IgJiYgKFxyXG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiY2FyZC1lcnJvclwiIHJvbGU9XCJhbGVydFwiPlxyXG4gICAgICAgICAge2Vycm9yfVxyXG4gICAgICAgIDwvZGl2PlxyXG4gICAgICApfVxyXG5cclxuICAgICAgey8qIFNob3cgYSBzdWNjZXNzIG1lc3NhZ2UgdXBvbiBjb21wbGV0aW9uICovfVxyXG4gICAgICA8cCBjbGFzc05hbWU9e3N1Y2NlZWRlZCA/IFwicmVzdWx0LW1lc3NhZ2VcIiA6IFwicmVzdWx0LW1lc3NhZ2UgaGlkZGVuXCJ9PlxyXG4gICAgICAgIFBheW1lbnQgc3VjY2VlZGVkLCBzZWUgdGhlIHJlc3VsdCBpbiB5b3VyXHJcbiAgICAgICAgPGFcclxuICAgICAgICAgIGhyZWY9e2BodHRwczovL2Rhc2hib2FyZC5zdHJpcGUuY29tL3Rlc3QvcGF5bWVudHNgfVxyXG4gICAgICAgID5cclxuICAgICAgICAgIHtcIiBcIn1cclxuICAgICAgICAgIFN0cmlwZSBkYXNoYm9hcmQuXHJcbiAgICAgICAgPC9hPiBSZWZyZXNoIHRoZSBwYWdlIHRvIHBheSBhZ2Fpbi5cclxuICAgICAgPC9wPlxyXG5cclxuICAgIDwvZm9ybT5cclxuICApO1xyXG59Il0sInNvdXJjZVJvb3QiOiIifQ==