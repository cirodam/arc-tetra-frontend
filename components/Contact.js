import React, {useState} from 'react';
import axios from 'axios';

const Contact = () => {

    const [formSubmitted, setFormSubmitted] = useState(false);
    const [formData, setFormData] = useState({
      firstName: "",
      lastName: "",
      email: "",
      message: ""
    })
    const {firstName, lastName, email, message} = formData;

    const onFormSubmit = async e => {
        e.preventDefault();

        const body = {firstName, lastName, email, message};
        const res = await axios.post('https://29oddee2eh.execute-api.us-east-1.amazonaws.com/dev/contact', body);
        setFormData({
          firstName: "",
          lastName: "",
          email: "",
          message: ""
        })
        setFormSubmitted(true);

    }

    const onInputChange = e => {
        setFormData({...formData, [e.target.name]: e.target.value});
      }

    return (
        <section id="contact">
            <div className="container">

                <div id="contact-info">
                    <h3 className="fs-30p ff-rob co-primary">Contact Us</h3>
                    <h2 className="fs-60p ff-rob">Let's Get Started On Your Project</h2>
                    <p>Use this form to contact us with your project information. We'll get in touch as soon as possible</p>
                </div>

                <form onSubmit={(e) => onFormSubmit(e)} id="contact-form">
                    <div className="form-row">
                        <input type="text" name="firstName" id="first-name" placeholder="First Name" className="input" value={firstName} onChange={e => onInputChange(e)} />
                        <input type="text" name="lastName" id="last-name" placeholder="Last Name" className="input" value={lastName} onChange={e => onInputChange(e)} />
                    </div>
                    <input type="email" name="email" id="email" placeholder="Email Address" className="input input-double" value={email} onChange={e => onInputChange(e)} />
                    <textarea name="message" id="message" placeholder="Message" className="input input-textarea input-double" value={message} onChange={e => onInputChange(e)} ></textarea>
                    {formSubmitted && <p>Form Submitted!</p>}
                    <button type="submit" className="btn">Contact Us</button>
                </form>

            </div>
        </section>
    )
}

export default Contact
