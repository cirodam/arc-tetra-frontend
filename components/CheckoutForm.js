import React, { useState, useEffect } from "react";
import {CardElement, useStripe, useElements} from "@stripe/react-stripe-js";
import axios from 'axios'

export default function CheckoutForm({orderID, amount, onSuccess}) {

  const [clientSecret, setClientSecret] = useState('');

  const stripe = useStripe();
  const elements = useElements();

  useEffect(() => {

    const getIntent = async () => {
        const res = await axios.post(`https://api.arctetra.com/orders/${orderID}/intent`);
        setClientSecret(res.data.clientSecret);
    }

    getIntent();

  }, []);

  const cardStyle = {
    style: {
      base: {
        color: "#32325d",
        fontFamily: 'Poppins, sans-serif',
        fontSmoothing: "antialiased",
        fontSize: "16px",
        "::placeholder": {
          color: "#777"
        }
      },
      invalid: {
        color: "#fa755a",
        iconColor: "#fa755a"
      }
    }
  };

  const handleChange = async (event) => {

  };

  const handleSubmit = async e => {

    e.preventDefault();

    const res = await stripe.confirmCardPayment(clientSecret, {
      payment_method: {
        card: elements.getElement(CardElement)
      }
    });

    if (res.error) {

    } else {

      onSuccess(res);
    }

  };

  return (

    <form onSubmit={handleSubmit}>

      <CardElement id="card-element" options={cardStyle} onChange={handleChange} />
      <button id="submit" className="btn">Pay Now</button>

    </form>
  );
}