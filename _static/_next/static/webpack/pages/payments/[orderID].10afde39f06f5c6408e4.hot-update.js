webpackHotUpdate_N_E("pages/payments/[orderID]",{

/***/ "./components/CheckoutForm.js":
/*!************************************!*\
  !*** ./components/CheckoutForm.js ***!
  \************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return CheckoutForm; });
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "./node_modules/react/jsx-dev-runtime.js");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var C_Users_Tyler_Desktop_Projects_arc_tetra_software_v2_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/@babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var C_Users_Tyler_Desktop_Projects_arc_tetra_software_v2_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(C_Users_Tyler_Desktop_Projects_arc_tetra_software_v2_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var C_Users_Tyler_Desktop_Projects_arc_tetra_software_v2_node_modules_babel_runtime_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/esm/asyncToGenerator */ "./node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _stripe_react_stripe_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @stripe/react-stripe-js */ "./node_modules/@stripe/react-stripe-js/dist/react-stripe.umd.js");
/* harmony import */ var _stripe_react_stripe_js__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_stripe_react_stripe_js__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _stripe_stripe_js__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @stripe/stripe-js */ "./node_modules/@stripe/stripe-js/dist/stripe.esm.js");




var _jsxFileName = "C:\\Users\\Tyler\\Desktop\\Projects\\arc-tetra-software-v2\\components\\CheckoutForm.js",
    _s = $RefreshSig$();






var promise = Object(_stripe_stripe_js__WEBPACK_IMPORTED_MODULE_6__["loadStripe"])("pk_test_51IXU28HSmEVGHmHn6ls1H8IAYwPDeF7LqnY2W2hz9wNBjUNGdsoTOrhxeKOeqlOJGCnMyhtRzB1lRDGPuVoX8f4n00Gw4z1dtY");
function CheckoutForm(_ref) {
  _s();

  var orderID = _ref.orderID,
      amount = _ref.amount;

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_3__["useState"])(false),
      succeeded = _useState[0],
      setSucceeded = _useState[1];

  var _useState2 = Object(react__WEBPACK_IMPORTED_MODULE_3__["useState"])(null),
      error = _useState2[0],
      setError = _useState2[1];

  var _useState3 = Object(react__WEBPACK_IMPORTED_MODULE_3__["useState"])(''),
      processing = _useState3[0],
      setProcessing = _useState3[1];

  var _useState4 = Object(react__WEBPACK_IMPORTED_MODULE_3__["useState"])(false),
      disabled = _useState4[0],
      setDisabled = _useState4[1];

  var _useState5 = Object(react__WEBPACK_IMPORTED_MODULE_3__["useState"])(''),
      clientSecret = _useState5[0],
      setClientSecret = _useState5[1];

  var stripe = Object(_stripe_react_stripe_js__WEBPACK_IMPORTED_MODULE_4__["useStripe"])();
  var elements = Object(_stripe_react_stripe_js__WEBPACK_IMPORTED_MODULE_4__["useElements"])();
  Object(react__WEBPACK_IMPORTED_MODULE_3__["useEffect"])(function () {
    // Create PaymentIntent as soon as the page loads
    window.fetch("https://29oddee2eh.execute-api.us-east-1.amazonaws.com/dev/stripe", {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        amount: amount
      })
    }).then(function (res) {
      return res.json();
    }).then(function (data) {
      setClientSecret(data.clientSecret);
    });
  }, []);
  var cardStyle = {
    style: {
      base: {
        color: "#32325d",
        fontFamily: 'Poppins, sans-serif',
        fontSmoothing: "antialiased",
        fontSize: "16px",
        "::placeholder": {
          color: "#32325d"
        }
      },
      invalid: {
        color: "#fa755a",
        iconColor: "#fa755a"
      }
    }
  };

  var handleChange = /*#__PURE__*/function () {
    var _ref2 = Object(C_Users_Tyler_Desktop_Projects_arc_tetra_software_v2_node_modules_babel_runtime_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_2__["default"])( /*#__PURE__*/C_Users_Tyler_Desktop_Projects_arc_tetra_software_v2_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.mark(function _callee(event) {
      return C_Users_Tyler_Desktop_Projects_arc_tetra_software_v2_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              setDisabled(event.empty);
              setError(event.error ? event.error.message : "");

            case 2:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }));

    return function handleChange(_x) {
      return _ref2.apply(this, arguments);
    };
  }();

  var handleSubmit = /*#__PURE__*/function () {
    var _ref3 = Object(C_Users_Tyler_Desktop_Projects_arc_tetra_software_v2_node_modules_babel_runtime_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_2__["default"])( /*#__PURE__*/C_Users_Tyler_Desktop_Projects_arc_tetra_software_v2_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.mark(function _callee2(ev) {
      var payload, body;
      return C_Users_Tyler_Desktop_Projects_arc_tetra_software_v2_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.wrap(function _callee2$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
              ev.preventDefault();
              setProcessing(true);
              _context2.next = 4;
              return stripe.confirmCardPayment(clientSecret, {
                payment_method: {
                  card: elements.getElement(_stripe_react_stripe_js__WEBPACK_IMPORTED_MODULE_4__["CardElement"])
                }
              });

            case 4:
              payload = _context2.sent;

              if (!payload.error) {
                _context2.next = 10;
                break;
              }

              setError("Payment failed ".concat(payload.error.message));
              setProcessing(false);
              _context2.next = 16;
              break;

            case 10:
              setError(null);
              setProcessing(false);
              setSucceeded(true);
              body = {
                paymentResult: payload
              };
              _context2.next = 16;
              return axios__WEBPACK_IMPORTED_MODULE_5___default.a.put("https://29oddee2eh.execute-api.us-east-1.amazonaws.com/dev/orders/".concat(orderID), body);

            case 16:
            case "end":
              return _context2.stop();
          }
        }
      }, _callee2);
    }));

    return function handleSubmit(_x2) {
      return _ref3.apply(this, arguments);
    };
  }();

  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("section", {
    id: "stripe",
    children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("h2", {
      className: "fs-60p ff-rob ta-center",
      children: "Pay Now"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 90,
      columnNumber: 5
    }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_stripe_react_stripe_js__WEBPACK_IMPORTED_MODULE_4__["Elements"], {
      stripe: promise,
      children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("form", {
        id: "payment-form",
        onSubmit: handleSubmit,
        children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_stripe_react_stripe_js__WEBPACK_IMPORTED_MODULE_4__["CardElement"], {
          id: "card-element",
          options: cardStyle,
          onChange: handleChange
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 94,
          columnNumber: 13
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("button", {
          disabled: processing || disabled || succeeded,
          id: "submit",
          children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("span", {
            id: "button-text",
            children: processing ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
              className: "spinner",
              id: "spinner"
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 98,
              columnNumber: 13
            }, this) : "Pay now"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 96,
            columnNumber: 13
          }, this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 95,
          columnNumber: 13
        }, this), error && /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
          className: "card-error",
          role: "alert",
          children: error
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 107,
          columnNumber: 13
        }, this)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 92,
        columnNumber: 9
      }, this)
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 91,
      columnNumber: 5
    }, this)]
  }, void 0, true, {
    fileName: _jsxFileName,
    lineNumber: 89,
    columnNumber: 5
  }, this);
}

_s(CheckoutForm, "GhxqeXSXbsPzU6SqFIQHLcFON5I=", false, function () {
  return [_stripe_react_stripe_js__WEBPACK_IMPORTED_MODULE_4__["useStripe"], _stripe_react_stripe_js__WEBPACK_IMPORTED_MODULE_4__["useElements"]];
});

_c = CheckoutForm;

var _c;

$RefreshReg$(_c, "CheckoutForm");

;
    var _a, _b;
    // Legacy CSS implementations will `eval` browser code in a Node.js context
    // to extract CSS. For backwards compatibility, we need to check we're in a
    // browser context before continuing.
    if (typeof self !== 'undefined' &&
        // AMP / No-JS mode does not inject these helpers:
        '$RefreshHelpers$' in self) {
        var currentExports = module.__proto__.exports;
        var prevExports = (_b = (_a = module.hot.data) === null || _a === void 0 ? void 0 : _a.prevExports) !== null && _b !== void 0 ? _b : null;
        // This cannot happen in MainTemplate because the exports mismatch between
        // templating and execution.
        self.$RefreshHelpers$.registerExportsForReactRefresh(currentExports, module.i);
        // A module can be accepted automatically based on its exports, e.g. when
        // it is a Refresh Boundary.
        if (self.$RefreshHelpers$.isReactRefreshBoundary(currentExports)) {
            // Save the previous exports on update so we can compare the boundary
            // signatures.
            module.hot.dispose(function (data) {
                data.prevExports = currentExports;
            });
            // Unconditionally accept an update to this module, we'll check if it's
            // still a Refresh Boundary later.
            module.hot.accept();
            // This field is set when the previous version of this module was a
            // Refresh Boundary, letting us know we need to check for invalidation or
            // enqueue an update.
            if (prevExports !== null) {
                // A boundary can become ineligible if its exports are incompatible
                // with the previous exports.
                //
                // For example, if you add/remove/change exports, we'll want to
                // re-execute the importing modules, and force those components to
                // re-render. Similarly, if you convert a class component to a
                // function, we want to invalidate the boundary.
                if (self.$RefreshHelpers$.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)) {
                    module.hot.invalidate();
                }
                else {
                    self.$RefreshHelpers$.scheduleUpdate();
                }
            }
        }
        else {
            // Since we just executed the code for the module, it's possible that the
            // new exports made it ineligible for being a boundary.
            // We only care about the case when we were _previously_ a boundary,
            // because we already accepted this update (accidental side effect).
            var isNoLongerABoundary = prevExports !== null;
            if (isNoLongerABoundary) {
                module.hot.invalidate();
            }
        }
    }

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../node_modules/next/dist/compiled/webpack/harmony-module.js */ "./node_modules/next/dist/compiled/webpack/harmony-module.js")(module)))

/***/ }),

/***/ "./pages/payments/[orderID].js":
/*!*************************************!*\
  !*** ./pages/payments/[orderID].js ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "./node_modules/react/jsx-dev-runtime.js");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var C_Users_Tyler_Desktop_Projects_arc_tetra_software_v2_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/@babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var C_Users_Tyler_Desktop_Projects_arc_tetra_software_v2_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(C_Users_Tyler_Desktop_Projects_arc_tetra_software_v2_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var C_Users_Tyler_Desktop_Projects_arc_tetra_software_v2_node_modules_babel_runtime_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/esm/asyncToGenerator */ "./node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! next/router */ "./node_modules/next/router.js");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _stripe_react_stripe_js__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @stripe/react-stripe-js */ "./node_modules/@stripe/react-stripe-js/dist/react-stripe.umd.js");
/* harmony import */ var _stripe_react_stripe_js__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_stripe_react_stripe_js__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _components_CheckoutForm__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../components/CheckoutForm */ "./components/CheckoutForm.js");




var _jsxFileName = "C:\\Users\\Tyler\\Desktop\\Projects\\arc-tetra-software-v2\\pages\\payments\\[orderID].js",
    _this = undefined,
    _s = $RefreshSig$();







var PaymentSummary = function PaymentSummary() {
  _s();

  var router = Object(next_router__WEBPACK_IMPORTED_MODULE_4__["useRouter"])();
  var orderID = router.query.orderID;

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_3__["useState"])(0),
      paymentAmount = _useState[0],
      setPaymentAmount = _useState[1];

  Object(react__WEBPACK_IMPORTED_MODULE_3__["useEffect"])(function () {
    var getOrder = /*#__PURE__*/function () {
      var _ref = Object(C_Users_Tyler_Desktop_Projects_arc_tetra_software_v2_node_modules_babel_runtime_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_2__["default"])( /*#__PURE__*/C_Users_Tyler_Desktop_Projects_arc_tetra_software_v2_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.mark(function _callee() {
        var res;
        return C_Users_Tyler_Desktop_Projects_arc_tetra_software_v2_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.next = 2;
                return axios__WEBPACK_IMPORTED_MODULE_5___default.a.get("https://29oddee2eh.execute-api.us-east-1.amazonaws.com/dev/orders/".concat(orderID));

              case 2:
                res = _context.sent;
                setPaymentAmount(res.data.amount);

              case 4:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }));

      return function getOrder() {
        return _ref.apply(this, arguments);
      };
    }();

    getOrder();
  }, [orderID]);
  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
    children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("section", {
      id: "payment-summary",
      children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("h2", {
        className: "fs-60p ff-rob ta-center",
        children: "Order Details"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 30,
        columnNumber: 17
      }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("table", {
        id: "order-summary",
        children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("tbody", {
          children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("tr", {
            children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
              children: "OrderID: "
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 34,
              columnNumber: 29
            }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
              children: orderID
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 35,
              columnNumber: 29
            }, _this)]
          }, void 0, true, {
            fileName: _jsxFileName,
            lineNumber: 33,
            columnNumber: 25
          }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("tr", {
            children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
              children: "Payer: "
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 38,
              columnNumber: 29
            }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
              children: "John Doe"
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 39,
              columnNumber: 29
            }, _this)]
          }, void 0, true, {
            fileName: _jsxFileName,
            lineNumber: 37,
            columnNumber: 25
          }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("tr", {
            children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
              children: "Amount: "
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 42,
              columnNumber: 29
            }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
              children: paymentAmount
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 43,
              columnNumber: 29
            }, _this)]
          }, void 0, true, {
            fileName: _jsxFileName,
            lineNumber: 41,
            columnNumber: 25
          }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("tr", {
            children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
              children: "Description: "
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 46,
              columnNumber: 29
            }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
              children: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptates dicta architecto quas dignissimos et esse, impedit aliquam sint eveniet nam obcaecati ipsum, rem eaque? Ratione nesciunt repellendus maiores optio natus."
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 47,
              columnNumber: 29
            }, _this)]
          }, void 0, true, {
            fileName: _jsxFileName,
            lineNumber: 45,
            columnNumber: 25
          }, _this)]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 32,
          columnNumber: 21
        }, _this)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 31,
        columnNumber: 17
      }, _this)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 29,
      columnNumber: 13
    }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("section", {
      id: "stripe",
      children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("h2", {
        className: "fs-60p ff-rob ta-center",
        children: "Pay Now"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 53,
        columnNumber: 17
      }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_stripe_react_stripe_js__WEBPACK_IMPORTED_MODULE_6__["Elements"], {
        stripe: promise,
        children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_components_CheckoutForm__WEBPACK_IMPORTED_MODULE_7__["default"], {
          orderID: orderID,
          amount: 123
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 55,
          columnNumber: 21
        }, _this)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 54,
        columnNumber: 17
      }, _this)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 52,
      columnNumber: 13
    }, _this)]
  }, void 0, true, {
    fileName: _jsxFileName,
    lineNumber: 28,
    columnNumber: 9
  }, _this);
};

_s(PaymentSummary, "SkbJUi6Zx1J5UhCbJOzINsKkrI4=", false, function () {
  return [next_router__WEBPACK_IMPORTED_MODULE_4__["useRouter"]];
});

_c = PaymentSummary;
/* harmony default export */ __webpack_exports__["default"] = (PaymentSummary);

var _c;

$RefreshReg$(_c, "PaymentSummary");

;
    var _a, _b;
    // Legacy CSS implementations will `eval` browser code in a Node.js context
    // to extract CSS. For backwards compatibility, we need to check we're in a
    // browser context before continuing.
    if (typeof self !== 'undefined' &&
        // AMP / No-JS mode does not inject these helpers:
        '$RefreshHelpers$' in self) {
        var currentExports = module.__proto__.exports;
        var prevExports = (_b = (_a = module.hot.data) === null || _a === void 0 ? void 0 : _a.prevExports) !== null && _b !== void 0 ? _b : null;
        // This cannot happen in MainTemplate because the exports mismatch between
        // templating and execution.
        self.$RefreshHelpers$.registerExportsForReactRefresh(currentExports, module.i);
        // A module can be accepted automatically based on its exports, e.g. when
        // it is a Refresh Boundary.
        if (self.$RefreshHelpers$.isReactRefreshBoundary(currentExports)) {
            // Save the previous exports on update so we can compare the boundary
            // signatures.
            module.hot.dispose(function (data) {
                data.prevExports = currentExports;
            });
            // Unconditionally accept an update to this module, we'll check if it's
            // still a Refresh Boundary later.
            module.hot.accept();
            // This field is set when the previous version of this module was a
            // Refresh Boundary, letting us know we need to check for invalidation or
            // enqueue an update.
            if (prevExports !== null) {
                // A boundary can become ineligible if its exports are incompatible
                // with the previous exports.
                //
                // For example, if you add/remove/change exports, we'll want to
                // re-execute the importing modules, and force those components to
                // re-render. Similarly, if you convert a class component to a
                // function, we want to invalidate the boundary.
                if (self.$RefreshHelpers$.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)) {
                    module.hot.invalidate();
                }
                else {
                    self.$RefreshHelpers$.scheduleUpdate();
                }
            }
        }
        else {
            // Since we just executed the code for the module, it's possible that the
            // new exports made it ineligible for being a boundary.
            // We only care about the case when we were _previously_ a boundary,
            // because we already accepted this update (accidental side effect).
            var isNoLongerABoundary = prevExports !== null;
            if (isNoLongerABoundary) {
                module.hot.invalidate();
            }
        }
    }

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../node_modules/next/dist/compiled/webpack/harmony-module.js */ "./node_modules/next/dist/compiled/webpack/harmony-module.js")(module)))

/***/ })

})
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9fTl9FLy4vY29tcG9uZW50cy9DaGVja291dEZvcm0uanMiLCJ3ZWJwYWNrOi8vX05fRS8uL3BhZ2VzL3BheW1lbnRzL1tvcmRlcklEXS5qcyJdLCJuYW1lcyI6WyJwcm9taXNlIiwibG9hZFN0cmlwZSIsIkNoZWNrb3V0Rm9ybSIsIm9yZGVySUQiLCJhbW91bnQiLCJ1c2VTdGF0ZSIsInN1Y2NlZWRlZCIsInNldFN1Y2NlZWRlZCIsImVycm9yIiwic2V0RXJyb3IiLCJwcm9jZXNzaW5nIiwic2V0UHJvY2Vzc2luZyIsImRpc2FibGVkIiwic2V0RGlzYWJsZWQiLCJjbGllbnRTZWNyZXQiLCJzZXRDbGllbnRTZWNyZXQiLCJzdHJpcGUiLCJ1c2VTdHJpcGUiLCJlbGVtZW50cyIsInVzZUVsZW1lbnRzIiwidXNlRWZmZWN0Iiwid2luZG93IiwiZmV0Y2giLCJtZXRob2QiLCJoZWFkZXJzIiwiYm9keSIsIkpTT04iLCJzdHJpbmdpZnkiLCJ0aGVuIiwicmVzIiwianNvbiIsImRhdGEiLCJjYXJkU3R5bGUiLCJzdHlsZSIsImJhc2UiLCJjb2xvciIsImZvbnRGYW1pbHkiLCJmb250U21vb3RoaW5nIiwiZm9udFNpemUiLCJpbnZhbGlkIiwiaWNvbkNvbG9yIiwiaGFuZGxlQ2hhbmdlIiwiZXZlbnQiLCJlbXB0eSIsIm1lc3NhZ2UiLCJoYW5kbGVTdWJtaXQiLCJldiIsInByZXZlbnREZWZhdWx0IiwiY29uZmlybUNhcmRQYXltZW50IiwicGF5bWVudF9tZXRob2QiLCJjYXJkIiwiZ2V0RWxlbWVudCIsIkNhcmRFbGVtZW50IiwicGF5bG9hZCIsInBheW1lbnRSZXN1bHQiLCJheGlvcyIsInB1dCIsIlBheW1lbnRTdW1tYXJ5Iiwicm91dGVyIiwidXNlUm91dGVyIiwicXVlcnkiLCJwYXltZW50QW1vdW50Iiwic2V0UGF5bWVudEFtb3VudCIsImdldE9yZGVyIiwiZ2V0Il0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBLElBQU1BLE9BQU8sR0FBR0Msb0VBQVUsQ0FBQyw2R0FBRCxDQUExQjtBQUVlLFNBQVNDLFlBQVQsT0FBeUM7QUFBQTs7QUFBQSxNQUFsQkMsT0FBa0IsUUFBbEJBLE9BQWtCO0FBQUEsTUFBVEMsTUFBUyxRQUFUQSxNQUFTOztBQUFBLGtCQUVwQkMsc0RBQVEsQ0FBQyxLQUFELENBRlk7QUFBQSxNQUUvQ0MsU0FGK0M7QUFBQSxNQUVwQ0MsWUFGb0M7O0FBQUEsbUJBRzVCRixzREFBUSxDQUFDLElBQUQsQ0FIb0I7QUFBQSxNQUcvQ0csS0FIK0M7QUFBQSxNQUd4Q0MsUUFId0M7O0FBQUEsbUJBSWxCSixzREFBUSxDQUFDLEVBQUQsQ0FKVTtBQUFBLE1BSS9DSyxVQUorQztBQUFBLE1BSW5DQyxhQUptQzs7QUFBQSxtQkFLdEJOLHNEQUFRLENBQUMsS0FBRCxDQUxjO0FBQUEsTUFLL0NPLFFBTCtDO0FBQUEsTUFLckNDLFdBTHFDOztBQUFBLG1CQU1kUixzREFBUSxDQUFDLEVBQUQsQ0FOTTtBQUFBLE1BTS9DUyxZQU4rQztBQUFBLE1BTWpDQyxlQU5pQzs7QUFPdEQsTUFBTUMsTUFBTSxHQUFHQyx5RUFBUyxFQUF4QjtBQUNBLE1BQU1DLFFBQVEsR0FBR0MsMkVBQVcsRUFBNUI7QUFFQUMseURBQVMsQ0FBQyxZQUFNO0FBQ2Q7QUFDQUMsVUFBTSxDQUNIQyxLQURILENBQ1MsbUVBRFQsRUFDOEU7QUFDMUVDLFlBQU0sRUFBRSxNQURrRTtBQUUxRUMsYUFBTyxFQUFFO0FBQ1Asd0JBQWdCO0FBRFQsT0FGaUU7QUFLMUVDLFVBQUksRUFBRUMsSUFBSSxDQUFDQyxTQUFMLENBQWU7QUFBQ3ZCLGNBQU0sRUFBTkE7QUFBRCxPQUFmO0FBTG9FLEtBRDlFLEVBUUd3QixJQVJILENBUVEsVUFBQUMsR0FBRyxFQUFJO0FBQ1gsYUFBT0EsR0FBRyxDQUFDQyxJQUFKLEVBQVA7QUFDRCxLQVZILEVBV0dGLElBWEgsQ0FXUSxVQUFBRyxJQUFJLEVBQUk7QUFDWmhCLHFCQUFlLENBQUNnQixJQUFJLENBQUNqQixZQUFOLENBQWY7QUFDRCxLQWJIO0FBY0QsR0FoQlEsRUFnQk4sRUFoQk0sQ0FBVDtBQWtCQSxNQUFNa0IsU0FBUyxHQUFHO0FBQ2hCQyxTQUFLLEVBQUU7QUFDTEMsVUFBSSxFQUFFO0FBQ0pDLGFBQUssRUFBRSxTQURIO0FBRUpDLGtCQUFVLEVBQUUscUJBRlI7QUFHSkMscUJBQWEsRUFBRSxhQUhYO0FBSUpDLGdCQUFRLEVBQUUsTUFKTjtBQUtKLHlCQUFpQjtBQUNmSCxlQUFLLEVBQUU7QUFEUTtBQUxiLE9BREQ7QUFVTEksYUFBTyxFQUFFO0FBQ1BKLGFBQUssRUFBRSxTQURBO0FBRVBLLGlCQUFTLEVBQUU7QUFGSjtBQVZKO0FBRFMsR0FBbEI7O0FBa0JBLE1BQU1DLFlBQVk7QUFBQSxtVUFBRyxpQkFBT0MsS0FBUDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRW5CN0IseUJBQVcsQ0FBQzZCLEtBQUssQ0FBQ0MsS0FBUCxDQUFYO0FBQ0FsQyxzQkFBUSxDQUFDaUMsS0FBSyxDQUFDbEMsS0FBTixHQUFja0MsS0FBSyxDQUFDbEMsS0FBTixDQUFZb0MsT0FBMUIsR0FBb0MsRUFBckMsQ0FBUjs7QUFIbUI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsS0FBSDs7QUFBQSxvQkFBWkgsWUFBWTtBQUFBO0FBQUE7QUFBQSxLQUFsQjs7QUFPQSxNQUFNSSxZQUFZO0FBQUEsbVVBQUcsa0JBQU1DLEVBQU47QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRW5CQSxnQkFBRSxDQUFDQyxjQUFIO0FBQ0FwQywyQkFBYSxDQUFDLElBQUQsQ0FBYjtBQUhtQjtBQUFBLHFCQUtHSyxNQUFNLENBQUNnQyxrQkFBUCxDQUEwQmxDLFlBQTFCLEVBQXdDO0FBQzVEbUMsOEJBQWMsRUFBRTtBQUNkQyxzQkFBSSxFQUFFaEMsUUFBUSxDQUFDaUMsVUFBVCxDQUFvQkMsbUVBQXBCO0FBRFE7QUFENEMsZUFBeEMsQ0FMSDs7QUFBQTtBQUtiQyxxQkFMYTs7QUFBQSxtQkFXZkEsT0FBTyxDQUFDN0MsS0FYTztBQUFBO0FBQUE7QUFBQTs7QUFZakJDLHNCQUFRLDBCQUFtQjRDLE9BQU8sQ0FBQzdDLEtBQVIsQ0FBY29DLE9BQWpDLEVBQVI7QUFDQWpDLDJCQUFhLENBQUMsS0FBRCxDQUFiO0FBYmlCO0FBQUE7O0FBQUE7QUFlakJGLHNCQUFRLENBQUMsSUFBRCxDQUFSO0FBQ0FFLDJCQUFhLENBQUMsS0FBRCxDQUFiO0FBQ0FKLDBCQUFZLENBQUMsSUFBRCxDQUFaO0FBRU1rQixrQkFuQlcsR0FtQko7QUFBQzZCLDZCQUFhLEVBQUVEO0FBQWhCLGVBbkJJO0FBQUE7QUFBQSxxQkFvQlhFLDRDQUFLLENBQUNDLEdBQU4sNkVBQStFckQsT0FBL0UsR0FBMEZzQixJQUExRixDQXBCVzs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxLQUFIOztBQUFBLG9CQUFab0IsWUFBWTtBQUFBO0FBQUE7QUFBQSxLQUFsQjs7QUF5QkEsc0JBRUU7QUFBUyxNQUFFLEVBQUMsUUFBWjtBQUFBLDRCQUNBO0FBQUksZUFBUyxFQUFDLHlCQUFkO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLFlBREEsZUFFQSxxRUFBQyxnRUFBRDtBQUFVLFlBQU0sRUFBRTdDLE9BQWxCO0FBQUEsNkJBQ0k7QUFBTSxVQUFFLEVBQUMsY0FBVDtBQUF3QixnQkFBUSxFQUFFNkMsWUFBbEM7QUFBQSxnQ0FFSSxxRUFBQyxtRUFBRDtBQUFhLFlBQUUsRUFBQyxjQUFoQjtBQUErQixpQkFBTyxFQUFFYixTQUF4QztBQUFtRCxrQkFBUSxFQUFFUztBQUE3RDtBQUFBO0FBQUE7QUFBQTtBQUFBLGdCQUZKLGVBR0k7QUFBUSxrQkFBUSxFQUFFL0IsVUFBVSxJQUFJRSxRQUFkLElBQTBCTixTQUE1QztBQUF1RCxZQUFFLEVBQUMsUUFBMUQ7QUFBQSxpQ0FDQTtBQUFNLGNBQUUsRUFBQyxhQUFUO0FBQUEsc0JBQ0NJLFVBQVUsZ0JBQ1g7QUFBSyx1QkFBUyxFQUFDLFNBQWY7QUFBeUIsZ0JBQUUsRUFBQztBQUE1QjtBQUFBO0FBQUE7QUFBQTtBQUFBLG9CQURXLEdBR1g7QUFKQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQSxnQkFISixFQWNLRixLQUFLLGlCQUNOO0FBQUssbUJBQVMsRUFBQyxZQUFmO0FBQTRCLGNBQUksRUFBQyxPQUFqQztBQUFBLG9CQUNLQTtBQURMO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0JBZko7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREo7QUFBQTtBQUFBO0FBQUE7QUFBQSxZQUZBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxVQUZGO0FBNkJEOztHQTNHdUJOLFk7VUFPUGUsaUUsRUFDRUUsbUU7OztLQVJLakIsWTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDUnhCO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7O0FBRUEsSUFBTXVELGNBQWMsR0FBRyxTQUFqQkEsY0FBaUIsR0FBTTtBQUFBOztBQUV6QixNQUFNQyxNQUFNLEdBQUdDLDZEQUFTLEVBQXhCO0FBRnlCLE1BR2xCeEQsT0FIa0IsR0FHUHVELE1BQU0sQ0FBQ0UsS0FIQSxDQUdsQnpELE9BSGtCOztBQUFBLGtCQUtpQkUsc0RBQVEsQ0FBQyxDQUFELENBTHpCO0FBQUEsTUFLbEJ3RCxhQUxrQjtBQUFBLE1BS0hDLGdCQUxHOztBQU96QjFDLHlEQUFTLENBQUMsWUFBTTtBQUVaLFFBQU0yQyxRQUFRO0FBQUEsb1VBQUc7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSx1QkFFS1IsNENBQUssQ0FBQ1MsR0FBTiw2RUFBK0U3RCxPQUEvRSxFQUZMOztBQUFBO0FBRVAwQixtQkFGTztBQUdiaUMsZ0NBQWdCLENBQUNqQyxHQUFHLENBQUNFLElBQUosQ0FBUzNCLE1BQVYsQ0FBaEI7O0FBSGE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsT0FBSDs7QUFBQSxzQkFBUjJELFFBQVE7QUFBQTtBQUFBO0FBQUEsT0FBZDs7QUFNQUEsWUFBUTtBQUVYLEdBVlEsRUFVTixDQUFDNUQsT0FBRCxDQVZNLENBQVQ7QUFZQSxzQkFDSTtBQUFBLDRCQUNJO0FBQVMsUUFBRSxFQUFDLGlCQUFaO0FBQUEsOEJBQ0k7QUFBSSxpQkFBUyxFQUFDLHlCQUFkO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGVBREosZUFFSTtBQUFPLFVBQUUsRUFBQyxlQUFWO0FBQUEsK0JBQ0k7QUFBQSxrQ0FDSTtBQUFBLG9DQUNJO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHFCQURKLGVBRUk7QUFBQSx3QkFBS0E7QUFBTDtBQUFBO0FBQUE7QUFBQTtBQUFBLHFCQUZKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxtQkFESixlQUtJO0FBQUEsb0NBQ0k7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEscUJBREosZUFFSTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxxQkFGSjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsbUJBTEosZUFTSTtBQUFBLG9DQUNJO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHFCQURKLGVBRUk7QUFBQSx3QkFBSzBEO0FBQUw7QUFBQTtBQUFBO0FBQUE7QUFBQSxxQkFGSjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsbUJBVEosZUFhSTtBQUFBLG9DQUNJO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHFCQURKLGVBRUk7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEscUJBRko7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLG1CQWJKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURKO0FBQUE7QUFBQTtBQUFBO0FBQUEsZUFGSjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsYUFESixlQXdCSTtBQUFTLFFBQUUsRUFBQyxRQUFaO0FBQUEsOEJBQ0k7QUFBSSxpQkFBUyxFQUFDLHlCQUFkO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGVBREosZUFFSSxxRUFBQyxnRUFBRDtBQUFVLGNBQU0sRUFBRTdELE9BQWxCO0FBQUEsK0JBQ0kscUVBQUMsZ0VBQUQ7QUFBYyxpQkFBTyxFQUFFRyxPQUF2QjtBQUFnQyxnQkFBTSxFQUFFO0FBQXhDO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFESjtBQUFBO0FBQUE7QUFBQTtBQUFBLGVBRko7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGFBeEJKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxXQURKO0FBaUNILENBcEREOztHQUFNc0QsYztVQUVhRSxxRDs7O0tBRmJGLGM7QUFzRFNBLDZFQUFmIiwiZmlsZSI6InN0YXRpYy93ZWJwYWNrL3BhZ2VzL3BheW1lbnRzL1tvcmRlcklEXS4xMGFmZGUzOWYwNmY1YzY0MDhlNC5ob3QtdXBkYXRlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IFJlYWN0LCB7IHVzZVN0YXRlLCB1c2VFZmZlY3QgfSBmcm9tIFwicmVhY3RcIjtcclxuaW1wb3J0IHtDYXJkRWxlbWVudCwgdXNlU3RyaXBlLCB1c2VFbGVtZW50c30gZnJvbSBcIkBzdHJpcGUvcmVhY3Qtc3RyaXBlLWpzXCI7XHJcbmltcG9ydCBheGlvcyBmcm9tICdheGlvcyc7XHJcbmltcG9ydCB7RWxlbWVudHN9IGZyb20gJ0BzdHJpcGUvcmVhY3Qtc3RyaXBlLWpzJztcclxuaW1wb3J0IHtsb2FkU3RyaXBlfSBmcm9tICdAc3RyaXBlL3N0cmlwZS1qcyc7XHJcblxyXG5jb25zdCBwcm9taXNlID0gbG9hZFN0cmlwZShcInBrX3Rlc3RfNTFJWFUyOEhTbUVWR0htSG42bHMxSDhJQVl3UERlRjdMcW5ZMlcyaHo5d05CalVOR2Rzb1RPcmh4ZUtPZXFsT0pHQ25NeWh0UnpCMWxSREdQdVZvWDhmNG4wMEd3NHoxZHRZXCIpO1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgZnVuY3Rpb24gQ2hlY2tvdXRGb3JtKHtvcmRlcklELCBhbW91bnR9KSB7XHJcblxyXG4gIGNvbnN0IFtzdWNjZWVkZWQsIHNldFN1Y2NlZWRlZF0gPSB1c2VTdGF0ZShmYWxzZSk7XHJcbiAgY29uc3QgW2Vycm9yLCBzZXRFcnJvcl0gPSB1c2VTdGF0ZShudWxsKTtcclxuICBjb25zdCBbcHJvY2Vzc2luZywgc2V0UHJvY2Vzc2luZ10gPSB1c2VTdGF0ZSgnJyk7XHJcbiAgY29uc3QgW2Rpc2FibGVkLCBzZXREaXNhYmxlZF0gPSB1c2VTdGF0ZShmYWxzZSk7XHJcbiAgY29uc3QgW2NsaWVudFNlY3JldCwgc2V0Q2xpZW50U2VjcmV0XSA9IHVzZVN0YXRlKCcnKTtcclxuICBjb25zdCBzdHJpcGUgPSB1c2VTdHJpcGUoKTtcclxuICBjb25zdCBlbGVtZW50cyA9IHVzZUVsZW1lbnRzKCk7XHJcblxyXG4gIHVzZUVmZmVjdCgoKSA9PiB7XHJcbiAgICAvLyBDcmVhdGUgUGF5bWVudEludGVudCBhcyBzb29uIGFzIHRoZSBwYWdlIGxvYWRzXHJcbiAgICB3aW5kb3dcclxuICAgICAgLmZldGNoKFwiaHR0cHM6Ly8yOW9kZGVlMmVoLmV4ZWN1dGUtYXBpLnVzLWVhc3QtMS5hbWF6b25hd3MuY29tL2Rldi9zdHJpcGVcIiwge1xyXG4gICAgICAgIG1ldGhvZDogXCJQT1NUXCIsXHJcbiAgICAgICAgaGVhZGVyczoge1xyXG4gICAgICAgICAgXCJDb250ZW50LVR5cGVcIjogXCJhcHBsaWNhdGlvbi9qc29uXCJcclxuICAgICAgICB9LFxyXG4gICAgICAgIGJvZHk6IEpTT04uc3RyaW5naWZ5KHthbW91bnR9KVxyXG4gICAgICB9KVxyXG4gICAgICAudGhlbihyZXMgPT4ge1xyXG4gICAgICAgIHJldHVybiByZXMuanNvbigpO1xyXG4gICAgICB9KVxyXG4gICAgICAudGhlbihkYXRhID0+IHtcclxuICAgICAgICBzZXRDbGllbnRTZWNyZXQoZGF0YS5jbGllbnRTZWNyZXQpO1xyXG4gICAgICB9KTtcclxuICB9LCBbXSk7XHJcblxyXG4gIGNvbnN0IGNhcmRTdHlsZSA9IHtcclxuICAgIHN0eWxlOiB7XHJcbiAgICAgIGJhc2U6IHtcclxuICAgICAgICBjb2xvcjogXCIjMzIzMjVkXCIsXHJcbiAgICAgICAgZm9udEZhbWlseTogJ1BvcHBpbnMsIHNhbnMtc2VyaWYnLFxyXG4gICAgICAgIGZvbnRTbW9vdGhpbmc6IFwiYW50aWFsaWFzZWRcIixcclxuICAgICAgICBmb250U2l6ZTogXCIxNnB4XCIsXHJcbiAgICAgICAgXCI6OnBsYWNlaG9sZGVyXCI6IHtcclxuICAgICAgICAgIGNvbG9yOiBcIiMzMjMyNWRcIlxyXG4gICAgICAgIH1cclxuICAgICAgfSxcclxuICAgICAgaW52YWxpZDoge1xyXG4gICAgICAgIGNvbG9yOiBcIiNmYTc1NWFcIixcclxuICAgICAgICBpY29uQ29sb3I6IFwiI2ZhNzU1YVwiXHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9O1xyXG5cclxuICBjb25zdCBoYW5kbGVDaGFuZ2UgPSBhc3luYyAoZXZlbnQpID0+IHtcclxuXHJcbiAgICBzZXREaXNhYmxlZChldmVudC5lbXB0eSk7XHJcbiAgICBzZXRFcnJvcihldmVudC5lcnJvciA/IGV2ZW50LmVycm9yLm1lc3NhZ2UgOiBcIlwiKTtcclxuXHJcbiAgfTtcclxuXHJcbiAgY29uc3QgaGFuZGxlU3VibWl0ID0gYXN5bmMgZXYgPT4ge1xyXG5cclxuICAgIGV2LnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICBzZXRQcm9jZXNzaW5nKHRydWUpO1xyXG5cclxuICAgIGNvbnN0IHBheWxvYWQgPSBhd2FpdCBzdHJpcGUuY29uZmlybUNhcmRQYXltZW50KGNsaWVudFNlY3JldCwge1xyXG4gICAgICBwYXltZW50X21ldGhvZDoge1xyXG4gICAgICAgIGNhcmQ6IGVsZW1lbnRzLmdldEVsZW1lbnQoQ2FyZEVsZW1lbnQpXHJcbiAgICAgIH1cclxuICAgIH0pO1xyXG5cclxuICAgIGlmIChwYXlsb2FkLmVycm9yKSB7XHJcbiAgICAgIHNldEVycm9yKGBQYXltZW50IGZhaWxlZCAke3BheWxvYWQuZXJyb3IubWVzc2FnZX1gKTtcclxuICAgICAgc2V0UHJvY2Vzc2luZyhmYWxzZSk7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICBzZXRFcnJvcihudWxsKTtcclxuICAgICAgc2V0UHJvY2Vzc2luZyhmYWxzZSk7XHJcbiAgICAgIHNldFN1Y2NlZWRlZCh0cnVlKTtcclxuXHJcbiAgICAgIGNvbnN0IGJvZHkgPSB7cGF5bWVudFJlc3VsdDogcGF5bG9hZH07XHJcbiAgICAgIGF3YWl0IGF4aW9zLnB1dChgaHR0cHM6Ly8yOW9kZGVlMmVoLmV4ZWN1dGUtYXBpLnVzLWVhc3QtMS5hbWF6b25hd3MuY29tL2Rldi9vcmRlcnMvJHtvcmRlcklEfWAsIGJvZHkpO1xyXG4gICAgfVxyXG5cclxuICB9O1xyXG5cclxuICByZXR1cm4gKFxyXG5cclxuICAgIDxzZWN0aW9uIGlkPVwic3RyaXBlXCI+XHJcbiAgICA8aDIgY2xhc3NOYW1lPVwiZnMtNjBwIGZmLXJvYiB0YS1jZW50ZXJcIj5QYXkgTm93PC9oMj5cclxuICAgIDxFbGVtZW50cyBzdHJpcGU9e3Byb21pc2V9PlxyXG4gICAgICAgIDxmb3JtIGlkPVwicGF5bWVudC1mb3JtXCIgb25TdWJtaXQ9e2hhbmRsZVN1Ym1pdH0+XHJcblxyXG4gICAgICAgICAgICA8Q2FyZEVsZW1lbnQgaWQ9XCJjYXJkLWVsZW1lbnRcIiBvcHRpb25zPXtjYXJkU3R5bGV9IG9uQ2hhbmdlPXtoYW5kbGVDaGFuZ2V9IC8+XHJcbiAgICAgICAgICAgIDxidXR0b24gZGlzYWJsZWQ9e3Byb2Nlc3NpbmcgfHwgZGlzYWJsZWQgfHwgc3VjY2VlZGVkfSBpZD1cInN1Ym1pdFwiPlxyXG4gICAgICAgICAgICA8c3BhbiBpZD1cImJ1dHRvbi10ZXh0XCI+XHJcbiAgICAgICAgICAgIHtwcm9jZXNzaW5nID8gKFxyXG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInNwaW5uZXJcIiBpZD1cInNwaW5uZXJcIj48L2Rpdj5cclxuICAgICAgICAgICAgKSA6IChcclxuICAgICAgICAgICAgXCJQYXkgbm93XCJcclxuICAgICAgICAgICAgKX1cclxuICAgICAgICAgICAgPC9zcGFuPlxyXG4gICAgICAgICAgICA8L2J1dHRvbj5cclxuXHJcbiAgICAgICAgICAgIHsvKiBTaG93IGFueSBlcnJvciB0aGF0IGhhcHBlbnMgd2hlbiBwcm9jZXNzaW5nIHRoZSBwYXltZW50ICovfVxyXG4gICAgICAgICAgICB7ZXJyb3IgJiYgKFxyXG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImNhcmQtZXJyb3JcIiByb2xlPVwiYWxlcnRcIj5cclxuICAgICAgICAgICAgICAgIHtlcnJvcn1cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICl9XHJcblxyXG4gICAgICAgIDwvZm9ybT5cclxuICAgIDwvRWxlbWVudHM+XHJcbiAgICA8L3NlY3Rpb24+XHJcbiAgKTtcclxufSIsImltcG9ydCBSZWFjdCwge3VzZUVmZmVjdCwgdXNlU3RhdGV9IGZyb20gJ3JlYWN0JztcclxuaW1wb3J0IHt1c2VSb3V0ZXJ9IGZyb20gJ25leHQvcm91dGVyJztcclxuaW1wb3J0IGF4aW9zIGZyb20gJ2F4aW9zJztcclxuXHJcbmltcG9ydCB7RWxlbWVudHN9IGZyb20gJ0BzdHJpcGUvcmVhY3Qtc3RyaXBlLWpzJztcclxuaW1wb3J0IENoZWNrb3V0Rm9ybSBmcm9tICcuLi8uLi9jb21wb25lbnRzL0NoZWNrb3V0Rm9ybSc7XHJcblxyXG5jb25zdCBQYXltZW50U3VtbWFyeSA9ICgpID0+IHtcclxuXHJcbiAgICBjb25zdCByb3V0ZXIgPSB1c2VSb3V0ZXIoKTtcclxuICAgIGNvbnN0IHtvcmRlcklEfSA9IHJvdXRlci5xdWVyeTtcclxuXHJcbiAgICBjb25zdCBbcGF5bWVudEFtb3VudCwgc2V0UGF5bWVudEFtb3VudF0gPSB1c2VTdGF0ZSgwKTtcclxuXHJcbiAgICB1c2VFZmZlY3QoKCkgPT4ge1xyXG5cclxuICAgICAgICBjb25zdCBnZXRPcmRlciA9IGFzeW5jICgpID0+IHtcclxuXHJcbiAgICAgICAgICAgIGNvbnN0IHJlcyA9IGF3YWl0IGF4aW9zLmdldChgaHR0cHM6Ly8yOW9kZGVlMmVoLmV4ZWN1dGUtYXBpLnVzLWVhc3QtMS5hbWF6b25hd3MuY29tL2Rldi9vcmRlcnMvJHtvcmRlcklEfWApO1xyXG4gICAgICAgICAgICBzZXRQYXltZW50QW1vdW50KHJlcy5kYXRhLmFtb3VudCk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBnZXRPcmRlcigpO1xyXG5cclxuICAgIH0sIFtvcmRlcklEXSlcclxuXHJcbiAgICByZXR1cm4gKFxyXG4gICAgICAgIDxkaXY+XHJcbiAgICAgICAgICAgIDxzZWN0aW9uIGlkPVwicGF5bWVudC1zdW1tYXJ5XCI+XHJcbiAgICAgICAgICAgICAgICA8aDIgY2xhc3NOYW1lPVwiZnMtNjBwIGZmLXJvYiB0YS1jZW50ZXJcIj5PcmRlciBEZXRhaWxzPC9oMj5cclxuICAgICAgICAgICAgICAgIDx0YWJsZSBpZD1cIm9yZGVyLXN1bW1hcnlcIj5cclxuICAgICAgICAgICAgICAgICAgICA8dGJvZHk+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDx0cj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDx0ZD5PcmRlcklEOiA8L3RkPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPHRkPntvcmRlcklEfTwvdGQ+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvdHI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDx0cj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDx0ZD5QYXllcjogPC90ZD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDx0ZD5Kb2huIERvZTwvdGQ+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvdHI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDx0cj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDx0ZD5BbW91bnQ6IDwvdGQ+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8dGQ+e3BheW1lbnRBbW91bnR9PC90ZD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPC90cj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPHRyPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPHRkPkRlc2NyaXB0aW9uOiA8L3RkPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPHRkPkxvcmVtIGlwc3VtIGRvbG9yIHNpdCBhbWV0IGNvbnNlY3RldHVyIGFkaXBpc2ljaW5nIGVsaXQuIFZvbHVwdGF0ZXMgZGljdGEgYXJjaGl0ZWN0byBxdWFzIGRpZ25pc3NpbW9zIGV0IGVzc2UsIGltcGVkaXQgYWxpcXVhbSBzaW50IGV2ZW5pZXQgbmFtIG9iY2FlY2F0aSBpcHN1bSwgcmVtIGVhcXVlPyBSYXRpb25lIG5lc2NpdW50IHJlcGVsbGVuZHVzIG1haW9yZXMgb3B0aW8gbmF0dXMuPC90ZD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPC90cj5cclxuICAgICAgICAgICAgICAgICAgICA8L3Rib2R5PlxyXG4gICAgICAgICAgICAgICAgPC90YWJsZT5cclxuICAgICAgICAgICAgPC9zZWN0aW9uPlxyXG4gICAgICAgICAgICA8c2VjdGlvbiBpZD1cInN0cmlwZVwiPlxyXG4gICAgICAgICAgICAgICAgPGgyIGNsYXNzTmFtZT1cImZzLTYwcCBmZi1yb2IgdGEtY2VudGVyXCI+UGF5IE5vdzwvaDI+XHJcbiAgICAgICAgICAgICAgICA8RWxlbWVudHMgc3RyaXBlPXtwcm9taXNlfT5cclxuICAgICAgICAgICAgICAgICAgICA8Q2hlY2tvdXRGb3JtIG9yZGVySUQ9e29yZGVySUR9IGFtb3VudD17MTIzfS8+XHJcbiAgICAgICAgICAgICAgICA8L0VsZW1lbnRzPlxyXG4gICAgICAgICAgICA8L3NlY3Rpb24+XHJcbiAgICAgICAgPC9kaXY+XHJcbiAgICApXHJcbn1cclxuXHJcbmV4cG9ydCBkZWZhdWx0IFBheW1lbnRTdW1tYXJ5O1xyXG4iXSwic291cmNlUm9vdCI6IiJ9