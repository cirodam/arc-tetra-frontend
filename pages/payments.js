import React, {useState} from 'react'
import Link from 'next/Link';
import Head from 'next/head';

export const getStaticProps = (context) => {
    return {
      props: {}
    }
  } 

const payments = () => {

    const [orderID, setOrderID] = useState("");
    const [accessCode, setAccessCode] = useState("");

    return (
        <>
            <Head>
                <title>Arc Tetra Software | Payments</title>
                <meta name="description" content="Pay an Invoice"/>
            </Head>

            <section id="find-payment-section">
                <h2 className="fs-60p ff-rob ta-center">Make A Payment</h2>
                <form id="find-payment-form">
                    <input type="text" name="orderID" id="orderID" placeholder="Order ID" className="input span-2" value={orderID} onChange={e => setOrderID(e.target.value)}/>
                    <input type="text" name="accessCode" id="accessCode" placeholder="Access Code" className="input span-2" value={accessCode} onChange={e => setAccessCode(e.target.value)} />
                </form>
                <Link href={`/payments/${orderID}?accessCode=${accessCode}`}><a className="btn">Next</a></Link>
            </section>
        </>
    )
}

export default payments
