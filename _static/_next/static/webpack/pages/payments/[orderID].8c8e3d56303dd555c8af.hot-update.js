webpackHotUpdate_N_E("pages/payments/[orderID]",{

/***/ "./components/CheckoutForm.js":
/*!************************************!*\
  !*** ./components/CheckoutForm.js ***!
  \************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return CheckoutForm; });
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "./node_modules/react/jsx-dev-runtime.js");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var C_Users_Tyler_Desktop_Projects_arc_tetra_software_v2_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/@babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var C_Users_Tyler_Desktop_Projects_arc_tetra_software_v2_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(C_Users_Tyler_Desktop_Projects_arc_tetra_software_v2_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var C_Users_Tyler_Desktop_Projects_arc_tetra_software_v2_node_modules_babel_runtime_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/esm/asyncToGenerator */ "./node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _stripe_react_stripe_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @stripe/react-stripe-js */ "./node_modules/@stripe/react-stripe-js/dist/react-stripe.umd.js");
/* harmony import */ var _stripe_react_stripe_js__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_stripe_react_stripe_js__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_5__);




var _jsxFileName = "C:\\Users\\Tyler\\Desktop\\Projects\\arc-tetra-software-v2\\components\\CheckoutForm.js",
    _s = $RefreshSig$();





function CheckoutForm(_ref) {
  _s();

  var orderID = _ref.orderID,
      amount = _ref.amount;

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_3__["useState"])(false),
      succeeded = _useState[0],
      setSucceeded = _useState[1];

  var _useState2 = Object(react__WEBPACK_IMPORTED_MODULE_3__["useState"])(null),
      error = _useState2[0],
      setError = _useState2[1];

  var _useState3 = Object(react__WEBPACK_IMPORTED_MODULE_3__["useState"])(''),
      processing = _useState3[0],
      setProcessing = _useState3[1];

  var _useState4 = Object(react__WEBPACK_IMPORTED_MODULE_3__["useState"])(false),
      disabled = _useState4[0],
      setDisabled = _useState4[1];

  var _useState5 = Object(react__WEBPACK_IMPORTED_MODULE_3__["useState"])(''),
      clientSecret = _useState5[0],
      setClientSecret = _useState5[1];

  var stripe = Object(_stripe_react_stripe_js__WEBPACK_IMPORTED_MODULE_4__["useStripe"])();
  var elements = Object(_stripe_react_stripe_js__WEBPACK_IMPORTED_MODULE_4__["useElements"])();
  Object(react__WEBPACK_IMPORTED_MODULE_3__["useEffect"])(function () {
    // Create PaymentIntent as soon as the page loads
    window.fetch("https://29oddee2eh.execute-api.us-east-1.amazonaws.com/dev/stripe", {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        amount: amount
      })
    }).then(function (res) {
      return res.json();
    }).then(function (data) {
      setClientSecret(data.clientSecret);
    });
  }, []);
  var cardStyle = {
    style: {
      base: {
        color: "#32325d",
        fontFamily: 'Poppins, sans-serif',
        fontSmoothing: "antialiased",
        fontSize: "16px",
        "::placeholder": {
          color: "#32325d"
        }
      },
      invalid: {
        color: "#fa755a",
        iconColor: "#fa755a"
      }
    }
  };

  var handleChange = /*#__PURE__*/function () {
    var _ref2 = Object(C_Users_Tyler_Desktop_Projects_arc_tetra_software_v2_node_modules_babel_runtime_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_2__["default"])( /*#__PURE__*/C_Users_Tyler_Desktop_Projects_arc_tetra_software_v2_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.mark(function _callee(event) {
      return C_Users_Tyler_Desktop_Projects_arc_tetra_software_v2_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              setDisabled(event.empty);
              setError(event.error ? event.error.message : "");

            case 2:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }));

    return function handleChange(_x) {
      return _ref2.apply(this, arguments);
    };
  }();

  var handleSubmit = /*#__PURE__*/function () {
    var _ref3 = Object(C_Users_Tyler_Desktop_Projects_arc_tetra_software_v2_node_modules_babel_runtime_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_2__["default"])( /*#__PURE__*/C_Users_Tyler_Desktop_Projects_arc_tetra_software_v2_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.mark(function _callee2(ev) {
      var payload, body;
      return C_Users_Tyler_Desktop_Projects_arc_tetra_software_v2_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.wrap(function _callee2$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
              ev.preventDefault();
              setProcessing(true);
              _context2.next = 4;
              return stripe.confirmCardPayment(clientSecret, {
                payment_method: {
                  card: elements.getElement(_stripe_react_stripe_js__WEBPACK_IMPORTED_MODULE_4__["CardElement"])
                }
              });

            case 4:
              payload = _context2.sent;

              if (!payload.error) {
                _context2.next = 10;
                break;
              }

              setError("Payment failed ".concat(payload.error.message));
              setProcessing(false);
              _context2.next = 16;
              break;

            case 10:
              setError(null);
              setProcessing(false);
              setSucceeded(true);
              body = {
                paymentResult: payload
              };
              _context2.next = 16;
              return axios__WEBPACK_IMPORTED_MODULE_5___default.a.put("https://29oddee2eh.execute-api.us-east-1.amazonaws.com/dev/orders/".concat(orderID), body);

            case 16:
            case "end":
              return _context2.stop();
          }
        }
      }, _callee2);
    }));

    return function handleSubmit(_x2) {
      return _ref3.apply(this, arguments);
    };
  }();

  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("section", {
    id: "stripe",
    children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("h2", {
      className: "fs-60p ff-rob ta-center",
      children: "Pay Now"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 87,
      columnNumber: 5
    }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_stripe_react_stripe_js__WEBPACK_IMPORTED_MODULE_4__["Elements"], {
      stripe: promise,
      children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("form", {
        id: "payment-form",
        onSubmit: handleSubmit,
        children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_stripe_react_stripe_js__WEBPACK_IMPORTED_MODULE_4__["CardElement"], {
          id: "card-element",
          options: cardStyle,
          onChange: handleChange
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 91,
          columnNumber: 13
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("button", {
          disabled: processing || disabled || succeeded,
          id: "submit",
          children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("span", {
            id: "button-text",
            children: processing ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
              className: "spinner",
              id: "spinner"
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 95,
              columnNumber: 13
            }, this) : "Pay now"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 93,
            columnNumber: 13
          }, this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 92,
          columnNumber: 13
        }, this), error && /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
          className: "card-error",
          role: "alert",
          children: error
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 104,
          columnNumber: 13
        }, this)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 89,
        columnNumber: 9
      }, this)
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 88,
      columnNumber: 5
    }, this)]
  }, void 0, true, {
    fileName: _jsxFileName,
    lineNumber: 86,
    columnNumber: 5
  }, this);
}

_s(CheckoutForm, "GhxqeXSXbsPzU6SqFIQHLcFON5I=", false, function () {
  return [_stripe_react_stripe_js__WEBPACK_IMPORTED_MODULE_4__["useStripe"], _stripe_react_stripe_js__WEBPACK_IMPORTED_MODULE_4__["useElements"]];
});

_c = CheckoutForm;

var _c;

$RefreshReg$(_c, "CheckoutForm");

;
    var _a, _b;
    // Legacy CSS implementations will `eval` browser code in a Node.js context
    // to extract CSS. For backwards compatibility, we need to check we're in a
    // browser context before continuing.
    if (typeof self !== 'undefined' &&
        // AMP / No-JS mode does not inject these helpers:
        '$RefreshHelpers$' in self) {
        var currentExports = module.__proto__.exports;
        var prevExports = (_b = (_a = module.hot.data) === null || _a === void 0 ? void 0 : _a.prevExports) !== null && _b !== void 0 ? _b : null;
        // This cannot happen in MainTemplate because the exports mismatch between
        // templating and execution.
        self.$RefreshHelpers$.registerExportsForReactRefresh(currentExports, module.i);
        // A module can be accepted automatically based on its exports, e.g. when
        // it is a Refresh Boundary.
        if (self.$RefreshHelpers$.isReactRefreshBoundary(currentExports)) {
            // Save the previous exports on update so we can compare the boundary
            // signatures.
            module.hot.dispose(function (data) {
                data.prevExports = currentExports;
            });
            // Unconditionally accept an update to this module, we'll check if it's
            // still a Refresh Boundary later.
            module.hot.accept();
            // This field is set when the previous version of this module was a
            // Refresh Boundary, letting us know we need to check for invalidation or
            // enqueue an update.
            if (prevExports !== null) {
                // A boundary can become ineligible if its exports are incompatible
                // with the previous exports.
                //
                // For example, if you add/remove/change exports, we'll want to
                // re-execute the importing modules, and force those components to
                // re-render. Similarly, if you convert a class component to a
                // function, we want to invalidate the boundary.
                if (self.$RefreshHelpers$.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)) {
                    module.hot.invalidate();
                }
                else {
                    self.$RefreshHelpers$.scheduleUpdate();
                }
            }
        }
        else {
            // Since we just executed the code for the module, it's possible that the
            // new exports made it ineligible for being a boundary.
            // We only care about the case when we were _previously_ a boundary,
            // because we already accepted this update (accidental side effect).
            var isNoLongerABoundary = prevExports !== null;
            if (isNoLongerABoundary) {
                module.hot.invalidate();
            }
        }
    }

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../node_modules/next/dist/compiled/webpack/harmony-module.js */ "./node_modules/next/dist/compiled/webpack/harmony-module.js")(module)))

/***/ })

})
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9fTl9FLy4vY29tcG9uZW50cy9DaGVja291dEZvcm0uanMiXSwibmFtZXMiOlsiQ2hlY2tvdXRGb3JtIiwib3JkZXJJRCIsImFtb3VudCIsInVzZVN0YXRlIiwic3VjY2VlZGVkIiwic2V0U3VjY2VlZGVkIiwiZXJyb3IiLCJzZXRFcnJvciIsInByb2Nlc3NpbmciLCJzZXRQcm9jZXNzaW5nIiwiZGlzYWJsZWQiLCJzZXREaXNhYmxlZCIsImNsaWVudFNlY3JldCIsInNldENsaWVudFNlY3JldCIsInN0cmlwZSIsInVzZVN0cmlwZSIsImVsZW1lbnRzIiwidXNlRWxlbWVudHMiLCJ1c2VFZmZlY3QiLCJ3aW5kb3ciLCJmZXRjaCIsIm1ldGhvZCIsImhlYWRlcnMiLCJib2R5IiwiSlNPTiIsInN0cmluZ2lmeSIsInRoZW4iLCJyZXMiLCJqc29uIiwiZGF0YSIsImNhcmRTdHlsZSIsInN0eWxlIiwiYmFzZSIsImNvbG9yIiwiZm9udEZhbWlseSIsImZvbnRTbW9vdGhpbmciLCJmb250U2l6ZSIsImludmFsaWQiLCJpY29uQ29sb3IiLCJoYW5kbGVDaGFuZ2UiLCJldmVudCIsImVtcHR5IiwibWVzc2FnZSIsImhhbmRsZVN1Ym1pdCIsImV2IiwicHJldmVudERlZmF1bHQiLCJjb25maXJtQ2FyZFBheW1lbnQiLCJwYXltZW50X21ldGhvZCIsImNhcmQiLCJnZXRFbGVtZW50IiwiQ2FyZEVsZW1lbnQiLCJwYXlsb2FkIiwicGF5bWVudFJlc3VsdCIsImF4aW9zIiwicHV0IiwicHJvbWlzZSJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFFZSxTQUFTQSxZQUFULE9BQXlDO0FBQUE7O0FBQUEsTUFBbEJDLE9BQWtCLFFBQWxCQSxPQUFrQjtBQUFBLE1BQVRDLE1BQVMsUUFBVEEsTUFBUzs7QUFBQSxrQkFFcEJDLHNEQUFRLENBQUMsS0FBRCxDQUZZO0FBQUEsTUFFL0NDLFNBRitDO0FBQUEsTUFFcENDLFlBRm9DOztBQUFBLG1CQUc1QkYsc0RBQVEsQ0FBQyxJQUFELENBSG9CO0FBQUEsTUFHL0NHLEtBSCtDO0FBQUEsTUFHeENDLFFBSHdDOztBQUFBLG1CQUlsQkosc0RBQVEsQ0FBQyxFQUFELENBSlU7QUFBQSxNQUkvQ0ssVUFKK0M7QUFBQSxNQUluQ0MsYUFKbUM7O0FBQUEsbUJBS3RCTixzREFBUSxDQUFDLEtBQUQsQ0FMYztBQUFBLE1BSy9DTyxRQUwrQztBQUFBLE1BS3JDQyxXQUxxQzs7QUFBQSxtQkFNZFIsc0RBQVEsQ0FBQyxFQUFELENBTk07QUFBQSxNQU0vQ1MsWUFOK0M7QUFBQSxNQU1qQ0MsZUFOaUM7O0FBT3RELE1BQU1DLE1BQU0sR0FBR0MseUVBQVMsRUFBeEI7QUFDQSxNQUFNQyxRQUFRLEdBQUdDLDJFQUFXLEVBQTVCO0FBRUFDLHlEQUFTLENBQUMsWUFBTTtBQUNkO0FBQ0FDLFVBQU0sQ0FDSEMsS0FESCxDQUNTLG1FQURULEVBQzhFO0FBQzFFQyxZQUFNLEVBQUUsTUFEa0U7QUFFMUVDLGFBQU8sRUFBRTtBQUNQLHdCQUFnQjtBQURULE9BRmlFO0FBSzFFQyxVQUFJLEVBQUVDLElBQUksQ0FBQ0MsU0FBTCxDQUFlO0FBQUN2QixjQUFNLEVBQU5BO0FBQUQsT0FBZjtBQUxvRSxLQUQ5RSxFQVFHd0IsSUFSSCxDQVFRLFVBQUFDLEdBQUcsRUFBSTtBQUNYLGFBQU9BLEdBQUcsQ0FBQ0MsSUFBSixFQUFQO0FBQ0QsS0FWSCxFQVdHRixJQVhILENBV1EsVUFBQUcsSUFBSSxFQUFJO0FBQ1poQixxQkFBZSxDQUFDZ0IsSUFBSSxDQUFDakIsWUFBTixDQUFmO0FBQ0QsS0FiSDtBQWNELEdBaEJRLEVBZ0JOLEVBaEJNLENBQVQ7QUFrQkEsTUFBTWtCLFNBQVMsR0FBRztBQUNoQkMsU0FBSyxFQUFFO0FBQ0xDLFVBQUksRUFBRTtBQUNKQyxhQUFLLEVBQUUsU0FESDtBQUVKQyxrQkFBVSxFQUFFLHFCQUZSO0FBR0pDLHFCQUFhLEVBQUUsYUFIWDtBQUlKQyxnQkFBUSxFQUFFLE1BSk47QUFLSix5QkFBaUI7QUFDZkgsZUFBSyxFQUFFO0FBRFE7QUFMYixPQUREO0FBVUxJLGFBQU8sRUFBRTtBQUNQSixhQUFLLEVBQUUsU0FEQTtBQUVQSyxpQkFBUyxFQUFFO0FBRko7QUFWSjtBQURTLEdBQWxCOztBQWtCQSxNQUFNQyxZQUFZO0FBQUEsbVVBQUcsaUJBQU9DLEtBQVA7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVuQjdCLHlCQUFXLENBQUM2QixLQUFLLENBQUNDLEtBQVAsQ0FBWDtBQUNBbEMsc0JBQVEsQ0FBQ2lDLEtBQUssQ0FBQ2xDLEtBQU4sR0FBY2tDLEtBQUssQ0FBQ2xDLEtBQU4sQ0FBWW9DLE9BQTFCLEdBQW9DLEVBQXJDLENBQVI7O0FBSG1CO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLEtBQUg7O0FBQUEsb0JBQVpILFlBQVk7QUFBQTtBQUFBO0FBQUEsS0FBbEI7O0FBT0EsTUFBTUksWUFBWTtBQUFBLG1VQUFHLGtCQUFNQyxFQUFOO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVuQkEsZ0JBQUUsQ0FBQ0MsY0FBSDtBQUNBcEMsMkJBQWEsQ0FBQyxJQUFELENBQWI7QUFIbUI7QUFBQSxxQkFLR0ssTUFBTSxDQUFDZ0Msa0JBQVAsQ0FBMEJsQyxZQUExQixFQUF3QztBQUM1RG1DLDhCQUFjLEVBQUU7QUFDZEMsc0JBQUksRUFBRWhDLFFBQVEsQ0FBQ2lDLFVBQVQsQ0FBb0JDLG1FQUFwQjtBQURRO0FBRDRDLGVBQXhDLENBTEg7O0FBQUE7QUFLYkMscUJBTGE7O0FBQUEsbUJBV2ZBLE9BQU8sQ0FBQzdDLEtBWE87QUFBQTtBQUFBO0FBQUE7O0FBWWpCQyxzQkFBUSwwQkFBbUI0QyxPQUFPLENBQUM3QyxLQUFSLENBQWNvQyxPQUFqQyxFQUFSO0FBQ0FqQywyQkFBYSxDQUFDLEtBQUQsQ0FBYjtBQWJpQjtBQUFBOztBQUFBO0FBZWpCRixzQkFBUSxDQUFDLElBQUQsQ0FBUjtBQUNBRSwyQkFBYSxDQUFDLEtBQUQsQ0FBYjtBQUNBSiwwQkFBWSxDQUFDLElBQUQsQ0FBWjtBQUVNa0Isa0JBbkJXLEdBbUJKO0FBQUM2Qiw2QkFBYSxFQUFFRDtBQUFoQixlQW5CSTtBQUFBO0FBQUEscUJBb0JYRSw0Q0FBSyxDQUFDQyxHQUFOLDZFQUErRXJELE9BQS9FLEdBQTBGc0IsSUFBMUYsQ0FwQlc7O0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsS0FBSDs7QUFBQSxvQkFBWm9CLFlBQVk7QUFBQTtBQUFBO0FBQUEsS0FBbEI7O0FBeUJBLHNCQUVFO0FBQVMsTUFBRSxFQUFDLFFBQVo7QUFBQSw0QkFDQTtBQUFJLGVBQVMsRUFBQyx5QkFBZDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxZQURBLGVBRUEscUVBQUMsZ0VBQUQ7QUFBVSxZQUFNLEVBQUVZLE9BQWxCO0FBQUEsNkJBQ0k7QUFBTSxVQUFFLEVBQUMsY0FBVDtBQUF3QixnQkFBUSxFQUFFWixZQUFsQztBQUFBLGdDQUVJLHFFQUFDLG1FQUFEO0FBQWEsWUFBRSxFQUFDLGNBQWhCO0FBQStCLGlCQUFPLEVBQUViLFNBQXhDO0FBQW1ELGtCQUFRLEVBQUVTO0FBQTdEO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0JBRkosZUFHSTtBQUFRLGtCQUFRLEVBQUUvQixVQUFVLElBQUlFLFFBQWQsSUFBMEJOLFNBQTVDO0FBQXVELFlBQUUsRUFBQyxRQUExRDtBQUFBLGlDQUNBO0FBQU0sY0FBRSxFQUFDLGFBQVQ7QUFBQSxzQkFDQ0ksVUFBVSxnQkFDWDtBQUFLLHVCQUFTLEVBQUMsU0FBZjtBQUF5QixnQkFBRSxFQUFDO0FBQTVCO0FBQUE7QUFBQTtBQUFBO0FBQUEsb0JBRFcsR0FHWDtBQUpBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGdCQUhKLEVBY0tGLEtBQUssaUJBQ047QUFBSyxtQkFBUyxFQUFDLFlBQWY7QUFBNEIsY0FBSSxFQUFDLE9BQWpDO0FBQUEsb0JBQ0tBO0FBREw7QUFBQTtBQUFBO0FBQUE7QUFBQSxnQkFmSjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFESjtBQUFBO0FBQUE7QUFBQTtBQUFBLFlBRkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLFVBRkY7QUE2QkQ7O0dBM0d1Qk4sWTtVQU9QZSxpRSxFQUNFRSxtRTs7O0tBUktqQixZIiwiZmlsZSI6InN0YXRpYy93ZWJwYWNrL3BhZ2VzL3BheW1lbnRzL1tvcmRlcklEXS44YzhlM2Q1NjMwM2RkNTU1YzhhZi5ob3QtdXBkYXRlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IFJlYWN0LCB7IHVzZVN0YXRlLCB1c2VFZmZlY3QgfSBmcm9tIFwicmVhY3RcIjtcclxuaW1wb3J0IHtDYXJkRWxlbWVudCwgdXNlU3RyaXBlLCB1c2VFbGVtZW50c30gZnJvbSBcIkBzdHJpcGUvcmVhY3Qtc3RyaXBlLWpzXCI7XHJcbmltcG9ydCBheGlvcyBmcm9tICdheGlvcyc7XHJcbmltcG9ydCB7RWxlbWVudHN9IGZyb20gJ0BzdHJpcGUvcmVhY3Qtc3RyaXBlLWpzJztcclxuXHJcbmV4cG9ydCBkZWZhdWx0IGZ1bmN0aW9uIENoZWNrb3V0Rm9ybSh7b3JkZXJJRCwgYW1vdW50fSkge1xyXG5cclxuICBjb25zdCBbc3VjY2VlZGVkLCBzZXRTdWNjZWVkZWRdID0gdXNlU3RhdGUoZmFsc2UpO1xyXG4gIGNvbnN0IFtlcnJvciwgc2V0RXJyb3JdID0gdXNlU3RhdGUobnVsbCk7XHJcbiAgY29uc3QgW3Byb2Nlc3NpbmcsIHNldFByb2Nlc3NpbmddID0gdXNlU3RhdGUoJycpO1xyXG4gIGNvbnN0IFtkaXNhYmxlZCwgc2V0RGlzYWJsZWRdID0gdXNlU3RhdGUoZmFsc2UpO1xyXG4gIGNvbnN0IFtjbGllbnRTZWNyZXQsIHNldENsaWVudFNlY3JldF0gPSB1c2VTdGF0ZSgnJyk7XHJcbiAgY29uc3Qgc3RyaXBlID0gdXNlU3RyaXBlKCk7XHJcbiAgY29uc3QgZWxlbWVudHMgPSB1c2VFbGVtZW50cygpO1xyXG5cclxuICB1c2VFZmZlY3QoKCkgPT4ge1xyXG4gICAgLy8gQ3JlYXRlIFBheW1lbnRJbnRlbnQgYXMgc29vbiBhcyB0aGUgcGFnZSBsb2Fkc1xyXG4gICAgd2luZG93XHJcbiAgICAgIC5mZXRjaChcImh0dHBzOi8vMjlvZGRlZTJlaC5leGVjdXRlLWFwaS51cy1lYXN0LTEuYW1hem9uYXdzLmNvbS9kZXYvc3RyaXBlXCIsIHtcclxuICAgICAgICBtZXRob2Q6IFwiUE9TVFwiLFxyXG4gICAgICAgIGhlYWRlcnM6IHtcclxuICAgICAgICAgIFwiQ29udGVudC1UeXBlXCI6IFwiYXBwbGljYXRpb24vanNvblwiXHJcbiAgICAgICAgfSxcclxuICAgICAgICBib2R5OiBKU09OLnN0cmluZ2lmeSh7YW1vdW50fSlcclxuICAgICAgfSlcclxuICAgICAgLnRoZW4ocmVzID0+IHtcclxuICAgICAgICByZXR1cm4gcmVzLmpzb24oKTtcclxuICAgICAgfSlcclxuICAgICAgLnRoZW4oZGF0YSA9PiB7XHJcbiAgICAgICAgc2V0Q2xpZW50U2VjcmV0KGRhdGEuY2xpZW50U2VjcmV0KTtcclxuICAgICAgfSk7XHJcbiAgfSwgW10pO1xyXG5cclxuICBjb25zdCBjYXJkU3R5bGUgPSB7XHJcbiAgICBzdHlsZToge1xyXG4gICAgICBiYXNlOiB7XHJcbiAgICAgICAgY29sb3I6IFwiIzMyMzI1ZFwiLFxyXG4gICAgICAgIGZvbnRGYW1pbHk6ICdQb3BwaW5zLCBzYW5zLXNlcmlmJyxcclxuICAgICAgICBmb250U21vb3RoaW5nOiBcImFudGlhbGlhc2VkXCIsXHJcbiAgICAgICAgZm9udFNpemU6IFwiMTZweFwiLFxyXG4gICAgICAgIFwiOjpwbGFjZWhvbGRlclwiOiB7XHJcbiAgICAgICAgICBjb2xvcjogXCIjMzIzMjVkXCJcclxuICAgICAgICB9XHJcbiAgICAgIH0sXHJcbiAgICAgIGludmFsaWQ6IHtcclxuICAgICAgICBjb2xvcjogXCIjZmE3NTVhXCIsXHJcbiAgICAgICAgaWNvbkNvbG9yOiBcIiNmYTc1NWFcIlxyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfTtcclxuXHJcbiAgY29uc3QgaGFuZGxlQ2hhbmdlID0gYXN5bmMgKGV2ZW50KSA9PiB7XHJcblxyXG4gICAgc2V0RGlzYWJsZWQoZXZlbnQuZW1wdHkpO1xyXG4gICAgc2V0RXJyb3IoZXZlbnQuZXJyb3IgPyBldmVudC5lcnJvci5tZXNzYWdlIDogXCJcIik7XHJcblxyXG4gIH07XHJcblxyXG4gIGNvbnN0IGhhbmRsZVN1Ym1pdCA9IGFzeW5jIGV2ID0+IHtcclxuXHJcbiAgICBldi5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgc2V0UHJvY2Vzc2luZyh0cnVlKTtcclxuXHJcbiAgICBjb25zdCBwYXlsb2FkID0gYXdhaXQgc3RyaXBlLmNvbmZpcm1DYXJkUGF5bWVudChjbGllbnRTZWNyZXQsIHtcclxuICAgICAgcGF5bWVudF9tZXRob2Q6IHtcclxuICAgICAgICBjYXJkOiBlbGVtZW50cy5nZXRFbGVtZW50KENhcmRFbGVtZW50KVxyXG4gICAgICB9XHJcbiAgICB9KTtcclxuXHJcbiAgICBpZiAocGF5bG9hZC5lcnJvcikge1xyXG4gICAgICBzZXRFcnJvcihgUGF5bWVudCBmYWlsZWQgJHtwYXlsb2FkLmVycm9yLm1lc3NhZ2V9YCk7XHJcbiAgICAgIHNldFByb2Nlc3NpbmcoZmFsc2UpO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgc2V0RXJyb3IobnVsbCk7XHJcbiAgICAgIHNldFByb2Nlc3NpbmcoZmFsc2UpO1xyXG4gICAgICBzZXRTdWNjZWVkZWQodHJ1ZSk7XHJcblxyXG4gICAgICBjb25zdCBib2R5ID0ge3BheW1lbnRSZXN1bHQ6IHBheWxvYWR9O1xyXG4gICAgICBhd2FpdCBheGlvcy5wdXQoYGh0dHBzOi8vMjlvZGRlZTJlaC5leGVjdXRlLWFwaS51cy1lYXN0LTEuYW1hem9uYXdzLmNvbS9kZXYvb3JkZXJzLyR7b3JkZXJJRH1gLCBib2R5KTtcclxuICAgIH1cclxuXHJcbiAgfTtcclxuXHJcbiAgcmV0dXJuIChcclxuXHJcbiAgICA8c2VjdGlvbiBpZD1cInN0cmlwZVwiPlxyXG4gICAgPGgyIGNsYXNzTmFtZT1cImZzLTYwcCBmZi1yb2IgdGEtY2VudGVyXCI+UGF5IE5vdzwvaDI+XHJcbiAgICA8RWxlbWVudHMgc3RyaXBlPXtwcm9taXNlfT5cclxuICAgICAgICA8Zm9ybSBpZD1cInBheW1lbnQtZm9ybVwiIG9uU3VibWl0PXtoYW5kbGVTdWJtaXR9PlxyXG5cclxuICAgICAgICAgICAgPENhcmRFbGVtZW50IGlkPVwiY2FyZC1lbGVtZW50XCIgb3B0aW9ucz17Y2FyZFN0eWxlfSBvbkNoYW5nZT17aGFuZGxlQ2hhbmdlfSAvPlxyXG4gICAgICAgICAgICA8YnV0dG9uIGRpc2FibGVkPXtwcm9jZXNzaW5nIHx8IGRpc2FibGVkIHx8IHN1Y2NlZWRlZH0gaWQ9XCJzdWJtaXRcIj5cclxuICAgICAgICAgICAgPHNwYW4gaWQ9XCJidXR0b24tdGV4dFwiPlxyXG4gICAgICAgICAgICB7cHJvY2Vzc2luZyA/IChcclxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJzcGlubmVyXCIgaWQ9XCJzcGlubmVyXCI+PC9kaXY+XHJcbiAgICAgICAgICAgICkgOiAoXHJcbiAgICAgICAgICAgIFwiUGF5IG5vd1wiXHJcbiAgICAgICAgICAgICl9XHJcbiAgICAgICAgICAgIDwvc3Bhbj5cclxuICAgICAgICAgICAgPC9idXR0b24+XHJcblxyXG4gICAgICAgICAgICB7LyogU2hvdyBhbnkgZXJyb3IgdGhhdCBoYXBwZW5zIHdoZW4gcHJvY2Vzc2luZyB0aGUgcGF5bWVudCAqL31cclxuICAgICAgICAgICAge2Vycm9yICYmIChcclxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJjYXJkLWVycm9yXCIgcm9sZT1cImFsZXJ0XCI+XHJcbiAgICAgICAgICAgICAgICB7ZXJyb3J9XHJcbiAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICApfVxyXG5cclxuICAgICAgICA8L2Zvcm0+XHJcbiAgICA8L0VsZW1lbnRzPlxyXG4gICAgPC9zZWN0aW9uPlxyXG4gICk7XHJcbn0iXSwic291cmNlUm9vdCI6IiJ9