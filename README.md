Arc Tetra Frontend

This project contains the frontend of the website: https://arctetra.com

This project was made with NextJS.

How to Run:

    1. Download and unzip source code.
    2. Run command: npm install
    3. Run command: npm run dev

How to Build:

    1. Download and unzip source code.
    2. Run command: npm install
    3. Run command: npm run build